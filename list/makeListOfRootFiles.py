import os


def writeFileInDir(inputdirectory):
    inputdirectory = inputdirectory + "/"
    flist = os.listdir(inputdirectory)
    for i in range(len(flist)):
        flist[i]=inputdirectory + flist[i]
    rootfileshere = []
    subdirs = []
    outsubdirs = []
    for fname in flist:
        if os.path.isfile(fname): # if it is a file
            rootfileshere.append(fname)
        elif os.path.isdir(fname):  # if it's a directory name
            for fname2 in os.listdir(fname):
                flist.append(fname+"/"+fname2)
    output=inputdirectory.removeprefix(dir).removesuffix("/")
    print("%i files found in %s (assuming root files)" %(len(rootfileshere),output))

    f = open("listOfRootFile" + output + ".txt", "w")
    for line in rootfileshere:
        f.write(line + '\n')
    f.close()

dir="/eos/lyoeos.in2p3.fr/grid/cms/store/mc/RunIISummer20UL17NanoAODv9/"
#dir ="/eos/lyoeos.in2p3.fr/grid/cms/store/data/"
input = []
file = open("listOfDir.txt", "r")
for line in file:
    input.append(dir+line.removesuffix("\n"))

for set in input:
    writeFileInDir(set)