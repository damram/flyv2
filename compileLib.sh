export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly_3/fly/xgboost/include
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly_3/fly/xgboost/lib:/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly_3/fly/src
g++ -c -fPIC src/LMVA.cpp -o src/LMVA.o
g++ -shared -o src/libLMVA.so src/LMVA.o