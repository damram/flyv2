#!/usr/bin/env python3
import ROOT
import os
import sys

def dir_checker(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)

date = '2024-10-25'
print("**********")
print(date)
print("**********")
# year='2016'
year='2017'
#year='2018'
Lumi = 59.8
if('2016' in year):
    Lumi = 36.3
elif('2017' in year):
    Lumi = 41.5
# Masses = ['600', '625', '650', '675', '700', '800', '900', '1000', '1100', '1200']
Masses = ['700']
Samples = {
    'PROC_DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8': 18610000,
    'PROC_DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8': 6104000,
    'PROC_GluGluHToZZTo4L_M125_CP5TuneDown_13TeV_powheg2_JHUGenV7011_pythia8': 1218,
    'PROC_ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8': 354900,
    'PROC_ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8': 67930,
    'PROC_ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8': 113400,
    'PROC_ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8': 32510,
    'PROC_ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8': 32450,
    'PROC_THQ_ctcvcp_4f_Hincl_TuneCP5_13TeV_madgraph_pythia8': 745.2,
    'PROC_THW_ctcvcp_5f_Hincl_TuneCP5_13TeV_madgraph_pythia8': 147.1,
    'PROC_TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8': 3757,
    'PROC_ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8': 563.8,
    'PROC_TTJets_SingleLeptFromTbar_TuneCP5_13TeV-madgraphMLM-pythia8': 105700,
    'PROC_TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8': 687100,
    #'PROC_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8': 687100,
    'PROC_TTTT_TuneCP5_13TeV-amcatnlo-pythia8': 8.091,
    'PROC_TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8': 216.1,
    'PROC_TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8': 214.9,
    'PROC_TTWW_TuneCP5_13TeV-madgraph-pythia8': 7.003,
    'PROC_TTWZ_TuneCP5_13TeV-madgraph-pythia8': 2.441,
    'PROC_TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8': 281.36,
    'PROC_TTZToLL_M-1to10_TuneCP5_13TeV-amcatnlo-pythia8': 53.7,
    'PROC_TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8': 510.4,
    'PROC_TTZZ_TuneCP5_13TeV-madgraph-pythia8': 1.386,
    'PROC_tZq_ll_4f_ckm_NLO_TuneCP5_13TeV-amcatnlo-pythia8': 75.61,
    'PROC_WGToLNuG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8': 191300,
    'PROC_WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8': 61526700,
    'PROC_WminusH_HToZZTo4L_M125_CP5TuneUp_13TeV_powheg2-minlo-HWJ_JHUGenV7011_pythia8': 147,
    'PROC_WplusH_HToZZTo4L_M125_CP5TuneDown_13TeV_powheg2-minlo-HWJ_JHUGenV7011_pythia8': 232,
    #'PROC_WW_TuneCP5_13TeV-pythia8': 75800, //find why it's not there
    'PROC_WWW_4F_TuneCP5_13TeV-amcatnlo-pythia8': 215.8,
    'PROC_WWZ_4F_TuneCP5_13TeV-amcatnlo-pythia8': 165.1,
    'PROC_WZTo3LNu_TuneCP5_13TeV-amcatnloFXFX-pythia8': 5052,
    #'PROC_WZ_TuneCP5_13TeV-pythia8': 47130,
    'PROC_WZZ_TuneCP5_13TeV-amcatnlo-pythia8': 55.65,
    'PROC_ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8': 55480,
    #'PROC_ZZ_TuneCP5_13TeV-pythia8': 12140,
    'PROC_ZZTo4L_TuneCP5_13TeV_powheg_pythia8': 1256,
    'PROC_ZZZ_TuneCP5_13TeV-amcatnlo-pythia8': 13.98,
    #'RunALL': 1
}

Cuts = [

    '0000',
    '0010',
    '0020',
    # '0030',
    # '0040',
    # '0050',
    '0060',
    '0070'

]

if(year == '2016'):

    Weights = [

    'evWeight',
    'syst_elec_recoUp',
    'syst_elec_recoDown',
    'syst_elec_idUp',
    'syst_elec_idDown',
    'syst_muon_hltUp',
    'syst_muon_hltDown',
    'stat_muon_hlt_2016Up',
    'stat_muon_hlt_2016Down',
    'syst_muon_recoUp',
    'syst_muon_recoDown',
    'stat_muon_reco_2016Up',
    'stat_muon_reco_2016Down',
    'syst_muon_idUp',
    'syst_muon_idDown',
    'stat_muon_id_2016Up',
    'stat_muon_id_2016Down',
    'syst_muon_isoUp',
    'syst_muon_isoDown',
    'stat_muon_iso_2016Up',
    'stat_muon_iso_2016Down',
    'syst_puUp',
    'syst_puDown',
    'syst_b_correlatedUp',
    'syst_b_correlatedDown',
    'syst_b_uncorrelated_2016Up',
    'syst_b_uncorrelated_2016Down',
    'syst_l_correlatedUp',
    'syst_l_correlatedDown',
    'syst_l_uncorrelated_2016Up',
    'syst_l_uncorrelated_2016Down',
    'syst_prefiringUp',
    'syst_prefiringDown',
    'mescaleUp',
    'mescaleDown',
    'renscaleUp',
    'renscaleDown',
    'facscaleUp',
    'facscaleDown',
    'isrUp',
    'isrDown',
    'fsrUp',
    'fsrDown',
    # 'pdfalphasUp',
    # 'pdfalphasDown'
    
    ]

elif(year == '2017'):
    Weights = [

    'evWeight',
    'syst_elec_recoUp',
    'syst_elec_recoDown',
    'syst_elec_idUp',
    'syst_elec_idDown',
    'syst_muon_hltUp',
    'syst_muon_hltDown',
    'stat_muon_hlt_2017Up',
    'stat_muon_hlt_2017Down',
    'syst_muon_recoUp',
    'syst_muon_recoDown',
    'stat_muon_reco_2017Up',
    'stat_muon_reco_2017Down',
    'syst_muon_idUp',
    'syst_muon_idDown',
    'stat_muon_id_2017Up',
    'stat_muon_id_2017Down',
    'syst_muon_isoUp',
    'syst_muon_isoDown',
    'stat_muon_iso_2017Up',
    'stat_muon_iso_2017Down',
    'syst_puUp',
    'syst_puDown',
    'syst_b_correlatedUp',
    'syst_b_correlatedDown',
    'syst_b_uncorrelated_2017Up',
    'syst_b_uncorrelated_2017Down',
    'syst_l_correlatedUp',
    'syst_l_correlatedDown',
    'syst_l_uncorrelated_2017Up',
    'syst_l_uncorrelated_2017Down',
    'syst_prefiringUp',
    'syst_prefiringDown',
    'mescaleUp',
    'mescaleDown',
    'renscaleUp',
    'renscaleDown',
    'facscaleUp',
    'facscaleDown',
    'isrUp',
    'isrDown',
    'fsrUp',
    'fsrDown',
    # 'pdfalphasUp',
    # 'pdfalphasDown'

    ]

elif(year == '2018'):
    Weights = [

    'evWeight',
    'syst_elec_recoUp',
    'syst_elec_recoDown',
    'syst_elec_idUp',
    'syst_elec_idDown',
    'syst_muon_hltUp',
    'syst_muon_hltDown',
    'stat_muon_hlt_2018Up',
    'stat_muon_hlt_2018Down',
    'syst_muon_recoUp',
    'syst_muon_recoDown',
    'stat_muon_reco_2018Up',
    'stat_muon_reco_2018Down',
    'syst_muon_idUp',
    'syst_muon_idDown',
    'stat_muon_id_2018Up',
    'stat_muon_id_2018Down',
    'syst_muon_isoUp',
    'syst_muon_isoDown',
    'stat_muon_iso_2018Up',
    'stat_muon_iso_2018Down',
    'syst_puUp',
    'syst_puDown',
    'syst_b_correlatedUp',
    'syst_b_correlatedDown',
    'syst_b_uncorrelated_2018Up',
    'syst_b_uncorrelated_2018Down',
    'syst_l_correlatedUp',
    'syst_l_correlatedDown',
    'syst_l_uncorrelated_2018Up',
    'syst_l_uncorrelated_2018Down',
    'syst_prefiringUp',
    'syst_prefiringDown',
    'mescaleUp',
    'mescaleDown',
    'renscaleUp',
    'renscaleDown',
    'facscaleUp',
    'facscaleDown',
    'isrUp',
    'isrDown',
    'fsrUp',
    'fsrDown',
    # 'pdfalphasUp',
    # 'pdfalphasDown'

    ]

files = (
    (Samples, Cuts, Masses, Weights)
    )

for cut in files[1]:

    if cut=='0060':
        nameObs = 'signal_3l_'
    elif cut=='0070':
        nameObs = 'signal_4l_'
    elif cut=='0000':
        nameObs='ZZ_'
    elif cut=="0010":
        nameObs='WZ_'
    elif cut=="0020":
        nameObs='Nonprompt_'
    else:
        print("Invalid region :",cut)
        sys.exit(-1)
    region = cut

    print('region', region)

    Asimov = True
    # if('Signal' in cut):
    #     Asimov = True

    output = 'combine/' + year + '/'
    dir_checker(output)

    output = 'combine/' + year + '/inclusive/'
    dir_checker(output)

    output = 'combine/' + year + '/inclusive/inputs/'
    dir_checker(output)

    outputFile = ROOT.TFile.Open('combine/' + year + '/inclusive/inputs/' + nameObs + cut + '.root','RECREATE')

    name = []

    for sample in files[0]:

        process = 'none'
        if('TTZ' in sample):
            process = 'signal'
        # elif('TT_2L' in sample):
        #     process = 'tt2l'
        # elif('TT_SL' in sample):
        #     process = 'tt1l'
        # elif('TT_Had' in sample):
        #     process = 'tt0l'
        elif('_ST_' in sample):
            process = 'singletop'
        elif('TTTo2L' in sample or 'TTTT' in sample):
            process='tt'
        elif('TTW' in sample or 'ttH' in sample or 'TTG' in sample or 'TTJ' in sample or 'THQ' in sample or 'THW' in sample or 'tZq' in sample): #TWZ
            process = 'ttx'
        elif('WW' in sample or 'WWW' in sample or 'WH' in sample or 'ZH' in sample or 'WG' in sample or 'ZG' in sample):
            process = 'multibosons'
        elif('WZ' in sample):
            process = 'WZ'
        elif('ZZ' in sample):
            process = 'ZZ'
        elif('DY' in sample):
            process = 'dy'
        elif('WJets' in sample):
            process = 'wjets'
        elif('Run' in sample):
            process = 'data_obs' 
        else:
            print(sample, 'not categorized')

        inputFile = ROOT.TFile.Open('results/' + year[-2:] + '/' + date + '/ready/' + sample + "_" + cut + '.root')
        # if(year == '2016' and 'Signal' in sample and (region == '2_0_2_3_0' or region == '2_0_2_4_0')) or (year == '2018' and 'Signal' in sample and region == '2_0_2_3_0'):
        #     inputFile = ROOT.TFile.Open('analyzed/' + date + '/Merged/Signal650_UL' + year + '.root')

        if('Run' not in sample):
            genhisto = inputFile.Get('hSumOfWeights_nocut;1')
            sumofweights = genhisto.GetEntries()*genhisto.GetMean()

        scale = Samples[sample]*Lumi/sumofweights if 'Run' not in sample else 1

        # print('sample :',sample)
        # print('sow :', sumofweights)
        # print('scale :', scale)

        for weight in files[3]:

            # non separate the backgrounds with the flips and the fakes
            # print(weight)
            # if(year == '2016' and 'Signal' in sample and (region == '2_0_2_3_0' or region == '2_0_2_4_0')) or (year == '2018' and 'Signal' in sample and region == '2_0_2_3_0'):
            #     histo = inputFile.Get(process + '_' + weight + '_cut_2_0_2_2_0;1')
            # else:
            #     histo = inputFile.Get(process + '_' + weight + '_cut_' + region + ';1')
            #print('Getting', 'signal' + '_' + weight + '_cut' + region + ';1', 'in :', inputFile)
            # histo = inputFile.Get('signal' + '_' + weight + '_cut' + region + ';1') if 'Run' not in sample else inputFile.Get('data_obs' + '_' + weight + '_cut' + region + ';1')
            # histo_scaled = histo.Clone()
            # histo_scaled.Scale(scale)
            # histo_scaled.Rebin(200//5)
            # if('evWeight' in weight):
            #     name.append(process)
            #     outputFile.WriteObject(histo_scaled, process)
            #     if(Asimov == True and ('TTZ' not in sample and 'TTG' not in sample)):
            #         process_asimov = 'data_obs'
            #         name.append(process_asimov)
            #         outputFile.WriteObject(histo_scaled, process_asimov)
            # else:
            #     name.append(process + '_' + weight)
            #     outputFile.WriteObject(histo_scaled, process + '_' + weight)
            # if('Run' in sample):
            #     break
            # else:
            #     histoFake = inputFile.Get('signal' + '_' + weight + '_cut' + region[:-1] + '1;1')
            #     histoConversion = inputFile.Get('signal' + '_' + weight + '_cut' + region[:-1] + '2;1')

            #     histoFake_scaled = histoFake.Clone()
            #     histoFake_scaled.Scale(scale)
            #     histoFake_scaled.Rebin(200//5)

            #     histoConversion_scaled = histo.Clone()
            #     histoConversion_scaled.Scale(scale)
            #     histoConversion_scaled.Rebin(200//5)

            #     if('evWeight' in weight):
            #         name.append('fake')
            #         outputFile.WriteObject(histoFake_scaled, 'fake')
            #         name.append('conversion')
            #         outputFile.WriteObject(histoFake_scaled, 'conversion')
            #         if(Asimov == True and ('TTZ' not in sample and 'TTG' not in sample)):
            #             process_asimov = 'data_obs'
            #             name.append(process_asimov)
            #             histoAsimov = histoFake_scaled.Clone()
            #             histoAsimov.Add(histoConversion_scaled)
            #             outputFile.WriteObject(histoAsimov, process_asimov)
            #     else:
            #         name.append('fake_' + weight)
            #         outputFile.WriteObject(histoFake_scaled, 'fake_' + weight)
            #         name.append('conversion_' + weight)
            #         outputFile.WriteObject(histoConversion_scaled, 'conversion_' + weight)

            # separate the backgrounds with the flips and the fakes
            # print(weight)
            #print('Getting', 'signal' + '_' + weight + '_cut' + region + ';1', 'in :', inputFile)

            #Change this too
            histo = inputFile.Get(nameObs + weight + '_cut' + region + ';1') if 'Run' not in sample else inputFile.Get('data_obs' + '_' + weight + '_cut' + region + ';1')
            print("Using hist :",nameObs + weight + '_cut' + region + ';1')
            #modify those lines
            try:
                histo_scaled = histo.Clone()
            except:
                histo = inputFile.Get("signal_" + weight + '_cut' + region + ';1')
                histo_scaled = histo.Clone()
                print("exeception")
            
            # until here
            histo_scaled = histo.Clone()
            histo_scaled.Scale(scale)
            if('Run' not in sample):
                histoConversion = inputFile.Get(nameObs + weight + '_cut' + region[:-1] + '2;1')
                try:
                    histoConversion_scaled = histoConversion.Clone()
                except:
                    histoConversion = inputFile.Get("signal_" + weight + '_cut' + region[:-1] + '2;1')
                    histoConversion_scaled = histoConversion.Clone()
                histoConversion_scaled.Scale(scale)
                histoFake = inputFile.Get(nameObs + weight + '_cut' + region[:-1] + '1;1')
                try:
                    histoFake_scaled = histoFake.Clone()
                except:
                    histoFake = inputFile.Get("signal_" + weight + '_cut' + region[:-1] + '1;1')
                    histoFake_scaled = histoFake.Clone()
                histoFake_scaled.Scale(scale)
            if('evWeight' in weight) or weight=="":
                name.append(process)
                outputFile.WriteObject(histo_scaled, process)
                if('Run' not in sample):
                    name.append('conversion')
                    outputFile.WriteObject(histoConversion_scaled, 'conversion')
                    name.append('fake')
                    outputFile.WriteObject(histoFake_scaled, 'fake')
                if(Asimov == True and 'Signal' not in sample):
                    process_asimov = 'data_obs'
                    name.append(process_asimov)
                    histo_scaled.Add(histoConversion_scaled)
                    histo_scaled.Add(histoFake_scaled)
                    outputFile.WriteObject(histo_scaled, process_asimov)
            else:
                name.append(process + '_' + weight)
                outputFile.WriteObject(histo_scaled, process + '_' + weight)
                if('Signal' not in sample) and ('Data' not in sample):
                    name.append('conversion_' + weight)
                    outputFile.WriteObject(histoConversion_scaled, 'conversion_' + weight)
                    name.append('fake_' + weight)
                    outputFile.WriteObject(histoFake_scaled, 'fake_' + weight)
            if('Run' in sample):
                break

    # merge histograms with the same name
    name.sort()
    # print(name)
    for i in range(len(name) - 1):
        if(i == len(name) - 1):
            break
        j = 1
        while(name[i] == name[i+j]):
            j += 1
        if(j > 1):
            histo1 = outputFile.Get(name[i] + ';1')
            for k in range(j-1):
                number = str(k+2)
                histo2 = outputFile.Get(name[i] + ';' + number)
                histo1.Add(histo2)
                outputFile.Delete(name[i] + ';' + number)
                name.pop(i+1)
            outputFile.Delete(name[i] + ';1')
            outputFile.WriteObject(histo1, name[i])

print('Done')