#!/bin/bash
#
#SBATCH --spread-job
# mail-type=BEGIN, END, FAIL, REQUEUE, ALL, STAGE_OUT, TIME_LIMIT_90
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=d.amram@ip2i.in2p3.fr
#SBATCH --time=6:00:00
#SBATCH --output=slurm.out
#SBATCH --error=slurm.err
#SBATCH --job-name=test2
for i in {0..9}
do
    srun -n1 --exclusive sleep 10 &
done
wait