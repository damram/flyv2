"""
File contains job options 
"""

# options for Nanoaodrdframe
config = {
        # tree name of input file(s)
        'intreename': "Events",

        # tree name of output file(s) it cannot be the same as the input tree name or it'll crash
        'outtreename': "outputTree",

        #data year (2016,2017,2018)
        'year': '2017',

        # is ReReco or Ultra Legacy
        'runtype': 'UL',

        'datatype': -1, # 0=MC ; 1=DATA ; -1=Auto

        #for correction
        
        # good json file
        # 'goodjson': 'data/Cert_294927-306462_13TeV_EOY2017ReReco_Collisions17_JSON.txt',
        'goodjson' : 'data/Legacy_RunII/Cert_294927-306462_13TeV_UL2017_Collisions17_GoldenJSON.txt',

        # pileup weight for MC
        'pileupfname': 'data/LUM/2017_UL/puWeights.json',

        'pileuptag': 'Collisions17_UltraLegacy_goldenJSON',

        # json filename for BTV correction
        'btvfname': 'data/BTV/2017_UL/btagging.json',
        'fname_btagEff': 'data/BTV/2017_UL/BtaggingEfficiency.root',
        'hname_btagEff_bcflav': 'h_btagEff_bcflav',
        'hname_btagEff_lflav': 'h_btagEff_lflav',

        # BTV correction type
        # 'btvtype': 'deepJet_shape',
        'btvtype': 'deepJet_mujets',
    
        # Muon Correction 
        'muon_roch_fname': 'data/MUO/2017_UL/RoccoR2017UL.txt', 
        'muon_fname': 'data/MUO/2017_UL/muon_Z.json.gz', 
        'muon_HLT_type': 'NUM_IsoMu27_DEN_CutBasedIdTight_and_PFIsoTight',#'HLT UL scale factor',
        'muon_RECO_type': 'NUM_TrackerMuons_DEN_genTracks',#'RECO UL scale factor',
        'muon_ID_type': 'NUM_TightID_DEN_TrackerMuons',#'ID UL scale factor',
        'muon_ISO_type': 'NUM_TightRelIso_DEN_TightIDandIPCut',#'ISO UL scale factor',
        #'muontype': 'NUM_TightRelIso_DEN_MediumID',#'Medium ISO UL scale factor',

        # Electron Correction 
        'electron_fname': 'data/EGM/2017_UL/electron.json.gz', 
        'electron_reco_type': 'RecoAbove20',
        'electron_id_type': 'Tight',#'Tight ID UL scale factor',
        'fname_electriggerEff': 'data/EGM/2017_UL/egammaTrigEffi_wp90noiso_EGM2D_2017.root',
        #'muontype': 'NUM_TightRelIso_DEN_MediumID',#'Medium ISO UL scale factor',

        # json file name for JERC
        'jercfname': 'data/JERC/UL17_jerc.json',

        # conbined correction type for jets
        'jerctag': 'Summer19UL17_V5_MC_L1L2L3Res_AK4PFchs', 

        # jet uncertainty 
        'jercunctag': 'Summer19UL17_V5_MC_Total_AK4PFchs',

        'stepone': 1,

        'weights': [],
        
        }

# Define the initial systematic variations and other weights
initial_weights = [
        "syst_elec_hltUp", "syst_elec_hltDown", "stat_elec_hlt_2017Up", "stat_elec_hlt_2017Down", 
        "syst_elec_recoUp", "syst_elec_recoDown", "syst_elec_idUp", "syst_elec_idDown", 
        "syst_muon_hltUp", "syst_muon_hltDown", "stat_muon_hlt_2017Up", "stat_muon_hlt_2017Down", 
        "syst_muon_recoUp", "syst_muon_recoDown", "stat_muon_reco_2017Up", "stat_muon_reco_2017Down", 
        "syst_muon_idUp", "syst_muon_idDown", "stat_muon_id_2017Up", "stat_muon_id_2017Down", 
        "syst_muon_isoUp", "syst_muon_isoDown", "stat_muon_iso_2017Up", "stat_muon_iso_2017Down", 
        "syst_puUp", "syst_puDown",
        "syst_b_correlatedUp", "syst_b_correlatedDown", "syst_b_uncorrelated_2017Up", "syst_b_uncorrelated_2017Down",
        "syst_l_correlatedUp",  "syst_l_correlatedDown", "syst_l_uncorrelated_2017Up", "syst_l_uncorrelated_2017Down", 
        "syst_prefiringUp", "syst_prefiringDown",
        "syst_pt_topUp", "syst_pt_topDown"
]

# Add the scale and PS weights
scale_ps_weights = [
        "renscaleUp", "renscaleDown",
        "isrUp", "isrDown",
        "fsrUp",  "fsrDown",
]

# Combine the initial weights and scale/PS weights
config['weights'] = initial_weights + scale_ps_weights

# Add PDF weight variations
pdf_weights = ["syst_pdf{}".format(i) for i in range(1, 103)]
config['weights'].extend(pdf_weights)

# Add specific PDF alpha_s variations
config['weights'].extend(["syst_pdfalphasUp", "syst_pdfalphasDown"])


# processing options
procflags = {
        # how many jobs?
        'split': 'Max',
        # if False, one output file per input file, if True then one output file for everything
        'allinone': False, 
        # if True then skip existing analyzed files
        'skipold': True,
        # travel through the subdirectories and their subdirecties when processing.
        # becareful not to mix MC and real DATA in them.
        'recursive': True,
        # if False then only selected branches which is done in the .cpp file will be saved
        'saveallbranches': False,
        #How many input files?
        'nrootfiles': 10000,
        }