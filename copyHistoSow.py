#!/usr/bin/env python3
import ROOT
import sys

#file is the final file, in ready
def copy_histogram(file):
    # I get the file in merged that have the true sumofweight hist (before the cuts)
    name1 = file.replace('ready', 'merged')
    name1 = name1[:-10] + name1[-5:]
    file1 = ROOT.TFile(name1, "READ")
    histo1 = file1.Get("hSumOfWeights_nocut")

    # I now get the file i want to change
    file2 = ROOT.TFile(file, "UPDATE")

    # Copie de l'histogramme dans le deuxième fichier
    histo1.Write("hSumOfWeights_nocut", ROOT.TObject.kOverwrite)

    # Fermeture des fichiers
    file1.Close()
    file2.Close()

# Appel de la fonction pour copier ou remplacer l'histogramme
copy_histogram(sys.argv[1])
print(sys.argv[1], "done")