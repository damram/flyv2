import os
from datetime import date
import argparse

def dir_checker(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)

parser = argparse.ArgumentParser()
parser.add_argument('-Y', '--year', type=str, help='Give the year to analyze [2016pre, 2016post, 2017, 2018]')
parser.add_argument('-N', '--nfiles', type=str, help='Number of files to analyze. Give \'all\' to analyze all files.')
parser.add_argument('-O', '--outputFolder', type=str, help='Output folder name', default='test')
parser.add_argument('-T', '--type', type=str, help='Data or MC', default='mc')

args = parser.parse_args()
year = args.year
tasks = args.nfiles
extraOutputFile = args.outputFolder + '_nbTasks' + str(tasks)
type = args.type



pwdToMC16pre = 'DSinfo/16/mc/'
pwdToMC16post = 'DSinfo/16/mcAPV/'
pwdToMC17 = 'DSinfo/17/mc/'
pwdToMC18 = 'DSinfo/18/mc/'

monteCarloSaples = [
    'PROC_QCD_Pt-1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-120To170_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-15To20_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-15to20_EMEnriched_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-170To300_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-20To30_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-20to30_EMEnriched_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-300To470_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-30To50_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-30to50_EMEnriched_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-470To600_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-50To80_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-600To800_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-800To1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-80To120_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'PROC_QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV-pythia8',
    'PROC_DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'PROC_DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'PROC_GluGluHToZZTo4L_M125_CP5TuneDown_13TeV_powheg2_JHUGenV7011_pythia8',
    'PROC_ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'PROC_ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'PROC_ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'PROC_ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'PROC_THQ_ctcvcp_4f_Hincl_TuneCP5_13TeV_madgraph_pythia8',
    'PROC_THW_ctcvcp_5f_Hincl_TuneCP5_13TeV_madgraph_pythia8',
    'PROC_TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'PROC_ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8',
    'PROC_TTJets_SingleLeptFromTbar_TuneCP5_13TeV-madgraphMLM-pythia8',
    'PROC_TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8',
    'PROC_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
    'PROC_TTTT_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'PROC_TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'PROC_TTWW_TuneCP5_13TeV-madgraph-pythia8',
    'PROC_TTWZ_TuneCP5_13TeV-madgraph-pythia8',
    'PROC_TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_TTZToLL_M-1to10_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_TTZZ_TuneCP5_13TeV-madgraph-pythia8',
    'PROC_tZq_ll_4f_ckm_NLO_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_WGToLNuG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8',
    'PROC_WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8',
    'PROC_WminusH_HToZZTo4L_M125_CP5TuneUp_13TeV_powheg2-minlo-HWJ_JHUGenV7011_pythia8',
    'PROC_WplusH_HToZZTo4L_M125_CP5TuneDown_13TeV_powheg2-minlo-HWJ_JHUGenV7011_pythia8',
    'PROC_WW_TuneCP5_13TeV-pythia8',
    'PROC_WWW_4F_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_WWZ_4F_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_WZTo3LNu_TuneCP5_13TeV-amcatnloFXFX-pythia8',
    'PROC_WZ_TuneCP5_13TeV-pythia8',
    'PROC_WZZ_TuneCP5_13TeV-amcatnlo-pythia8',
    'PROC_ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8',
    'PROC_ZZ_TuneCP5_13TeV-pythia8',
    'PROC_ZZTo4L_TuneCP5_13TeV_powheg_pythia8',
    'PROC_ZZZ_TuneCP5_13TeV-amcatnlo-pythia8'

]


pwdToDATA16 = 'DSinfo/16/data/'
dataSamplesFor2016 = [

    'Run2016B_SingleElectron',
    'Run2016C_SingleElectron',
    'Run2016D_SingleElectron',
    'Run2016E_SingleElectron',
    'Run2016F_SingleElectron',
    'Run2016G_SingleElectron',
    'Run2016H_SingleElectron',


    'Run2016B_SingleMuon',
    'Run2016B_SingleMuon',
    'Run2016D_SingleMuon',
    'Run2016E_SingleMuon',
    'Run2016F_SingleMuon',
    'Run2016G_SingleMuon',
    'Run2016H_SingleMuon',

]


pwdToDATA17 = 'DSinfo/17/data/'
dataSamplesFor2017 = [

    'Run2017F_SingleElectron',
    'Run2017B_SingleElectron',
    'Run2017C_SingleElectron',
    'Run2017D_SingleElectron',
    'Run2017E_SingleElectron',

    'Run2017E_SingleMuon',
    'Run2017F_SingleMuon',
    'Run2017B_SingleMuon',
    'Run2017C_SingleMuon',
    'Run2017D_SingleMuon',

    'Run2017B_MuonEG',
    'Run2017C_MuonEG',
    'Run2017D_MuonEG',
    'Run2017E_MuonEG',
    'Run2017F_MuonEG',

    'Run2017B_DoubleMuon',
    'Run2017C_DoubleMuon',
    'Run2017D_DoubleMuon',
    'Run2017E_DoubleMuon',
    'Run2017F_DoubleMuon',

    'Run2017B_DoubleEG',
    'Run2017C_DoubleEG',
    'Run2017D_DoubleEG',
    'Run2017E_DoubleEG',
    'Run2017F_DoubleEG'

]

pwdToDATA18 = 'DSinfo/18/data/'
dataSamplesFor2018 = [

    'Run2018A_EGamma',
    'Run2018B_EGamma',
    'Run2018C_EGamma',
    'Run2018D_EGamma',

    'Run2018A_SingleMuon',
    'Run2018B_SingleMuon',
    'Run2018C_SingleMuon',
    'Run2018D_SingleMuon',

]


copyInstance = True

if year == '16pre':
    filesToAnalyze = (
        (monteCarloSaples, pwdToMC16pre),
        (dataSamplesFor2016, pwdToDATA16)
    )
    jobconfiganalysisFile = 'jobconfiganalysis_16pre'

if year == '16post':
    filesToAnalyze = (
        (monteCarloSaples, pwdToMC16post),
        (dataSamplesFor2016, pwdToDATA16),
    )
    jobconfiganalysisFile = 'jobconfiganalysis_16post'


if year == '17':
    # if type=='all':
    #     filesToAnalyze = (
    #         (monteCarloSaples, pwdToMC17),
    #         (dataSamplesFor2017, pwdToDATA17),
    #     )
    if type=='mc':
        filesToAnalyze = (
            (monteCarloSaples, pwdToMC17),
        )
        jobconfiganalysisFile = 'jobconfiganalysis_17_mc'
    elif type=='data':
        filesToAnalyze = (
            (dataSamplesFor2017, pwdToDATA17),
        )
        jobconfiganalysisFile = 'jobconfiganalysis_17_data'


if year == '18':
    filesToAnalyze = (
        (monteCarloSaples, pwdToMC18),
        (dataSamplesFor2018, pwdToDATA18),
    )
    jobconfiganalysisFile = 'jobconfiganalysis_18'


n = 0
while n != 2:
    for sample in filesToAnalyze[n][0]:

        if len(filesToAnalyze[n][0]) == 0: break

        analysisOutput = 'results/' + year + '/'
        dir_checker(analysisOutput)
        analysisOutput = 'results/' + year + '/' + str(date.today()) + '/'
        dir_checker(analysisOutput)
        analysisOutput = 'results/' + year + '/' + str(date.today()) + '/' + extraOutputFile + '/'
        dir_checker(analysisOutput)

        if copyInstance: os.system('cp src/BaseAnalyser.cpp ' + analysisOutput + "BaseAnalyser_COPY_"+ str(date.today()) +"_.cpp")

        analysisOutput += sample + '/'
        dir_checker(analysisOutput)
        dir_checker(analysisOutput + 'slurmOutputs')

        sample += '.txt'

        with open(filesToAnalyze[n][1] + sample, 'r') as fToRead:
            nLines = str(len(fToRead.readlines())+10)
        
        if tasks == 'all': ntasks = nLines
        elif tasks != 'all' and (int(nLines) < int(tasks)): ntasks = nLines
        elif tasks != 'all' and (int(nLines) > int(tasks)): ntasks = tasks

        slurmConfigs = [

            ' --cpus-per-task=' + '1',
            ' --partition=' + 'normal',
            ' --ntasks=' + ntasks,
            ' --job-name=' + sample.replace('PROC_',year + '').replace('.txt',''),
            ' --output=' + analysisOutput + 'slurmOutputs/output.out',
            ' --error=' + analysisOutput + 'slurmOutputs/errors.err'
        ]

        args = [
            ' ' + filesToAnalyze[n][1] + sample, #inputFile
            ' ' + analysisOutput, #outputFiles
            ' ' + tasks,
            ' ' + jobconfiganalysisFile,
        ]

        os.system('sbatch' + ''.join(slurmConfigs) + ' submitBatchJob.sh' + ''.join(args))
    n += 1