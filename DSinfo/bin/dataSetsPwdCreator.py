import os

monteCarloSaplesFor2016 = [
    
    'DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'QCD_Pt-1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-120To170_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-15To20_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-15to20_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-170To300_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-20To30_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-20to30_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-300To470_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-30To50_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-30to50_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-470To600_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-50To80_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-600To800_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-800To1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-80To120_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV-pythia8',
    'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8',
    'ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8',
    'TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
    'TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8',
    'TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8',
    'WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8',
    'WW_TuneCP5_13TeV-pythia8',
    'WZ_TuneCP5_13TeV-pythia8',
    'ZZ_TuneCP5_13TeV-pythia8',
 
]

monteCarloSaplesFor2016APV = [ #35 files
    
    'DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8',
    'QCD_Pt-1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-120To170_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-15To20_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-170To300_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-20To30_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-300To470_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-30To50_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-30to50_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-470To600_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-50To80_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-600To800_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-800To1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-80To120_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV-pythia8',
    'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8',
    'ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8',
    'TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
    'TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8',
    'TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8',
    'WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8',
    'WW_TuneCP5_13TeV-pythia8',
    'WZ_TuneCP5_13TeV-pythia8',
    'ZZ_TuneCP5_13TeV-pythia8',
 
]

monteCarloSaplesFor2017 = [ #59 files

    'DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'GluGluHToZZTo4L_M125_CP5TuneDown_13TeV_powheg2_JHUGenV7011_pythia8',
    'QCD_Pt-1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-120To170_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-15To20_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-15to20_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-170To300_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-20To30_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-20to30_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-300To470_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-30To50_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-30to50_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-470To600_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-50To80_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-600To800_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-800To1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-80To120_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV-pythia8',
    'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8',
    'ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'THQ_ctcvcp_4f_Hincl_TuneCP5_13TeV_madgraph_pythia8',
    'THW_ctcvcp_5f_Hincl_TuneCP5_13TeV_madgraph_pythia8',
    'TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8',
    'TTJets_SingleLeptFromTbar_TuneCP5_13TeV-madgraphMLM-pythia8',
    'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8',
    'TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
    'TTTT_TuneCP5_13TeV-amcatnlo-pythia8',
    'TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'TTWW_TuneCP5_13TeV-madgraph-pythia8',
    'TTWZ_TuneCP5_13TeV-madgraph-pythia8',
    'TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8',
    'TTZToLL_M-1to10_TuneCP5_13TeV-amcatnlo-pythia8',
    'TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8',
    'TTZZ_TuneCP5_13TeV-madgraph-pythia8',
    'tZq_ll_4f_ckm_NLO_TuneCP5_13TeV-amcatnlo-pythia8',
    'WGToLNuG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8',
    'WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8',
    'WminusH_HToZZTo4L_M125_CP5TuneUp_13TeV_powheg2-minlo-HWJ_JHUGenV7011_pythia8',
    'WplusH_HToZZTo4L_M125_CP5TuneDown_13TeV_powheg2-minlo-HWJ_JHUGenV7011_pythia8',
    'WW_TuneCP5_13TeV-pythia8',
    'WWW_4F_TuneCP5_13TeV-amcatnlo-pythia8',
    'WWZ_4F_TuneCP5_13TeV-amcatnlo-pythia8',
    'WZTo3LNu_TuneCP5_13TeV-amcatnloFXFX-pythia8',
    'WZ_TuneCP5_13TeV-pythia8',
    'WZZ_TuneCP5_13TeV-amcatnlo-pythia8',
    'ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8',
    'ZZ_TuneCP5_13TeV-pythia8',
    'ZZTo4L_TuneCP5_13TeV_powheg_pythia8',
    'ZZZ_TuneCP5_13TeV-amcatnlo-pythia8'

]

monteCarloSaplesFor2018 = [ #37 files
    
    'DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8',
    'QCD_Pt-1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-120To170_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-15To20_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-15to20_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-170To300_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-20To30_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-20to30_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-300To470_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-30To50_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-30to50_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-470To600_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-50To80_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV-pythia8',
    'QCD_Pt-600To800_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-800To1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-80To120_MuEnrichedPt5_TuneCP5_13TeV-pythia8',
    'QCD_Pt-80to120_EMEnriched_TuneCP5_13TeV-pythia8',
    'ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8',
    'ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8',
    'ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8',
    'TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8',
    'TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8',
    'TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8',
    'TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8',
    'TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8',
    'WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8',
    'WW_TuneCP5_13TeV-pythia8',
    'WZ_TuneCP5_13TeV-pythia8',
    'ZZ_TuneCP5_13TeV-pythia8',

]

dataSamplesFor2016 = [

    'Run2016B/SingleElectron/NANOAOD',
    'Run2016C/SingleElectron/NANOAOD',
    'Run2016D/SingleElectron/NANOAOD',
    'Run2016E/SingleElectron/NANOAOD',
    'Run2016F/SingleElectron/NANOAOD',
    'Run2016G/SingleElectron/NANOAOD',
    'Run2016H/SingleElectron/NANOAOD',

    'Run2016B/SingleMuon/NANOAOD',
    'Run2016C/SingleMuon/NANOAOD',
    'Run2016D/SingleMuon/NANOAOD',
    'Run2016E/SingleMuon/NANOAOD',
    'Run2016F/SingleMuon/NANOAOD',
    'Run2016G/SingleMuon/NANOAOD',
    'Run2016H/SingleMuon/NANOAOD'


]

dataSamplesFor2017 = [

    'Run2017B/SingleElectron/NANOAOD',
    'Run2017C/SingleElectron/NANOAOD',
    'Run2017D/SingleElectron/NANOAOD',
    'Run2017E/SingleElectron/NANOAOD',
    'Run2017F/SingleElectron/NANOAOD',

    'Run2017B/SingleMuon/NANOAOD',
    'Run2017C/SingleMuon/NANOAOD',
    'Run2017D/SingleMuon/NANOAOD',
    'Run2017E/SingleMuon/NANOAOD',
    'Run2017F/SingleMuon/NANOAOD',

    'Run2017B/MuonEG/NANOAOD',
    'Run2017C/MuonEG/NANOAOD',
    'Run2017D/MuonEG/NANOAOD',
    'Run2017E/MuonEG/NANOAOD',
    'Run2017F/MuonEG/NANOAOD',

    'Run2017B/DoubleMuon/NANOAOD',
    'Run2017C/DoubleMuon/NANOAOD',
    'Run2017D/DoubleMuon/NANOAOD',
    'Run2017E/DoubleMuon/NANOAOD',
    'Run2017F/DoubleMuon/NANOAOD',

    'Run2017B/DoubleEG/NANOAOD',
    'Run2017C/DoubleEG/NANOAOD',
    'Run2017D/DoubleEG/NANOAOD',
    'Run2017E/DoubleEG/NANOAOD',
    'Run2017F/DoubleEG/NANOAOD'

]

dataSamplesFor2018 = [

    'Run2018B/EGamma/NANOAOD',
    'Run2018C/EGamma/NANOAOD',
    'Run2018D/EGamma/NANOAOD',
    'Run2018E/EGamma/NANOAOD',
    'Run2018A/EGamma/NANOAOD',

    'Run2018B/SingleMuon/NANOAOD',
    'Run2018C/SingleMuon/NANOAOD',
    'Run2018D/SingleMuon/NANOAOD',
    'Run2018E/SingleMuon/NANOAOD',
    'Run2018A/SingleMuon/NANOAOD'

]


eosMC = '/eos/lyoeos.in2p3.fr/grid/cms/store/mc/RunIISummer20UL17NanoAODv9/'
eosData = '/eos/lyoeos.in2p3.fr/grid/cms/store/data/'

for sample in monteCarloSaplesFor2017:
    outputFile = open('/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly2/fly/DSinfo/17/mc/' + 'PROC_' + sample.replace('/','_') + '.txt','w+')
    outputFile.truncate(0)
    for root, dirs, files in os.walk(eosMC + sample):
        for file in files:
            outputFile.write(str(os.path.join(root,file)) + '\n')
    outputFile.close()
    # break


for sample in dataSamplesFor2017:
    outputFile = open('/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly2/fly/DSinfo/17/data/' + sample.replace('/','_').replace('_NANOAOD','') + '.txt','w+')
    outputFile.truncate(0)
    for root, dirs, files in os.walk(eosData + sample):
        for file in files:
            # outputFile.write('xrdcp root://lyoeos.in2p3.fr/' + str(os.path.join(root,file)) + '\n')
            outputFile.write(str(os.path.join(root,file)) + '\n')

    outputFile.close()
    # break