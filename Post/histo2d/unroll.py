import ROOT

def unroll_TH2D(input_file_sig, input_file_back, hist_name, output_file):
    # Charger le fichier ROOT et obtenir les histogrammes 2D
    file_sig = ROOT.TFile.Open(input_file_sig)
    file_back = ROOT.TFile.Open(input_file_back)

    hsig_0 = file_sig.Get(hist_name)
    hsig_1 = file_sig.Get(hist_name[:-1]+'1')
    hsig_2 = file_sig.Get(hist_name[:-1]+'2')
    hsig_0.Add(hsig_1)
    hsig_0.Add(hsig_2)

    hback_0 = file_back.Get(hist_name)
    hback_1 = file_back.Get(hist_name[:-1]+'1')
    hback_2 = file_back.Get(hist_name[:-1]+'2')
    hback_0.Add(hback_1)
    hback_0.Add(hback_2)

    # Obtenir le nombre de bins en X et en Y
    nbinsX = hsig_0.GetNbinsX()
    nbinsY = hsig_0.GetNbinsY()

    # Créer un histogramme 1D avec le nombre total de bins de l'histogramme 2D
    nbinsTotal = nbinsX * nbinsY
    h1_1 = ROOT.TH1D("bdtVSmll_sig", "Signal;BDT score;Events", nbinsTotal, 0, nbinsTotal)
    h1_2 = ROOT.TH1D("bdtVSmll_back", "Background;BDT score Bin;Events", nbinsTotal, 0, nbinsTotal)

    # Remplir les histogrammes 1D à partir des histogrammes 2D
    for i in range(1, nbinsX + 1):
        for j in range(1, nbinsY + 1):
            bin_index = (i - 1) * nbinsY + (j - 1)  # Calculer le bin correspondant dans l'histogramme 1D
            content1 = hsig_0.GetBinContent(i, j)
            content2 = hback_0.GetBinContent(i, j)
            h1_1.SetBinContent(bin_index + 1, content1)  # SetBinContent utilise des indices commençant à 1
            h1_2.SetBinContent(bin_index + 1, content2)  # SetBinContent utilise des indices commençant à 1

    # Désactiver la boîte de statistiques
    h1_1.SetStats(0)
    h1_2.SetStats(0)

    # Créer une canvas pour dessiner les histogrammes
    c1 = ROOT.TCanvas("c1", "Canvas", 1200, 800)
    
    # Dessiner le premier histogramme (Signal) avec une couleur et style spécifique
    h1_1.SetLineColor(ROOT.kBlue)
    h1_1.Draw()

    # Dessiner le deuxième histogramme (Background) avec une autre couleur et style
    h1_2.SetLineColor(ROOT.kRed)
    h1_2.Draw("SAME")  # Utiliser l'option SAME pour superposer les histogrammes

    # Ajouter des lignes de séparation pour chaque bin de X (seulement horizontalement)
    line = ROOT.TLine()
    line.SetLineStyle(2)  # Style de ligne en pointillé
    line.SetLineColor(ROOT.kGreen)

    for i in range(1, nbinsX):
        line.DrawLine(i * nbinsY, 0, i * nbinsY, h1_1.GetMaximum())

    # Ajouter des étiquettes spécifiques centrées dans chaque zone (pour le premier histogramme)
    labels = ["mll < 81 GeV", "81 GeV < mll < 101 GeV", "mll > 101 GeV"]  # Vous pouvez ajouter plus d'étiquettes si nécessaire
    text = ROOT.TText()
    text.SetTextSize(0.03)
    text.SetTextAlign(22)  # Centrer le texte

    for i in range(nbinsX):
        bin_center = (i + 0.5) * nbinsY
        if i < len(labels):
            label = labels[i]
        else:
            label = f"mll{i+1}"
        text.DrawText(bin_center, h1_1.GetMaximum() * 0.6, label)

    # Ajouter des numéros fractionnaires sous les limites des bins sur l'axe des X (pour le premier histogramme)
    fraction_text = ROOT.TLatex()
    fraction_text.SetTextSize(0.02)
    fraction_text.SetTextAlign(22)  # Centrer le texte

    for i in range(nbinsX):
        for j in range(1, nbinsY + 1):
            bin_pos = (i * nbinsY) + j
            if bin_pos <= nbinsTotal:
                fraction_text.DrawLatex(bin_pos - 0.5, -0.03 * h1_1.GetMaximum(), f"{j}/{nbinsY}")

    # Supprimer les numéros de bins sur l'axe des X (pour le premier histogramme)
    h1_1.GetXaxis().SetLabelSize(0)

    # Ajouter une légende
    legend = ROOT.TLegend(0.65, 0.7, 0.85, 0.85)
    legend.SetBorderSize(1)
    legend.SetFillColor(0)
    legend.SetTextSize(0.04)
    legend.AddEntry(h1_1, "Signal", "l")
    legend.AddEntry(h1_2, "Background", "l")
    legend.Draw()

    # Ajouter une légende pour les axes
    c1.Modified()
    c1.Update()

    # Sauvegarder les histogrammes déroulés avec les séparations et la légende
    c1.SaveAs(output_file)

    # Fermer le fichier ROOT
    file_sig.Close()
    file_back.Close()

# Appel de la fonction avec les noms de fichier appropriés
unroll_TH2D("../../results/17/2024-06-28/ready/Signal_006.root", "../../results/17/2024-06-28/ready/Background_006.root", "bdtvsmll_manual_cut0060", "unrolled_histograms.pdf")
