#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

void Branches(const char* filename, const char* treename) {
    // Ouvrir le fichier ROOT
    TFile *file = TFile::Open(filename);

    // Vérifier si le fichier est ouvert correctement
    if (!file || file->IsZombie()) {
        std::cerr << "Erreur lors de l'ouverture du fichier " << filename << std::endl;
        return;
    }

    // Obtenir le TTree à partir du fichier
    TTree *tree = dynamic_cast<TTree*>(file->Get(treename));

    // Vérifier si le TTree est obtenu correctement
    if (!tree) {
        std::cerr << "Erreur lors de la récupération du TTree " << treename << " à partir du fichier." << std::endl;
        file->Close();
        return;
    }

    // Obtenir la liste des branches
    TObjArray *branchList = tree->GetListOfBranches();

    // Afficher les noms des branches
    std::cout << "Liste des branches du TTree '" << treename << "':" << std::endl;
    for (int i = 0; i < branchList->GetEntries(); ++i) {
        TBranch *branch = dynamic_cast<TBranch*>(branchList->At(i));
        if (branch) {
            std::cout << branch->GetName() << std::endl;
        }
    }

    // Fermer le fichier
    file->Close();
}

int listOfBranch() {
    const char* filename = "/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly_3/fly/results/17/2024-03-14/merged/PROC_ALL.root";
    const char* treename = "outputTree";  // Remplacez par le nom de votre TTree

    Branches(filename, treename);

    return 0;
}
