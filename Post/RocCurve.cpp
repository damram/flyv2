#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TGraph.h"
#include <string>
#include "TCanvas.h"
#include <chrono>
#include <math.h>

void fillHistogram(const char* inputFileName, const char* treeName, const char* outputFileName, std::string baseName) {
    TFile *inputFile = TFile::Open(inputFileName);

    TTree *inputTree = (TTree*)inputFile->Get(treeName);

    auto scoreLMVAMuPt10ptr = new vector<double>();
    auto isPromptMuPt10ptr = new vector<int>();
    auto isTightMuPt10ptr = new vector<int>();

    inputTree->SetBranchAddress((baseName+"MvaScore").c_str(), &scoreLMVAMuPt10ptr);
    inputTree->SetBranchAddress(("isPrompt"+baseName).c_str(), &isPromptMuPt10ptr);
    inputTree->SetBranchAddress(("IsTight"+baseName).c_str(), &isTightMuPt10ptr);

    int nPoint=100000;
    double promptEff[nPoint];
    double nonPromptEff[nPoint];
    double nPrompt=0;
    double nNonPrompt=0;
    double nTightPrompt=0;
    // double nTightNonPrompt=0;

    for(int i=0; i<nPoint; i++){
        promptEff[i]=0;
        nonPromptEff[i]=0;
    }

    auto start = std::chrono::steady_clock::now();
    Long64_t totalEvents = inputTree->GetEntries();
    for (Long64_t iEvent = 0; iEvent < totalEvents; ++iEvent) {
        inputTree->GetEntry(iEvent);
        vector<double> scoreLMVA = *scoreLMVAMuPt10ptr;
        vector<int> isPromptMuon = *isPromptMuPt10ptr;
        // vector<int> isTightMuon = *isTightMuPt10ptr;
        if (scoreLMVA.size() != isPromptMuon.size()) cout<<"Error in definition in BaseAnalyser"<<endl;
        // if(iEvent%100000==0) cout<<"processing event "<<iEvent<<"/"<<totalEvents<<" ("<<(iEvent*100)/totalEvents<<"% )"<<endl;
        float progress = (float)iEvent / totalEvents * 100;
        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        float time_per_event = elapsed.count() / (iEvent + 1);
        float remaining_time = time_per_event * (totalEvents - iEvent - 1);
        if((iEvent % (totalEvents/10000))==0) std::cout << "\rProgress: [" << std::string(progress / 2, '#') << std::string(50 - progress / 2, ' ') << "] " << (float)((int)progress*100)/100 << "% | Estimated remaining time: " << round(remaining_time) <<"s    "<<flush;
        for(int j=0; j<scoreLMVA.size(); j++){
             // I create a table for the prompt efficiency. It acts like a histogram, where X is the lmva score and Y is the number of
             // lepton that get that score
             // If the lepton is prompt, I add an entry in the prompt table at the position nPoint-int(scoreLMVA[j]*nPoint)-1
             // This ensure that the Roc Curve is in the common format. Higher scores go to the left and lower scores go to the right
             // so that the Roc curve start at (0;0) and end at (1;1)
            if(isPromptMuon[j]==1){
                promptEff[nPoint-int(scoreLMVA[j]*nPoint)-1]++;
                nPrompt++;
                // if(isTightMuon[j]==1){
                //     nTightPrompt++;
                // }
            }
            else{
                nonPromptEff[nPoint-int(scoreLMVA[j]*nPoint)-1]++;
                nNonPrompt++;
                // if(isTightMuon[j]==1){
                //     nTightNonPrompt++;
                // }
            }
        }
    }
    // I do not want the number of lepton that get a particular score but the number of lepton that get at least the score.
    // Since the table starts at high scores and goes to low scores, I essentially integrate the number 
    // of leptons that get a score between 1 and X, where X is the score of the bin
    // Then, I divide everything by the total number of prompt lepton to get the efficiency
    promptEff[0]*=1/nPrompt;
    nonPromptEff[0]*=1/nNonPrompt;
    for(int i=1; i<nPoint; i++){
        promptEff[i]=(nPrompt*promptEff[i-1]+promptEff[i])/nPrompt;
        nonPromptEff[i]=(nNonPrompt*nonPromptEff[i-1]+nonPromptEff[i])/nNonPrompt;
    }

    TCanvas *canvas = new TCanvas(baseName.c_str(), baseName.c_str(), 800, 600);
    TGraph *graph = new TGraph(nPoint, nonPromptEff, promptEff);
    graph->SetName(baseName.c_str());

    graph->GetXaxis()->SetTitle("Non Prompt eff");
    graph->GetYaxis()->SetTitle("Prompt eff");
    graph->GetXaxis()->SetLimits(0.9e-3, 1);
    graph->SetTitle("RocCurve");
    graph->SetLineColor(kGreen);
    graph->SetLineWidth(2);
    canvas->SetGrid();

    // WP
    int tight = nPoint-int(0.94*nPoint)-1;
    int medium = nPoint-int(0.90*nPoint)-1;
    int loose = nPoint-int(0.81*nPoint)-1;
    int vloose = nPoint-int(0.59*nPoint)-1;
    TGraph *tightPoint = new TGraph();
    tightPoint->SetPoint(0, nonPromptEff[tight], promptEff[tight]);
    tightPoint->SetMarkerStyle(20);
    tightPoint->SetMarkerColor(kRed);

    TGraph *mediumPoint = new TGraph();
    mediumPoint->SetPoint(0, nonPromptEff[medium], promptEff[medium]);
    mediumPoint->SetMarkerStyle(20);
    mediumPoint->SetMarkerColor(kGreen);

    TGraph *loosePoint = new TGraph();
    loosePoint->SetPoint(0, nonPromptEff[loose], promptEff[loose]);
    loosePoint->SetMarkerStyle(20);
    loosePoint->SetMarkerColor(kOrange);

    TGraph *vloosePoint = new TGraph();
    vloosePoint->SetPoint(0, nonPromptEff[vloose], promptEff[vloose]);
    vloosePoint->SetMarkerStyle(20);
    vloosePoint->SetMarkerColor(kBlue);

    // TGraph *pogTightPoint = new TGraph();
    // pogTightPoint->SetPoint(0, (double)nTightNonPrompt/nNonPrompt, (double)nTightPrompt/nPrompt);
    // pogTightPoint->SetMarkerStyle(20);
    // pogTightPoint->SetMarkerColor(kViolet);

    //Legend
    TLegend *legend = new TLegend(0.65, 0.15, 0.9, 0.4);
    legend->AddEntry(graph, "TOP-UL (v2)", "l");
    legend->AddEntry(tightPoint, "Tight", "p");
    legend->AddEntry(mediumPoint, "Medium", "p");
    legend->AddEntry(loosePoint, "Loose", "p");
    legend->AddEntry(vloosePoint, "VLoose", "p");
    // legend->AddEntry(pogTightPoint, "POG tight", "p");

    TFile *outputFile = new TFile(outputFileName, "UPDATE");
    graph->Draw("AL");
    tightPoint->Draw("P SAME");
    mediumPoint->Draw("P SAME");
    loosePoint->Draw("P SAME");
    vloosePoint->Draw("P SAME");
    // pogTightPoint->Draw("P SAME");
    legend->Draw("SAME");
    canvas->SetLogx(); 
    canvas->Update();

    canvas->Write();
    outputFile->Close();

    // Fermer le fichier d'entrée
    inputFile->Close();
}

void RocCurve() {
    // const char* inputFileName = "/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly_3/fly/results/17/2024-03-19/merged/PROC_ALL.root";
    const char* inputFileName = "/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly_3/fly/results/17/2024-03-19/merged/PROC_TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8.root";
    // const char* inputFileName = "/gridgroup/cms/amram/analysis/CMSSW_12_3_7/src/fly_3/fly/results/test_DY.root";
    const char* treeName = "outputTree";
    const char* outputFileName = "rocCurvePres.root";

    std::string baseName="MuPt10";
    cout<<"Analyzing "<<inputFileName<<endl;
    cout<<"Using "<<baseName<<endl;
    fillHistogram(inputFileName, treeName, outputFileName, baseName);

    baseName="ElPt10";
    cout<<"Analyzing "<<inputFileName<<endl;
    cout<<"Using "<<baseName<<endl;
    fillHistogram(inputFileName, treeName, outputFileName, baseName);

    // baseName="goodMuon";
    // cout<<"Analyzing "<<inputFileName<<endl;
    // cout<<"Using "<<baseName<<endl;
    // fillHistogram(inputFileName, treeName, outputFileName, baseName);
}
