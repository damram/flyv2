imax 1 number of bins
jmax 9 number of background processes
kmax * number of nuisance parameters
--------------------------------------------------------------------------------------------- 
shapes * * ./inputs/2017/signal_3l_0060.root $PROCESS $PROCESS_$SYSTEMATIC
shapes sig * ./inputs/2017/signal_3l_0060.root $PROCESS $PROCESS_$SYSTEMATIC
--------------------------------------------------------------------------------------------- 
bin signal_3l
observation 90968.263714
--------------------------------------------------------------------------------------------- 
bin                                 signal_3l            signal_3l            signal_3l            signal_3l            signal_3l            signal_3l            signal_3l            signal_3l            signal_3l            signal_3l            
process                             signal               tt                   ttx                  singletop            multibosons          dy                   WZ                   ZZ                   fake                 conversion           
process                             0                    1                    2                    3                    4                    5                    6                    7                    8                    9                    
rate                                -1                   -1                   -1                   -1                   -1                   -1                   -1                   -1                   -1                   -1                   
--------------------------------------------------------------------------------------------- 
rtt                           lnN   -                    1.10                 1.15                 -                    -                    -                    -                    -                    -                    -                    
rttx                          lnN   -                    -                    1.15                 -                    -                    -                    -                    -                    -                    -                    
rsingletop                    lnN   -                    -                    -                    1.1                  -                    -                    -                    -                    -                    -                    
rmultibosons                  lnN   -                    -                    -                    -                    1.25                 -                    -                    -                    -                    -                    
rdy                           lnN   -                    -                    -                    -                    -                    1.08                 -                    -                    -                    -                    
rWZ                           lnN   -                    -                    -                    -                    -                    -                    1.06                 -                    -                    -                    
rZZ                           lnN   -                    -                    -                    -                    -                    -                    -                    1.10                 -                    -                    
rfake                         lnN   -                    -                    -                    -                    -                    -                    -                    -                    1.06                 -                    
rconversion                   lnN   -                    -                    -                    -                    -                    -                    -                    -                    -                    1.06                 
syst_elec_reco                shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_elec_id                  shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_muon_hlt                 shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
stat_muon_hlt_2017            shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_muon_reco                shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
stat_muon_reco_2017           shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_muon_id                  shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
stat_muon_id_2017             shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_muon_iso                 shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
stat_muon_iso_2017            shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_pu                       shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_b_correlated             shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_b_uncorrelated_2017      shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_l_correlated             shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_l_uncorrelated_2017      shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
syst_prefiring                shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
mescale                       shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
renscale                      shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
facscale                      shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
isr                           shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
fsr                           shape 1                    1                    1                    1                    1                    1                    1                    1                    1                    1                    
lumi_uncor_2017               lnN   1.02                 1.02                 1.02                 1.02                 1.02                 1.02                 1.02                 1.02                 1.02                 1.02                 
lumi_cor_2016_2018            lnN   1.009                1.009                1.009                1.009                1.009                1.009                1.009                1.009                1.009                1.009                
lumi_cor_2017_2018            lnN   1.006                1.006                1.006                1.006                1.006                1.006                1.006                1.006                1.006                1.006                
--------------------------------------------------------------------------------------------- 
* autoMCStats 0
