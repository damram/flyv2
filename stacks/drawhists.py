#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File        : drawhist.py
Developper     : Candan Dozen Altuntas < AT gmail [DOT] com>
Description : drawhist.py
"""
import copy
import ROOT
import stackhists as stackhists
import CMS_lumi
import tdrstyle
import click
import os

def get_prep_stack_hist(year, initial_tdrStyle, rlumi, verbose):
    """Prepare necessary variables with given year

    Args:
        year: given year
        initial_tdrStyle: initialized tdrStyle instance
        rlumi list
    Return:
        s, CMS_lumi.lumi_13TeV, rlumi, runs
    """
    runs = []
    s = None
    if year == "16pre":
        CMS_lumi.lumi_13TeV = "19.5 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 19.5, verbose)
        runs = ["16pre"]

    if year == "16post":
        CMS_lumi.lumi_13TeV = "16.8 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 16.8, verbose)
        runs = ["16post"]

    elif year == "17":
        CMS_lumi.lumi_13TeV = "41.5 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 41.48, verbose)
        runs = ["17"]

    elif year == "18":
        CMS_lumi.lumi_13TeV = "150.0 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 150.0, verbose)
        runs = ["18"]

    elif year == "run2":
        CMS_lumi.lumi_13TeV = "138 fb^{-1}"
        s = stackhists.StackHists(ROOT, initial_tdrStyle, 137.65, verbose)
        rlumi["16pre"] = 19.5 / 137.65
        rlumi["16post"] = 16.8 / 137.65
        rlumi["17"] = 41.48 / 137.65
        # rlumi["18"] = 59.83/137.65
        rlumi["18"] = 150.0 / 137.65
        runs = ["16pre", "16post", "17", "18"]

    # DATA
    if year == "run2":
        print("run2")
        #s.addChannel("Run2"+sys+".root", "data", 999, isMC=False)
    else:

        #s.addChannel("18/Signal_tightID_miniall_tight_SumPtTwoMuons160_DR18_BJetone.root", "data", 999, isMC=False)
        print("no run2")
        #s.addChannel("/home/cms/dozen-altuntas/FLY_project/CMSSW_12_3_2/src/fly/analyzed/test_nano_v1.root", "data", 999,isMC=False)
    #return s, CMS_lumi.lumi_13TeV, rlumi, runs
    return copy.copy(s), CMS_lumi.lumi_13TeV, rlumi, runs


@click.command()
@click.option('--ratio', '-R', is_flag=True, default=False, required=False)
@click.option('--significance', '-S', is_flag=True, default=False, required=False)
@click.option('--logstyle', '-L', is_flag=True, default=False, required=False)
@click.option('--verbose', '-v', is_flag=True, default=False, required=False)
@click.option('--year', '-Y', default="", type=str)
@click.option('--region' ,'-r', default="WZ", type=str)
@click.option('--fake' ,'-f', default="0", type=str)

def main(ratio, significance, logstyle, verbose, year, region, fake):
    click.echo(f'Input Arguments: ratio:{ratio}, significance:{significance}, logstyle:{logstyle}, year:{year}, region:{region}, fake:{fake}')
    print("region is %s" %region)
    if region == "ZZ":
        cut="0000"
        ttXname='rare'
        ttXColor=0
    elif region == "WZ":
        cut="0010"
        ttXname='rare'
        ttXColor=0
    elif region == "Xy":
        cut="0020"
        ttXname='rare'
        ttXColor=0
    elif region== "3l_TTZ":
        cut="0030"
        ttXname='t(t)X'
        ttXColor=7
    elif region== "4l_TTZ":
        cut="0040"
        ttXname='t(t)X'
        ttXColor=7
    elif region== "sig":
        cut="0050"
        ttXname='t(t)X'
        ttXColor=7
    elif region=='sig3l':
        cut="0060"
        ttXname='t(t)X'
        ttXColor=7
    elif region=='sig4l':
        cut="0070"
        ttXname='t(t)X'
        ttXColor=7
    else :
        ttXname='rare'
        cut=region
        ttXColor=0
    #color hist
    rareColor=0
    QCDColor=8
    STColor=rareColor
    WColor=rareColor
    ZZColor=1
    WZColor=2
    XyColor=3
    ttzColor=4
    signame = "tt(Z|gamma*)"
    sigColor=ttzColor
    DYColor=6

    dataColor=999


    # Subplot ( Ratio plot : option -R, --ratio (default) / significance plot :  option -S, --significance )
    initial_tdrStyle = ROOT.TStyle("tdrStyle", "Style for P-TDR")
    #tdrstyle.setTDRStyle(ROOT, initial_tdrStyle)

    sys = "_norm"
    # Lumi ratio dictionary for integrated Run2
    rlumi = {"16pre": 1., "16post": 1., "17": 1., "18": 1.}
    s, CMS_lumi.lumi_13TeV, rlumi, runs = get_prep_stack_hist(year, initial_tdrStyle, rlumi, verbose)

    s.setupStyle(alpha=1)
    CMS_lumi.extraText = ""
    # CMS_lumi.extraText = "Simulation"
    for run in runs:
        if run == "16pre":
            print("Run2016_pre")
            #s.addChannel(run+"/Signal_tightID_miniall_tight_SumPtTwoMuons160_DR18_BJetone.root", "W+jets", 3, isMC=True, xsec=rlumi[run]*61526700*0.9647, counter_histogram_root=run+"/Signal_Candan.root")

        elif run == "16post":
            print("Run2016_post")
            #s.addChannel(run+"/WJetsToLNu_inclHT100_"+run+sys+".root", "W+jets", 3, isMC=True, xsec=rlumi[run]*61526700*0.9647, counter_histogram_root=run+"/WJetsToLNu_inclHT100_"+run+sys+".root")

        elif run == "17":
            print("Run")
            dir = "../results/17/2024-10-28/ready/"
            #s.addChannel(run+"/WJetsToLNu_inclHT100_"+run+sys+".root", "W+jets", 3, isMC=True, xsec=rlumi[run]*61526700*0.9645, counter_histogram_root=run+"/WJetsToLNu_inclHT100_"+run+sys+".root")
            #signal
            s.addChannel(dir + "PROC_TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8_"+cut+".root", signame, ttzColor, isMC=True, xsec=281.36, counter_histogram_root='')# AN2022_035_v12
            s.addChannel(dir + "PROC_TTZToLL_M-1to10_TuneCP5_13TeV-amcatnlo-pythia8_"+cut+".root", signame, ttzColor, isMC=True, xsec=53.7, counter_histogram_root='')# Done
            
            #WZ
            #s.addChannel(dir + "PROC_WZ_TuneCP5_13TeV-pythia8_"+cut+".root", "WZ", WZColor, isMC=True, xsec=47130, counter_histogram_root='')# AN2021_116_v8 (previous was 27590 and worked better)
            s.addChannel(dir + "PROC_WZTo3LNu_TuneCP5_13TeV-amcatnloFXFX-pythia8_"+cut+".root", "WZ", WZColor, isMC=True, xsec=5052, counter_histogram_root='')# AN2022_035_v12
        
            #ZZ
            
            # s.addChannel(dir + "PROC_ZZ_TuneCP5_13TeV-pythia8_"+cut+".root", "ZZ", ZZColor, isMC=True, xsec=12140, counter_histogram_root='')
            s.addChannel(dir + "PROC_ZZTo4L_TuneCP5_13TeV_powheg_pythia8_"+cut+".root", "ZZ", ZZColor, isMC=True, xsec=1256, counter_histogram_root='')# AN2016_442_v8
            s.addChannel(dir + "PROC_WminusH_HToZZTo4L_M125_CP5TuneUp_13TeV_powheg2-minlo-HWJ_JHUGenV7011_pythia8_"+cut+".root", "ZZ", ZZColor, isMC=True, xsec=147, counter_histogram_root= '')# AN2016_442_v8
            s.addChannel(dir + "PROC_WplusH_HToZZTo4L_M125_CP5TuneDown_13TeV_powheg2-minlo-HWJ_JHUGenV7011_pythia8_"+cut+".root", "ZZ", ZZColor, isMC=True, xsec=232, counter_histogram_root='')# AN2016_442_v8
            s.addChannel(dir + "PROC_GluGluHToZZTo4L_M125_CP5TuneDown_13TeV_powheg2_JHUGenV7011_pythia8_"+cut+".root", "ZZ", ZZColor, isMC=True, xsec=1218, counter_histogram_root='')# AN2016_442_v8
            
            #Xy
            s.addChannel(dir + "PROC_ZGToLLG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8_"+cut+".root", "Xy", XyColor, isMC=True, xsec=55480.00, counter_histogram_root='')# Done
            s.addChannel(dir + "PROC_WGToLNuG_01J_5f_TuneCP5_13TeV-amcatnloFXFX-pythia8_"+cut+".root", "Xy", XyColor, isMC=True, xsec=191300.00, counter_histogram_root='')# Done
            #QCD
            # s.addChannel(dir + 'QCD_Pt-15To20_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=3336011*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-20To30_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=3987854.9*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-30To50_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=1705381*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-50To80_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=395178*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-80To120_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=106889.4*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-120To170_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=23773.61*10**3, counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-170To300_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=8292.9*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-300To470_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=797.4*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-470To600_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=56.6*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-600To800_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=25.1*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-800To1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8_'+cut+'.root', 'QCD', QCDColor, isMC=True, xsec=4.7*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-1000_MuEnrichedPt5_TuneCP5_13TeV-pythia8_', 'QCD'+cut+'.root', QCDColor, isMC=True, xsec=1.6*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-20to30_EMEnriched_TuneCP5_13TeV-pythia8_', 'QCD'+cut+'.root', QCDColor, isMC=True, xsec=4948840*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-30to50_EMEnriched_TuneCP5_13TeV-pythia8_', 'QCD'+cut+'.root', QCDColor, isMC=True, xsec=6324800*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-50to80_EMEnriched_TuneCP5_13TeV-pythia8_', 'QCD'+cut+'.root', QCDColor, isMC=True, xsec=1806336*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-80To120_MuEnrichedPt5_TuneCP5_13TeV-pythia8_', 'QCD'+cut+'.root', QCDColor, isMC=True, xsec=380538*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-120to170_EMEnriched_TuneCP5_13TeV-pythia8_', 'QCD'+cut+'.root', QCDColor, isMC=True, xsec=66634.3*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-170to300_EMEnriched_TuneCP5_13TeV-pythia8_', 'QCD'+cut+'.root', QCDColor, isMC=True, xsec=20859*10**3 , counter_histogram_root = '')
            # s.addChannel(dir + 'QCD_Pt-300toInf_EMEnriched_TuneCP5_13TeV-pythia8_', 'QCD'+cut+'.root', QCDColor, isMC=True, xsec=1350*10**3 , counter_histogram_root = '')

            #rare
            s.addChannel(dir + 'PROC_ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=113400, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=67930, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=354900, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=32450, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=32510, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=216.1, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=214.9, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=510.4, counter_histogram_root = '')# Done
            
            #"PROC_TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8.root" ?
            #"PROC_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8.root" ?

            s.addChannel(dir + 'PROC_TTGJets_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8_'+cut+'.root', signame, sigColor, isMC=True, xsec=3757, counter_histogram_root = '')# Done 
            s.addChannel(dir + 'PROC_TTJets_SingleLeptFromTbar_TuneCP5_13TeV-madgraphMLM-pythia8_'+cut+'.root', 'rare', STColor, isMC=True, xsec=105700, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=563.8, counter_histogram_root = '')# Done 
            s.addChannel(dir + 'PROC_TTTT_TuneCP5_13TeV-amcatnlo-pythia8_'+cut+'.root', 'rare', STColor, isMC=True, xsec=8.091, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_THQ_ctcvcp_4f_Hincl_TuneCP5_13TeV_madgraph_pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=745.2, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_THW_ctcvcp_5f_Hincl_TuneCP5_13TeV_madgraph_pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=147.1, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_TTWW_TuneCP5_13TeV-madgraph-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=7.003, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_TTWZ_TuneCP5_13TeV-madgraph-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=2.441, counter_histogram_root = '')# Done
            s.addChannel(dir + 'PROC_TTZZ_TuneCP5_13TeV-madgraph-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=1.386, counter_histogram_root = '')# Done

            s.addChannel(dir + 'PROC_tZq_ll_4f_ckm_NLO_TuneCP5_13TeV-amcatnlo-pythia8_'+cut+'.root', ttXname, ttXColor, isMC=True, xsec=75.61, counter_histogram_root = '')# Done

            s.addChannel(dir + 'PROC_DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8_'+cut+'.root', 'D-Y', DYColor, isMC=True, xsec=6104000, counter_histogram_root = '')# AN2016_442_v8         
            s.addChannel(dir + 'PROC_DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8_'+cut+'.root', 'D-Y', DYColor, isMC=True, xsec=18610000, counter_histogram_root = '')# AN2016_442_v8           
            s.addChannel(dir + 'PROC_WWW_4F_TuneCP5_13TeV-amcatnlo-pythia8_'+cut+'.root', 'rare', STColor, isMC=True, xsec=215.8, counter_histogram_root = '')# AN2022_035_v12      
            s.addChannel(dir + 'PROC_WWZ_4F_TuneCP5_13TeV-amcatnlo-pythia8_'+cut+'.root', 'rare', STColor, isMC=True, xsec=165.1, counter_histogram_root = '')# AN2022_035_v12     
            s.addChannel(dir + 'PROC_WZZ_TuneCP5_13TeV-amcatnlo-pythia8_'+cut+'.root', 'rare', STColor, isMC=True, xsec=55.65, counter_histogram_root = '')# Done   
            s.addChannel(dir + 'PROC_ZZZ_TuneCP5_13TeV-amcatnlo-pythia8_'+cut+'.root', 'rare', STColor, isMC=True, xsec=13.98, counter_histogram_root = '')# Done      

            #W
            s.addChannel(dir + 'PROC_WW_TuneCP5_13TeV-pythia8_'+cut+'.root', 'rare', WColor, isMC=True, xsec=75800 , counter_histogram_root = '')# AN2022_035_v12
            s.addChannel(dir + 'PROC_WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8_'+cut+'.root', 'rare', WColor, isMC=True, xsec=61526700 , counter_histogram_root = '')# AN2022_035_v12


            #Data
            if (int(cut)<30):
                print('#################################')
                print('data added')
                # s.addChannel(dir + 'RunB_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunB_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunB_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunB_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunB_'+cut+'.root', 'data', dataColor, isMC=False)

                # s.addChannel(dir + 'RunC_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunC_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunC_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunC_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunC_'+cut+'.root', 'data', dataColor, isMC=False)

                # s.addChannel(dir + 'RunD_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunD_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunD_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunD_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunD_'+cut+'.root', 'data', dataColor, isMC=False)

                # s.addChannel(dir + 'RunE_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunE_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunE_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunE_'+cut+'.root', 'data', dataColor, isMC=False)
                # s.addChannel(dir + 'RunE_'+cut+'.root', 'data', dataColor, isMC=False)

                # s.addChannel(dir + 'RunF_'+cut+'.root', 'dataSM', dataColor, isMC=False)
                # s.addChannel(dir + 'RunF_'+cut+'.root', 'dataDM', dataColor, isMC=False)
                # s.addChannel(dir + 'RunF_'+cut+'.root', 'dataSE', dataColor, isMC=False)
                # s.addChannel(dir + 'RunF_'+cut+'.root', 'dataDEG', dataColor, isMC=False)
                # s.addChannel(dir + 'RunF_'+cut+'.root', 'dataMEG', dataColor, isMC=False)
                # s.addChannel(dir + 'RunALL_'+cut+'.root', 'data', dataColor, isMC=False)
                print('#################################')





        elif run == "18":
            print("Run2018")
            #MC_Signal
            
        # cut='002'
        # Region Histograms
        # s.addHistogram("hngoodElectron_cut"+cut, "N_goode"+cut, "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hNgoodMuon_cut"+cut, "N_goodMu"+cut, "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        

        # if region == "WZ":
        #     print("cut is ", cut)
        #     s.addHistogram("hRegionWZ_cut"+cut, "N_e", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # if region =="ZZ":
        #     s.addHistogram("hRegion4lZZ_cut"+cut, "N_e/2", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # if region =="Xy":
        #     s.addHistogram("hRegionconversion_cut"+cut, "N_e", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # if region =="3l_TTZ":
        #     s.addHistogram("hRegion3lTTZ_cut"+cut, "N_j (/ N_b)", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        #     s.addHistogram("hRegion3lTTZ0e_cut"+cut, "N_j (/ N_b) 0e", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=True)
        #     s.addHistogram("hRegion3lTTZ1e_cut"+cut, "N_j (/ N_b) 1e", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=True)
        #     s.addHistogram("hRegion3lTTZ2e_cut"+cut, "N_j  (/ N_b) 2e", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=True)
        #     s.addHistogram("hRegion3lTTZ3e_cut"+cut, "N_j (/ N_b) 3e", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=True)
        #     s.addHistogram("hRegionTTZ_cut"+cut, "N_j (/ N_b)", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=True)



        # if region =="4l_TTZ":
        #     s.addHistogram("hRegion4lTTZ_cut"+cut, "N_b", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        #     s.addHistogram("hNgood_bjets_2_cut"+cut, "N_Bjets", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        #     #s.addHistogram("hNgoodJets_2_cut"+cut, "N_jets", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)


        # if region !="sig":
        #     s.addHistogram("hNtightJets_cut"+cut, "Njets", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # if region =="sig":
        #     s.addHistogram("hNtightJets_cut"+cut, "Njets", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=True)

        # s.addHistogram("hNgood_bjets_cut"+cut, "N_Bjets", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hLeadingLepton_pt_cut"+cut, "Leading lepton p_T [GeV]", "Entries / 20 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hSub_leadingLepton_pt_cut"+cut, "Sub-leading lepton p_T [GeV]", "Entries / 10 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hTrailing_lepton_pt_cut"+cut, "Trailing lepton p_T [GeV]", "Entries / 10 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        #s.addHistogram("hRegionTTZ_cut"+cut, "N_j + 4xN_b - 1", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=True)

        # if region =="sig":
        #     s.addHistogram("hZ_pt_cut"+cut, "Pt,Z [GeV]", "Entries / 25 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        #     s.addHistogram("hLeptonsNoZ_Pt_high_cut"+cut, "Pt,non-Z [GeV]", "Entries / 10 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        #     s.addHistogram("hRegion_sig_3l_cut"+cut, "Ne", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        #     s.addHistogram("hRegion_sig_4l_cut"+cut, "Ne", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hinvMassOSSF_pt_cut"+cut, "M(ll) [GeV]", "Entries / 2 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # if region == "WZ":
        #     s.addHistogram("hZ_pt_cut"+cut, "Pt,Z [GeV]", "Entries / 20 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        #     s.addHistogram("hLeptonsNoZ_Pt_high_cut"+cut, "Pt,non-Z [GeV]", "Entries / 10 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        
        # s.addHistogram("hgoodElectron_eta_cut"+cut, "Eta,Electrons", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hgoodMuon_eta_cut"+cut, "Eta,Muons", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hgoodMuon_phi_cut"+cut, "Phi,Muons", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hgoodElectron_phi_cut"+cut, "Phi,Electrons", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("htightJets_eta_cut"+cut, "Eta,Jets", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("htightJets_phi_cut"+cut, "Phi,Jets", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("htightJets_mass_cut"+cut, "Mass,Jets [GeV]", "Entries/2 GeV", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("hall_leptons_eta_cut"+cut, "Eta,leptons", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)

        # Combine variables #PB : only for MC since in data they have another name. The names should be the sames in the histograms for the framework ? Need to thing how to do it
        s.addHistogram("signal_3l_evWeight_cut"+cut, "nJetsObservable (+bJets) (3*4 bins) (3l)", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=True)
        # s.addHistogram("signal_4l_evWeight_cut"+cut, "nCleanBJets (4l)", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("ZZ_evWeight_cut"+cut, "NcleanJets+NcleanBJets (2*2 bins) (ZZ)", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("WZ_evWeight_cut"+cut, "NcleanJets (WZ)", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
        # s.addHistogram("Nonprompt_evWeight_cut"+cut, "nJets_Obsnonprompt + 3 (np)", "Entries", draw_mode=stackhists.STACKED, draw_option="hist", is_logy=False)
    

    # Jet Histograms
    # Leading Jet
    # s.addHistogram("h1jet1pt_cut000", "p_{T} of Leading Jet (GeV)", "Entries", drawmode=stackhists.STACKED, drawoption="hist", isLogy=logstyle, ymin=0.1)

    subplot = "R"
    if ratio:
        subplot = "R"
    elif significance:
        subplot = "S"
    print('before Draw')
    s.draw(subplot)
    print('after draw')
    os.system('mv plots.pdf plotPdf/plots_'+region+'_'+cut+'.pdf')
    os.system('cp plotPdf/plots_'+region+'_'+cut+'.pdf ' + dir)
    print('Created plots_'+region+'_'+cut+'.pdf')


if __name__ == '__main__':
    ROOT.gROOT.SetBatch(True)
    main()
