#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ROOT import RDataFrame
import numpy as np
import math
import os
import sys
import CMS_lumi
import array
import tdrstyle
from array import array
import pandas as pd
import matplotlib.pyplot as plt
import ROOT
from pathlib import Path

ROOT.TH1D.AddDirectory(False)

STACKED = 1
NORMALIZED = 0

# Colors
BLACK = "\u001b[0;30m"
RED = "\u001b[0;31m"
HRED = "\u001b[1;31m"
GREEN = "\u001b[0;32m"
YELLOW = "\u001b[0;33m"
BLUE = "\u001b[0;34m"
PURPLE = "\u001b[0;35m"
CYAN = "\u001b[0;36m"
WHITE = "\u001b[0;37m"
GRAY = "\u001b[0;90m"
LRED = "\u001b[1;91m"
LGREEN = "\u001b[1;92m"
LYELLOW = "\u001b[1;93m"
LBLUE = "\u001b[1;94m"
LPURPLE = "\u001b[1;95m"
LCYAN = "\u001b[1;96m"
BWHITE = "\u001b[1;97m"
RESET = "\u001b[0m"
DGRAY = "\u001b[1;30m"
LGRAY = "\u001b[0;37m"
BRED = "\u001b[1;31m"
BGREEN = "\u001b[1;32m"
BYELLOW = "\u001b[1;33m"
BBLUE = "\u001b[1;34m"
BPURPLE = "\u001b[1;35m"
BCYAN = "\u001b[1;36m"
# Reset
RESET = "\033[0m"


class StackHists:

    def __init__(self,
                 ROOT, 
                 tdrStyle, 
                 integer_lumi, 
                 verbose=False
                 ):
        self.verbose = verbose
        self.c1, self.data_file, self.fill_alpha = None, None, None

        self.mc_file_list, self.mc_label_list = [], []
        self.mc_color_list, self.color_list, self.mc_pattern_list, self.pattern_list = [], [], [], []
        self.sum_of_weights_hist_name = ''

        # QCD Fit specific objects
        self.mc_qcd_files_forFit, self.mc_qcd_files_inQCDregion_forFit, self.mc_qcd_xsec_forFit, self.mc_qcd_sf_forFit = [], [], [], []
        self.histName_forFit = ''
        self.preFit = False
        self.QCDregion_folder = ''

        self.data_file_list = []  # you can have more than one data file
        self.xsec_list, self.event_num_list, self.sf_list, self.resf_list, self.sumOfWeights_list = [], [], [], [], []

        self.mc_root_files, self.mc_counter_hist_files, self.mc_file_counter_hist_list = [], [], []
        self.data_root_files = []

        self.integer_lumi = integer_lumi

        self.stackOrder = [] # Order of the stack histogram stack contributions

        # bounding box location for the legends
        self.legend_x1, self.legend_y1, self.legend_x2, self.legend_y2 = 0.32, 0.72, 0.89, 0.89

        self.histogram_list, self.x_titles, self.y_titles, self.draw_modes, self.draw_options = [], [], [], [], []
        self.is_logy, self.is_logx, self.bin_lists, self.underflow_bin = [], [], [], []
        self.ymin, self.ymax = [], []  # histogram maximum values

        # Initialize tdrStyle
        self.tdrStyle = tdrstyle.setTDRStyle(ROOT, tdrStyle)

        # Set tdrStyle
        ROOT.gROOT.SetStyle("tdrStyle")
        self.ROOT = ROOT

        self.extra_text_hists = []

        # Stuff for plotter
        self.iso_cut_iso_mu = ''
        self.iso_cut_rev_iso_mu = ''

        # Print all defined variables if verbosity is requested
        if verbose:
            print("~~~~~~~~~~~~~~~~~ StackHists class attributes ~~~~~~~~~~~~~~~~~")
            for k, v in self.__dict__.items():
                print(k, ":", v)
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    def __del__(self):
        print("Destruct")

    def make_plot(self, histograms_info, histname, outFile):
        print("\033[91mInside make_plot function\033[0m")

        canva = ROOT.TCanvas("", "", 1200, 900)
        legend = ROOT.TLegend(0.5, 0.6, 0.95, 0.95)  # Adjust legend position
        max_y = 0.0  # Variable to store the maximum y-height

        for i, info in enumerate(histograms_info):
            histogram, color, title = info
            histogram.SetStats(ROOT.kFALSE)
            histogram.SetLineColor(color)
            histogram.SetLineWidth(2)  # Increase line width for better visibility
            histogram.SetMarkerStyle(20)  # Add markers
            histogram.SetMarkerColor(color)
            histogram.SetMarkerSize(1.0)
            if i == 0:
                histogram.Draw()
            else:
                histogram.Draw("SAME")

            # Update max_y if the histogram's maximum value is greater
            if histogram.GetMaximum() > max_y:
                max_y = histogram.GetMaximum()

            legend.AddEntry(histogram, title, "l")
            if 'cut' in histname:
                histname.replace('_cut000', '')

        histograms_info[0][0].GetXaxis().SetTitle(histname)
        histograms_info[0][0].GetXaxis().SetTitleSize(0.04)  # Adjust axis label size
        histograms_info[0][0].GetXaxis().SetTitleOffset(1.2)  # Adjust axis label position

        histograms_info[0][0].GetYaxis().SetTitle("Normalized")  # Add a label to the y-axis
        histograms_info[0][0].GetYaxis().SetTitleSize(0.04)  # Adjust axis label size
        histograms_info[0][0].GetYaxis().SetTitleOffset(1.4)  # Adjust axis label position

        # Set the maximum y-axis value 1.4 times above max_y
        max_y *= 1.4
        histograms_info[0][0].GetYaxis().SetRangeUser(0, max_y)

        legend.Draw()
        canva.SaveAs(outFile)

    def prepare_QCDfit(self, outPath):
        zipped_lists = zip(self.mc_file_list,
                           self.mc_root_files, 
                           self.sf_list, 
                           range(len(self.xsec_list)))
        
        qcd_zipped_list = zip(self.mc_qcd_files_forFit,
                              self.mc_qcd_xsec_forFit,
                              range(len(self.mc_qcd_xsec_forFit)))
        
        dataDriven_normalization_fromMC = 0.0
        QCDmc_in_RegRegion_h = self.ROOT.TH1D()
        for mc_qcd_file, xsec_qcd, range_id_qcd in qcd_zipped_list:
            # print(f'{GREEN}mc_qcd_file = {mc_qcd_file}{RESET}')
            # print(f'{GREEN}xsec_qcd = {xsec_qcd}{RESET}')
            # print(f'{GREEN}range_id_qcd = {range_id_qcd}{RESET}')
            file = self.ROOT.TFile.Open(str(mc_qcd_file))
            hist_forFit = file.Get(self.histName_forFit)
            if hist_forFit is None:
                print('Could not fetch histogram for fit')
                exit(1)
            sumOfWeights_h = file.Get(self.sum_of_weights_hist_name)
            sumOfWeights = sumOfWeights_h.GetMaximum()
            scaling = (xsec_qcd * self.integer_lumi)/sumOfWeights
            # print(f'{BCYAN} Weights 1: {scaling}{RESET}')
            hist_forFit.Scale(scaling)
            dataDriven_normalization_fromMC += hist_forFit.Integral()
            QCDmc_in_RegRegion_h.Add(hist_forFit)
            file.Close()
        print(f'{GREEN}QCD dataDriven normalization = {dataDriven_normalization_fromMC}{RESET}')

        if dataDriven_normalization_fromMC==0.0:
            print('dataDriven normalization scaling from QCD MC is nul.\nExiting program.')
            exit(1)

        
        counterForDataFiles = 0   
        for ifile in self.data_file_list:
            atfile = self.ROOT.TFile(ifile)
            print(f'{BGREEN}Using {ifile} for Data for QCD fit{RESET}')
            data_hist_forFit = atfile.Get(self.histName_forFit)
            counterForDataFiles += 1
        data_hist_integral = data_hist_forFit.Integral()
        
        if counterForDataFiles>1: 
            print(f'{BLUE}Careful, there is more than 1 root file during QCD fit{RESET}')

        qcd_data_driven_processes = []
        wjets_processes = []
        wjets_nJ_processes = []
        remaining_processes = []

        nbins = data_hist_forFit.GetNbinsX()
        xmin = data_hist_forFit.GetXaxis().GetXmin()
        xmax = data_hist_forFit.GetXaxis().GetXmax()

        wjets_nJ_DY_h = self.ROOT.TH1D('wjets_nJ', 'wjets_nJ', nbins, xmin, xmax)
        other_mc_h = self.ROOT.TH1D('other_mc', 'other_mc', nbins, xmin, xmax)
        QCDdD_h = self.ROOT.TH1D('qcd_dd', 'qcd_dd', nbins, xmin, xmax)

        # Iterate over the zipped list
        for mc_file_name, mc_TFile, sf, idx in zipped_lists:
            # Check the process name and categorize accordingly
            if 'QCD_dataDriven' in mc_file_name:
                hist = mc_TFile.Get(self.histName_forFit)
                dataDriven_integral_before_anyScaling = hist.Integral()
                print(f'{GREEN}DataDriven Integral Before Scaling it to MC: {dataDriven_integral_before_anyScaling}{RESET}')
                QCDdD_h.Add(hist)
                qcd_data_driven_processes.append((mc_file_name, sf, idx))
            elif 'PROC_WJets' in mc_file_name or 'DY' in mc_file_name:
                hist = mc_TFile.Get(self.histName_forFit)
                hist.Scale(sf)
                wjets_nJ_DY_h.Add(hist)
                wjets_nJ_processes.append((mc_file_name, sf, idx))
            else:
                hist = mc_TFile.Get(self.histName_forFit)
                hist.Scale(sf)
                other_mc_h.Add(hist)
                remaining_processes.append((mc_file_name, sf, idx))

        # print(f'qcd_data_driven_processes={qcd_data_driven_processes}')
        # print(f'wjets_processes={wjets_processes}')
        # print(f'remaining_processes={remaining_processes}')
        QCDdD_h.Scale(dataDriven_normalization_fromMC/dataDriven_integral_before_anyScaling)
        QCD_dD_h_copy_forPlot = QCDdD_h.Clone()

        all_mc = self.ROOT.TObjArray()
        all_mc.Add(other_mc_h)
        all_mc.Add(wjets_nJ_DY_h)
        all_mc.Add(QCDdD_h)

        other_mc_h_integral = other_mc_h.Integral()
        wjets_nJ_DY_integral = wjets_nJ_DY_h.Integral()
        QCDdD_h_integral = QCDdD_h.Integral()
        print(f'{GREEN}DataDriven Integral After Scaling it to MC = {QCDdD_h_integral}{RESET}')

        ratio_all_other_mc = other_mc_h_integral/data_hist_integral
        ratio_wjets_nJ_DY = wjets_nJ_DY_integral/data_hist_integral
        ratio_QCDdataDriven = QCDdD_h_integral/data_hist_integral
        print(f'{GREEN}ratio_all_other_mc = {ratio_all_other_mc}')
        print(f'ratio wjets_nJ = {ratio_wjets_nJ_DY}')
        print(f'ratio_QCDdataDriven = {ratio_QCDdataDriven}{RESET}')
        
        if not self.preFit:
            fit = self.ROOT.TFractionFitter(data_hist_forFit, all_mc)
            # Set constraints
            fit.Constrain(0, ratio_all_other_mc - ratio_all_other_mc*0.5, ratio_all_other_mc + ratio_all_other_mc*0.5) # All other MC (Mostly TTBar)
            fit.Constrain(1, ratio_wjets_nJ_DY - ratio_wjets_nJ_DY*0.5, ratio_wjets_nJ_DY + ratio_wjets_nJ_DY*0.5) # WJets_nJ + DY
            fit.Constrain(2, 0, ratio_QCDdataDriven*2) # QCD DataDriven

            fit.SetRangeX(int(xmin),int(xmax))
            status = fit.Fit()
            print(f'{BYELLOW}status: {status}{RESET}')

            # Define C-style double precision variables
            import ctypes
            param0 = ctypes.c_double(0.0)
            param1 = ctypes.c_double(0.0)
            param2 = ctypes.c_double(0.0)
            param0_error = ctypes.c_double(0.0)
            param1_error = ctypes.c_double(0.0)
            param2_error = ctypes.c_double(0.0)

            # Call GetResult() with the address of the double precision variables
            fit.GetResult(0, param0, param0_error)
            fit.GetResult(1, param1, param1_error)
            fit.GetResult(2, param2, param2_error)

            # Extract the double precision values from the ctypes variables
            param0 = param0.value
            param1 = param1.value
            param2 = param2.value
            param0_error = param0_error.value
            param1_error = param1_error.value
            param2_error = param2_error.value

            # print(f'param0={param0}')
            # print(f'param1={param1}')

            all_mc_SF = data_hist_integral * param0 / other_mc_h_integral
            wjets_nJ_SF = data_hist_integral * param1 / wjets_nJ_DY_integral
            QCD_dataDriven_SF = (data_hist_integral * param2 / QCDdD_h_integral)

            dataDriven_normalization = dataDriven_normalization_fromMC/dataDriven_integral_before_anyScaling
            # Create and save scale factors table
            table_text = f"""
            +---------------------+----------------------------+
            | Variable            | Value                      |
            +---------------------+----------------------------+
            | all_other_mc_sf     | {all_mc_SF:.5f}    |
            | wjets_nJ_SF         | {wjets_nJ_SF:.5f}    |
            | QCD_dataDriven_sf   | {QCD_dataDriven_SF:.5f}    |
            +---------------------+----------------------------+
            """
            file_path = outPath + 'A_scaleFactors' + CMS_lumi.outputFileName + '.txt'
            with open(file_path, "w") as f:
                f.write(table_text)


        else:
            print(f'{BPURPLE}Inside preFit{RESET}')
            CMS_lumi.extraText += 'prefit'
            CMS_lumi.outputFileName += '_prefit'
            all_mc_SF = 1.0
            wjets_nJ_SF = 1.0
            QCD_dataDriven_SF = 1.0
            dataDriven_normalization = dataDriven_normalization_fromMC/dataDriven_integral_before_anyScaling

        print("all_other_mc_sf =", f'{all_mc_SF:.2f}')
        print("wjets_nJ_SF =", f'{wjets_nJ_SF:.2f}')
        print("QCD_dataDriven_sf =", f'{QCD_dataDriven_SF:.2f}')

        # print(f'qcd_data_driven_processes={qcd_data_driven_processes}')
        # print(f'wjets_processes={wjets_processes}')
        # print(f'remaining_processes={remaining_processes}')

        # Applying QCD_dataDriven_SF to QCD data driven processes
        for qcd_data_driven_process in qcd_data_driven_processes:
            print(f'{GREEN}Applying dataDriven_normalization to {qcd_data_driven_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[qcd_data_driven_process[2]] *= QCD_dataDriven_SF * dataDriven_normalization

        for wjets_nJ_process in wjets_nJ_processes:
            print(f'{GREEN}Applying wjets_nJ_SF to {wjets_nJ_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[wjets_nJ_process[2]] *= wjets_nJ_SF

        # Applying all_mc_SF to remaining processes
        for remaining_process in remaining_processes:
            print(f'{GREEN}Applying all_other_mc_sf to {remaining_process[0].split("/")[-1].replace(".root","")}{RESET}')
            self.sf_list[remaining_process[2]] *= all_mc_SF

        mcQCD_inQCDregion_hist = self.get_QCDmc_fromQCD_region_hist(outPath+self.QCDregion_folder)
        QCD_dD_h_copy_forPlot.Scale(1/QCD_dD_h_copy_forPlot.Integral())
        QCDmc_in_RegRegion_h.Scale(1/QCDmc_in_RegRegion_h.Integral())
        mcQCD_inQCDregion_hist.Scale(1/mcQCD_inQCDregion_hist.Integral())
        histosToPlot = [
        [QCD_dD_h_copy_forPlot, ROOT.kRed, "Data Driven QCD I_{rel} " + self.iso_cut_rev_iso_mu],
        [QCDmc_in_RegRegion_h, ROOT.kBlue, "QCD in WJets Region I_{rel} " + self.iso_cut_iso_mu],
        [mcQCD_inQCDregion_hist, ROOT.kGreen, "QCD in QCD region I_{rel} " + self.iso_cut_rev_iso_mu]
        ]

        self.make_plot(histosToPlot, self.histName_forFit, f"{outPath}/comparison_QCD_plot" + CMS_lumi.outputFileName + ".png")

    def get_QCDmc_fromQCD_region_hist(self, inPath):
        
        histToReturn = self.ROOT.TH1D()
        for rootFile, xsec in zip(self.mc_qcd_files_inQCDregion_forFit,
                                  self.mc_qcd_xsec_forFit):
            QCDfile_inQCDreg = self.ROOT.TFile.Open(str(rootFile))
            hist = QCDfile_inQCDreg.Get(self.histName_forFit)
            sumOfWeights_h = QCDfile_inQCDreg.Get(self.sum_of_weights_hist_name)
            sumOfWeights = sumOfWeights_h.GetMaximum()
            scale = (self.integer_lumi*xsec)/sumOfWeights
            hist.Scale(scale)
            histToReturn.Add(hist)
            # print(f'{BCYAN} Weights 1: {scale}{RESET}')

            QCDfile_inQCDreg.Close()
        
        return histToReturn

    def prepare_root_files(self):
        """Open ROOT files where histograms reside in MC and data
        """
        for mc_file, mc_cntr_hist_file in zip(self.mc_file_list, self.mc_file_counter_hist_list):
            t_file_mc = self.ROOT.TFile(mc_file)
            if mc_cntr_hist_file == "":
                mc_cntr_hist_file = mc_file
            t_file_mc_cntr_hist = self.ROOT.TFile(mc_cntr_hist_file)
            self.mc_root_files.append(t_file_mc)
            self.mc_counter_hist_files.append(t_file_mc_cntr_hist)

        """
        for mc_file in self.data_file_list:
            t_file_mc = self.ROOT.TFile(mc_file)
            self.data_root_files.append(t_file_mc)
        """

    def open_root_file(self, file_path, mode='READ'):
        try:
            return self.ROOT.TFile(file_path, mode)
        except Exception as e:
            print(f"Error opening file {file_path}: {e}")
            exit(1)

    def calculate_sum_of_weights(self, t_file, mc_file, is_data_driven):
        if is_data_driven:
            return 1.0
        else:
            histogram = t_file.Get(self.sum_of_weights_hist_name)
            if histogram:
                return histogram.GetBinContent(1)
            else:
                print(f"Histogram {self.sum_of_weights_hist_name} not found in {mc_file}")
                exit(1)

    def process_mc_file(self, mc_file, mc_cntr_hist_file, xsec, range_id, is_data_driven):
        t_file_mc_cntr_hist = self.open_root_file(mc_cntr_hist_file)
        if not t_file_mc_cntr_hist:
            return

        sum_of_weights = self.calculate_sum_of_weights(t_file_mc_cntr_hist, mc_cntr_hist_file, is_data_driven)
        if sum_of_weights is None or sum_of_weights==0:
            print(f'sumOfWeights not found.\n Exiting process.')
            exit(1)

        scaling_factor = 1 if is_data_driven else (xsec * self.integer_lumi / sum_of_weights)
        self.sf_list[range_id] *= scaling_factor
        self.sumOfWeights_list[range_id] = sum_of_weights
        print(f"{mc_file.split('/')[-1].replace('.root', '')} scaling factor: {self.sf_list[range_id]}\n {RED}{self.sum_of_weights_hist_name} = {self.sumOfWeights_list[range_id]}{RESET}\n {BRED}xsec = {xsec} pb{RESET}")

    def prepare_scaling_factors(self):
        zipped_lists = zip(self.mc_file_list, 
                           self.mc_file_counter_hist_list, 
                           self.xsec_list, 
                           range(len(self.xsec_list)))

        for mc_file, mc_cntr_hist_file, xsec, range_id in zipped_lists:
            is_data_driven = 'dataDriven' in mc_file
            mc_cntr_hist_file = mc_file if mc_cntr_hist_file == "" else mc_cntr_hist_file
            self.process_mc_file(mc_file, mc_cntr_hist_file, xsec, range_id, is_data_driven)

    def setupStyle(self, color_list=None, pattern_list=None, alpha=1.0):
        """Setup style
        """
        self.fill_alpha = alpha
        if color_list is None:
            self.color_list = [self.ROOT.TColor.GetColor('#BF0000'), # Signal
                               self.ROOT.TColor.GetColor('#00de00'), #alt W/Z-Jets
                               self.ROOT.TColor.GetColor('#660000'), 
                               self.ROOT.TColor.GetColor('#ff8c00'), # TTBar
                               self.ROOT.TColor.GetColor('#FFBF00'), # Single-top tW and s-channel
                               self.ROOT.TColor.GetColor('#7393B3'), # ttX
                               self.ROOT.TColor.GetColor('#00cccc'), 
                               self.ROOT.TColor.GetColor('#ff66ff'),
                               self.ROOT.TColor.GetColor('#cccccc'), # QCD
                               self.ROOT.TColor.GetColor('#000000'),
                               self.ROOT.TColor.GetColor('#339933'), # W/Z-Jets
                               self.ROOT.TColor.GetColor('#6B8551'),
                               self.ROOT.TColor.GetColor('#908DCC'), 
                               self.ROOT.TColor.GetColor('#4F4D80'),
                               self.ROOT.TColor.GetColor('#0000FF'), # Diboson
                               ]
        else:
            self.color_list = color_list

        if pattern_list is None:
            for i in range(10):
                self.pattern_list.append(1001)
        else:
            self.pattern_list = pattern_list

        # TODO check if it is still working
        # self.ROOT.gROOT.SetStyle("tdrStyle")
        # tdrstyle.setTDRStyle()
        pass

    def addChannel(self, root_file, label, color_index, pattern_index=0, isMC=True, xsec=1.0, event_num=1.0,
                   scale_factor=1.0, counter_histogram_root="", sumOfWeights=1.0):
        """Add channel
        """
        if os.path.isfile(root_file):
            if isMC:
                self.mc_file_list.append(root_file)
                self.mc_file_counter_hist_list.append(counter_histogram_root)
                self.mc_label_list.append(label)  # if same label, then the histograms will be added together
                self.mc_color_list.append(color_index)
                self.mc_pattern_list.append(pattern_index)
                self.xsec_list.append(xsec)
                self.event_num_list.append(event_num)
                self.sf_list.append(scale_factor)
                self.resf_list.append(scale_factor)
                self.sumOfWeights_list.append(sumOfWeights)
            else:
                self.data_file_list.append(root_file)
        else:
            print(f'Cannot add file {root_file}, it does not exist')
            print('Please Check')
            sys.exit(-1)
        pass

    def add_mcQCD_channel_forFit(self, root_file, xsec=1.0):
        """Add QCD Monte Carlo Channel for QCD Fit
        """
        if os.path.isfile(root_file):
            self.mc_qcd_files_forFit.append(root_file)
            self.mc_qcd_xsec_forFit.append(xsec)
        else:
            print(f'Cannot add file {root_file}, it does not exist')
            print('Please Check')
            sys.exit(-1)

        parts = root_file.split('/')
        if parts[-1].endswith('.root'):
            parts.insert(-1, self.QCDregion_folder.replace('/',''))
        
        root_file_inQCDregion = '/'.join(parts)

        if os.path.isfile(root_file_inQCDregion):
            self.mc_qcd_files_inQCDregion_forFit.append(root_file_inQCDregion)
            print(f'adding file {root_file_inQCDregion}')
        else:
            print(f'Cannot add file {root_file_inQCDregion}, it does not exist')
            print('Please Check')
            sys.exit(-1)

        pass 
 
    def addHistogram(self, hist_name, x_title="", y_title="", draw_mode=STACKED, draw_option="", is_logy=False, is_logx=False, underflow_bin=False,
                     ymin=-1111, ymax=-1111, binlist=[], extratext=''):
        """Add histogram
        """
        self.histogram_list.append(hist_name)
        self.x_titles.append(x_title)
        self.y_titles.append(y_title)
        self.draw_modes.append(draw_mode)
        self.draw_options.append(draw_option)
        self.is_logy.append(is_logy)
        self.is_logx.append(is_logx)
        self.underflow_bin.append(underflow_bin)
        self.ymin.append(ymin)
        self.ymax.append(ymax)
        self.bin_lists.append(binlist)
        self.extra_text_hists.append(extratext)

    def MakeSoverB(self, BG_hist_stack, S_hist, label, S_peak_bin, below=None):
        """Make Sover B"""
        n_bins = BG_hist_stack.GetNbinsX()
        BG_peak_bin = BG_hist_stack.GetMaximumBin()
        cut_info = "x<cut"
        if below is None:
            if S_peak_bin < BG_peak_bin or S_peak_bin < n_bins / 2:
                below = True
            else:
                below = False
                cut_info = "x>cut"

        BG_int = self.MakeCumulative(BG_hist_stack, 1, n_bins + 1, below)
        S_int = self.MakeCumulative(S_hist, 1, n_bins + 1, below)

        s_over_b = BG_int.Clone()
        s_over_b.Reset()

        arr = label.split('x', 1)
        M = 1.0
        if len(arr) > 1:
            M = float(arr[1])

        for ix in range(1, n_bins + 1):
            val = 0
            bin_ctx = BG_int.GetBinContent(ix)
            if bin_ctx > 0:
                val = (S_int.GetBinContent(ix) / M) / math.sqrt(BG_int.GetBinContent(ix))

            if self.verbose:
                print(f"BG_int bin context: {bin_ctx}, value: {val}")

            s_over_b.SetBinContent(ix, val)

        return s_over_b, cut_info

    def MakeCumulative(self, hist, low, high, below):
        out = hist.Clone(hist.GetName() + '_cumul')
        out.Reset()
        prev = 0
        if below:
            to_scan = range(low, high)
        else:
            to_scan = range(high - 1, low - 1, -1)
        for ix in to_scan:
            val = prev + hist.GetBinContent(ix)
            out.SetBinContent(ix, val)
            prev = val
        return out
    
    def create_table(self, data: list, title: str, outPath: str):
        # Convert the list of dictionaries to a dataframe
        df = pd.DataFrame(data)

        # Sort the dataframe by the 'Integral Value' column in descending order
        df.sort_values(by='Integral Value', ascending=False, inplace=True)

        # Format the integrals and entries to have commas as thousand separators
        df['Integral Value'] = df['Integral Value'].apply(lambda x: "{:,.2f}".format(x))
        df['Entries'] = df['Entries'].apply(lambda x: "{:,.2f}".format(x))

        # Set the aesthetic parameters for the table
        cell_text_color = 'black'
        cell_background_color = ['white', 'lightgrey']

        # Plot a table based on the dataframe and remove all the axes
        fig, ax = plt.subplots(figsize=(7, 6)) # set size frame
        ax.axis('off')

        table = ax.table(cellText=df.values, colLabels=df.columns, loc='center', 
                        cellLoc='center', cellColours=[[cell_background_color[i%len(cell_background_color)] for _ in range(len(df.columns))] for i in range(len(df))])
        table.auto_set_font_size(False)
        table.set_fontsize(10)
        table.scale(1.0, 1.2)

        # Set the color of the cell borders and text
        for _, cell in table.get_celld().items():
            cell.set_edgecolor("black")
            cell.set_text_props(color=cell_text_color)

        # Add a title closer to the table using text
        ax.text(0.5, 0.75, title, fontsize=16, ha='center')

        if outPath[-1] != '/': outPath += '/'
        plt.savefig(outPath + 'table' + CMS_lumi.outputFileName + '.png')

    def draw(self, subplot, outputPathway, region, muelchannel, QCD):
        self.prepare_root_files()
        self.prepare_scaling_factors()
        if QCD: self.prepare_QCDfit(outputPathway)

        self.c1 = self.ROOT.TCanvas("c1", "c1")
        # self.c1.Print("plots.pdf[")
        for hist_name, x_title, y_title, mode, draw_option, is_logy, is_logx, underflow_bin, ymin, ymax, binlist, extraText in zip(self.histogram_list,
                                                                                                                                    self.x_titles,
                                                                                                                                    self.y_titles,
                                                                                                                                    self.draw_modes,
                                                                                                                                    self.draw_options,
                                                                                                                                    self.is_logy,
                                                                                                                                    self.is_logx,
                                                                                                                                    self.underflow_bin,
                                                                                                                                    self.ymin,
                                                                                                                                    self.ymax,
                                                                                                                                    self.bin_lists,
                                                                                                                                    self.extra_text_hists):
            self.createStacks(hist_name, x_title, y_title, mode, outputPathway, region, muelchannel, QCD, underflow_bin, draw_option, is_logy, is_logx, ymin, ymax, binlist, subplot, extraText)

        # self.c1.Print("plots.pdf]")
        self.c1.Close()

    def bin_log_x(self, h):

        axis = h.GetXaxis()
        bins = axis.GetNbins()

        from_val = axis.GetXmin()
        to_val = axis.GetXmax()
        width = (to_val - from_val) / bins
        new_bins = []

        for i in range(bins + 1):
            new_bins.append(10**(from_val + i * width))

        axis.Set(bins, array('d', new_bins))
        # print(f'new_bins === {new_bins}')

    def createStacks(self, hist_name, xtitle, ytitle, mode, outPath, region, muelchannel, QCD, underflow_bin, option="", isLogy=False, isLogx=False, ymin=-1111, ymax=-1111, binlist=[], subplot='', extraText = ''):

        print('\n')
        print(f'{HRED}Creating {hist_name} histogram{RESET}')
        print('\n')

        
        # Global Variables
        hs = self.ROOT.THStack()
        tl = self.ROOT.TLegend(self.legend_x1, self.legend_y1, self.legend_x2, self.legend_y2)
        tl.SetNColumns(4)
        tl.SetTextAlign(12)
        tl.SetMargin(0.2)
        tl.SetColumnSeparation(0.02)
        tl.SetBorderSize(0)
        hist_group = dict()
        nbOfEventsPerChannel = dict()
        labellist = []

        # Significance
        B = 0.0
        S = 0.0
        Z = 0.0

        if outPath != '':
            if outPath[-1] != '/': outPath += '/'
        
        path = outPath + "plots/"

        if not os.path.exists(path):
            os.mkdir(path)



        # adding signal contribution 
        signal_hist = None
        signal_histlist = []
        # adding BKG contributions
        mc_hist_sum = None
        allMChistos = None

        xbins = []
        nrebins=-1

        if len(binlist)>0:
            xbins = array.array('d', binlist)
            nrebins=len(binlist)-1

     ########################## Get Monte Carlos histos info and divide them in Signals and Backgrounds ##########################

        for ifile in range(len(self.mc_file_list)):

            # rootFile = self.mc_file_list[ifile].split("/")[-1]
            # print(f'rootFile = {rootFile}') # To Print the name of the root files being read

            # if 'PROC_QCD_dataDriven_Muon' in rootFile:
            #     hist_name = hist_name.replace('cut101','dataDriven')



            ahist = self.mc_root_files[ifile].Get(hist_name)


            if nrebins>0: ahist=ahist.Rebin(nrebins,hist_name+'rebinned_mc'+str(ifile),xbins)
            # print("histogram mc files rebinned:", ahist)
            # ahist.SetBinContent(ahist.GetNbinsX(), ahist.GetBinContent(ahist.GetNbinsX()) + ahist.GetBinContent(ahist.GetNbinsX()+1))
            # ahist.GetXaxis().SetLog(1)
            # ahist.SetLogx()

            # print(f'hist_name = {hist_name}') # Prints histogram being made



            if isLogx:
                self.bin_log_x(ahist)
                if underflow_bin:
                    underflow = ahist.GetBinContent(0)
                    first_bin_content = ahist.GetBinContent(1)
                    new_first_bin_content = first_bin_content + underflow
                    ahist.SetBinContent(1, new_first_bin_content)
                    ahist.SetBinContent(0, 0)

            if ahist == None:
                print("histogram %s not found in %s"%(hist_name, self.mc_file_list[ifile]))
                print("quitting")
                sys.exit(-1)
            else:

                if underflow_bin:
                    underflow = ahist.GetBinContent(0)
                    first_bin_content = ahist.GetBinContent(1)
                    new_first_bin_content = first_bin_content + underflow
                    ahist.SetBinContent(1, new_first_bin_content)
                    ahist.SetBinContent(0, 0)

                # Handling overflow
                overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
                last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
                new_last_bin_content = last_bin_content + overflow
                ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
                ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)
    

                rootFileName_process = str(self.mc_root_files[ifile]).split('/')[-1]
                rootFileName_process = rootFileName_process.replace(".root", "")
                rootFileName_process = rootFileName_process.replace(" Title: ", "")
                # print(f'{rootFileName_process} Integral before SF (SF = {self.sf_list[ifile]}): {ahist.Integral()}')
                ahist.Scale(self.sf_list[ifile])
                # print(f'{rootFileName_process} Integral after SF (SF = {self.sf_list[ifile]}): {ahist.Integral()}')                               

                # group by labels
                label = self.mc_label_list[ifile]
                # print(f'label = {label}')
                # print(f'Entries={ahist.GetEntries()}') # Prints number of entries in the histogram
                # print(f'integral={ahist.Integral()}')


                if "dataDriven" in label:
                    path += 'dataDriven/'
                    if not os.path.exists(path):
                        os.mkdir(path)



                if label not in hist_group:
                    hist_group[label] = ahist
                    labellist.append(label) # need to take care of the order
                    nbOfEventsPerChannel[label] = ahist.GetEntries()
                else:
                    hist_group[label].Add(ahist)
                    nbOfEventsPerChannel[label] += ahist.GetEntries()

                if 't-channel' not in label:
                    if mc_hist_sum == None:
                        mc_hist_sum = ahist.Clone("mc_hist_sum")
                    else:
                        mc_hist_sum.Add(ahist)
                    
                    B += ahist.GetEntries()
                    # print('Events in ' + str(label) + ' = ' + str(ahist.GetEntries()))
                    if mode==NORMALIZED:
                        ahist
                    ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
                    ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
                    ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])
                    # ahist.UseCurrentStyle()

                # Signal
                else:
                    if signal_hist is None:
                        signal_hist = ahist
                    else:
                        signal_hist.Add(ahist)
                    
                    S += ahist.GetEntries()
                    # print('Events in ' + str(label) + ' = ' + str(ahist.GetEntries()))
                    ahist.SetFillColorAlpha(self.color_list[self.mc_color_list[ifile]], self.fill_alpha)
                    ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
                    ahist.SetFillStyle(self.pattern_list[self.mc_pattern_list[ifile]])                    
                    # ahist.SetLineColor(self.color_list[self.mc_color_list[ifile]])
                    signal_histlist.append(ahist)
                # print('\n')
                if allMChistos == None: allMChistos = ahist.Clone('allMChistos')
                else: allMChistos.Add(ahist)
        
        # Compute the significance
        # print('S = ',S)
        # print('B = ',B)

        # Z = round(S/(np.sqrt(S+B)),1) 
        # print('Z = ',Z)

    
     ########################## Organize the cmsExtra text in accordance to channel and region and create the good output directory ##########################

        table_title = ''

        regionCode = [char for char in hist_name if char.isdigit()]

        if 'el' in muelchannel:
            CMS_lumi.extraText += '  el'
            table_title += ' el'
            path += 'el/'
        elif 'mu' in muelchannel:
            CMS_lumi.extraText += '  mu'
            table_title += ' mu'
            path += 'mu/'
        else:
            path += 'otherChannel/'

        if not os.path.exists(path):
            os.mkdir(path)

        
        if region == '2j1t':
            CMS_lumi.extraText += ' Signal'
            table_title += ' Signal'
            path += '2j1t/'

        elif region == '2j0t':
            CMS_lumi.extraText += ' 2j0t'
            table_title += ' 2j0t'
            path += '2j0t/'

        elif region == '2j0t1m':
            CMS_lumi.extraText += ' 2j0t1m'
            table_title += ' 2j0t1m'
            path += '2j0t1m/'

        elif region == '2j0t0m1l':
            CMS_lumi.extraText += ' 2j0t0m1l'
            table_title += ' 2j0t0m1l'
            path += '2j0t0m1l/'

        elif region == '3j2t':
            CMS_lumi.extraText += ' 3j2t'
            table_title += ' 3j2t'
            path += '3j2t/'

        elif region == 'nocut':
            CMS_lumi.extraText += ' nocut'
            table_title += 'nocut'
            path += 'nocut/'

        else:
            CMS_lumi.extraText += '    ' + 'cut:' + ''.join(regionCode)
            table_title += '    ' + 'cut:' + ''.join(regionCode)
            path += ''.join(regionCode) + '/'


        if not os.path.exists(path):
            os.mkdir(path)


        if QCD:
            # CMS_lumi.extraText += ' (QCD)'
            path += 'QCDregion/'

        if not os.path.exists(path):
            os.mkdir(path)
  
        # CMS_lumi.extraText += '  Z=' + str(Z)   

        CMS_lumi.extraText += ' ' + extraText  


     ########################## Now we create the stack histo and the legend ##########################

        # List to reorder the labels in the right way
        labels_in_order = []

        # Organize and Stack Monte Carlos Backgrounds
        normevts = dict()
        for label in labellist:
            if not "t-channel" in label:
                normevts[label] = hist_group[label].Integral()
        renormevts_list=sorted(normevts.items(),key=lambda x:x[1],reverse=True)
        # print('renormevts_list: ',renormevts_list)

        reordered_labellist = self.stackOrder
        # Append the other processes in their sorted order
        for label, _ in renormevts_list:
            if label not in self.stackOrder:
                reordered_labellist.append(label)
        # Comment out the previous lines and comment in this on if you only wish to order the processes by the number of events in each process.
        # reordered_labellist = [i[0] for i in renormevts_list] 


        # print('reordered_labellist: ',reordered_labellist)
        if isLogy: reordered_labellist.reverse()
        
        table_data = []

        for label in reordered_labellist:
            ahist = hist_group[label]
            # print('Stacking ', label)
            print(label+" Integral: %f"%ahist.Integral())
            print(label+" Entries: %f"%ahist.GetEntries())


            if 't-channel' not in label:
                if mode == NORMALIZED:
                    ahistcopy = ahist.Clone()
                    normscale = ahistcopy.Integral()
                    print('normscale_label=',normscale)
                    ahistcopy.Scale(1.0/normscale)
                    hs.Add(ahistcopy)
                    labels_in_order.append(label)
                    # Add a dictionary with the data for this label to the list
                    table_data.append({
                        'Process Name': label,
                        'Integral Value': ahistcopy.Integral(),
                        'Entries': ahistcopy.GetEntries()
                    })
                else:
                    hs.Add(ahist)
                    labels_in_order.append(label)
                    # Add a dictionary with the data for this label to the list
                    table_data.append({
                        'Process Name': label,
                        'Integral Value': ahist.Integral(),
                        'Entries': ahist.GetEntries()
                    })



        print(f'signal Integral: {signal_hist.Integral()}')
        print(f'signal Entries: {signal_hist.GetEntries()}')            

        if mode == NORMALIZED:
            ahistcopy_sig = signal_hist.Clone()
            normscale = ahistcopy_sig.Integral()
            print('normscale_label=',normscale)
            ahistcopy_sig.Scale(1.0/normscale)
            hs.Add(ahistcopy_sig)
            labels_in_order.append('t-channel')
            table_data.append({
                'Process Name': 't-channel',
                'Integral Value': ahistcopy_sig.Integral(),
                'Entries': ahistcopy_sig.GetEntries()
            })

        else:
            hs.Add(signal_hist)
            labels_in_order.append('t-channel')
            table_data.append({
                'Process Name': 't-channel',
                'Integral Value': signal_hist.Integral(),
                'Entries': signal_hist.GetEntries()
            })


        labels_in_order.reverse()

        # Put MC labels in the correct order
        for label in labels_in_order:
            ahist = hist_group[label]
            tl.AddEntry(ahist, label, "F")

     ########################## DATA input ##########################
        final_data_hist = None

        if (self.data_file_list and '2j1t' not in CMS_lumi.extraText):
            for ifile in self.data_file_list:
                atfile = self.ROOT.TFile(ifile)
                ahist = atfile.Get(hist_name)

                if underflow_bin:
                    underflow = ahist.GetBinContent(0)
                    first_bin_content = ahist.GetBinContent(1)
                    new_first_bin_content = first_bin_content + underflow
                    ahist.SetBinContent(1, new_first_bin_content)
                    ahist.SetBinContent(0, 0)

                # Handling overflow
                overflow = ahist.GetBinContent(ahist.GetNbinsX() + 1)
                last_bin_content = ahist.GetBinContent(ahist.GetNbinsX())
                new_last_bin_content = last_bin_content + overflow
                ahist.SetBinContent(ahist.GetNbinsX(), new_last_bin_content)
                ahist.SetBinContent(ahist.GetNbinsX() + 1, 0)

                if isLogx:
                    self.bin_log_x(ahist)

                # print("histogram datafile names:", ahist)
                if nrebins>0: ahist=ahist.Rebin(nrebins,hist_name+'rebinned_data',xbins)
                # ahist.SetBinContent(ahist.GetNbinsX(), ahist.GetBinContent(ahist.GetNbinsX()) + ahist.GetBinContent(ahist.GetNbinsX()+1))
                if ahist is None:
                    print("histogram %s not found in %s"%(hist_name, self.data_file_list[ifile]))
                    sys.exit(-1)

                else:
                    if final_data_hist == None:
                        final_data_hist = ahist.Clone("finaldata")
                    else:
                        final_data_hist.Add(ahist)
                print("Data : %i"%final_data_hist.Integral())

            if mode == NORMALIZED:
                print("Normalizing Data to one")
                normscale = final_data_hist.Integral()
                final_data_hist.Scale(1.0/normscale)
        
            # Legend add entry
            tl.AddEntry(final_data_hist, "Data", "P")


     ########################## Create TPads and elements of Pad ##########################
        c1_top = None
        if not self.data_file_list or '2j1t' in CMS_lumi.extraText:
            c1_top = self.ROOT.TPad("c1_top", "top", 0.01, 0.01, 0.99, 0.99)
        else:
            c1_top = self.ROOT.TPad("c1_top", "top", 0.01, 0.33, 0.99, 0.99)
        c1_top.Draw()
        c1_top.cd()
        
        # log y scale
        if isLogy:
            c1_top.SetLogy(isLogy)

        if isLogx:
            c1_top.SetLogx(isLogx)

        if mode == STACKED:
            hs.Draw(option)
        else:
            hs.Draw("nostack " + option)
                
        # Define the Max for Yaxis
        sig_max = -1
        for sighist in signal_histlist:
            if sig_max<sighist.GetMaximum():
                sig_max = sighist.GetMaximum()
                #print("sig_max==", sig_max)

        # Set vertical range
        max1 = hs.GetMaximum()
        # print("max1 =", max1)
        max2 = -1
        max3 = sig_max
        total_max = max(max(max1,max2),max3)
        if self.data_file_list and '2j1t' not in CMS_lumi.extraText:
            max2 = final_data_hist.GetMaximum()
        if ymin != -1111:
            hs.SetMinimum(ymin)
        if ymax != -1111:
            hs.SetMaximum(ymax)
        elif isLogy:
            hs.SetMaximum(total_max**1.65)
        else:
            hs.SetMaximum(total_max*1.3) #Y axis range


     ########################## Define type of plot to draw ##########################

        if self.data_file_list and '2j1t' not in CMS_lumi.extraText:

            if not isLogy:
                self.ROOT.TGaxis.SetExponentOffset(-0.039,0.01,"y")


            CMS_lumi.lumiTextSize = 0.6
            CMS_lumi.cmsTextSize = 0.6
            CMS_lumi.extraOverCmsTextSize = 0.9

            c1_top.SetMargin(0.13, 0.04, 0.02, 0.09) # left, right, bottom, top

            final_data_hist.SetMarkerStyle(self.ROOT.kFullCircle)
            final_data_hist.Draw("same err P")

            xaxis = hs.GetXaxis()
            xaxis.SetLabelSize(0)

            yaxis = hs.GetYaxis()
            yaxis.SetTitle(ytitle)
            yaxis.SetNdivisions(6,5,0)
            yaxis.SetMaxDigits(3)
            yaxis.SetTitleSize(0.05)
            yaxis.SetLabelSize(0.04)
            # yaxis.SetTitleOffset(1.8)

            # Ratio plot
            if subplot == "R":
                hstackhist = allMChistos
                ratiohist = final_data_hist.Clone("ratiohist")
                ratiohist.Divide(hstackhist)
                for n in range(final_data_hist.GetNbinsX()):
                    newerror=ratiohist.GetBinContent(n)/math.sqrt(final_data_hist.GetBinContent(n)) if final_data_hist.GetBinContent(n)!=0 else 0
                    ratiohist.SetBinError(n,newerror)
                    # print('bin ' + str(n) + ' = ' + str(ratiohist.GetBinContent(n)))
                ratiohist.SetMinimum(0.3)
                ratiohist.SetMaximum(1.7)

                tl.Draw()
                c1_top.Modified()
                #CMS_lumi.CMS_lumi(c1_top, 4, 11)
                CMS_lumi.CMS_lumi(self.ROOT, c1_top, 4, 0)
                c1_top.cd()
                c1_top.Update()
                c1_top.RedrawAxis()

                self.c1.cd()
                c1_bottom = self.ROOT.TPad("c1_bottom", "bottom", 0.01, 0.01, 0.99, 0.33)
                c1_bottom.Draw()
                c1_bottom.cd()
                c1_bottom.SetMargin(0.13, 0.04, 0.4, 0.02) # left, right, bottom, top
                c1_bottom.SetGridx(1)
                c1_bottom.SetGridy(1)
                ratiohist.Draw("err")

                xaxis = ratiohist.GetXaxis()
                xaxis.SetTitle(xtitle)
                xaxis.SetNdivisions(6,5,0)
                xaxis.SetTitleSize(0.1)
                xaxis.SetTitleOffset(1.5)
                xaxis.SetLabelSize(0.10)

                yaxis = ratiohist.GetYaxis()
                yaxis.SetTitle("Data/Exp")
                yaxis.SetTitleSize(0.1)
                yaxis.SetTitleOffset(0.5)
                yaxis.SetNdivisions(6,5,0)
                yaxis.SetLabelSize(0.08) #Set offset between axis and axis labels
                yaxis.SetLabelOffset(0.007)

                if isLogx:
                    c1_bottom.SetLogx(isLogx)

                c1_bottom.Update()

            #Significance plot (SoverB hist)
            elif subplot == "S":
                soverbhistlist = []
                M = -1
                S_peak_bin = -1
                for label in labellist :
                    if 'LFV T' in label:
                        Thist = hist_group[label]
                        S_peak_bin = Thist.GetMaximumBin()
                        break
                for label in labellist :
                    if 'MC_Sig' in label:
                        sighist = hist_group[label]
                        soverbhist, cutinfo = self.MakeSoverB(mc_hist_sum, sighist, label, S_peak_bin)
                        soverbhistlist.append(soverbhist)
                        tempM = soverbhist.GetMaximum()
                        if M < tempM : M = tempM

                tl.Draw()
                c1_top.Modified()
                #CMS_lumi.CMS_lumi(c1_top, 4, 11)
                CMS_lumi.CMS_lumi(self.ROOT, c1_top, 4, 0)
                c1_top.cd()
                c1_top.Update()
                c1_top.RedrawAxis()

                self.c1.cd()
                c1_bottom = self.ROOT.TPad("c1_bottom", "bottom", 0.01, 0.01, 0.99, 0.32)
                c1_bottom.Draw()
                c1_bottom.cd()
                c1_bottom.SetTopMargin(0.02)
                c1_bottom.SetBottomMargin(0.3)
                c1_bottom.SetRightMargin(0.1)
                c1_bottom.SetGridx(1)
                c1_bottom.SetGridy(1)


                n = 0
                for soverbh in soverbhistlist:
                    soverbh.SetMinimum(0)
                    soverbh.SetMaximum(M*1.2)
                    soverbh.SetLineWidth(2)
                    soverbh.SetLineColor(self.color_list[self.mc_color_list[n]])
                    n = n+1
                    soverbh.Draw("same Hist")
                    if n == 1 :
                        soverbtitle = "S/#sqrt{B}"+" ( "+cutinfo+" )"
                        xaxis = soverbh.GetXaxis()
                        xaxis.SetTitle(xtitle)
                        xaxis.SetNdivisions(6,5,0)
                        xaxis.SetTitleSize(0.12)
                        xaxis.SetLabelSize(0.10)

                        yaxis = soverbh.GetYaxis()
                        yaxis.SetTitle(soverbtitle)
                        yaxis.SetNdivisions(6,5,0)
                        yaxis.SetTitleSize(0.1)
                        yaxis.SetLabelSize(0.08)
                        yaxis.SetTitleOffset(0.7)
                        yaxis.SetLabelOffset(0.007)

                c1_bottom.Modified()
                c1_bottom.RedrawAxis()
            
            else:

                if not isLogy:
                    self.ROOT.TGaxis.SetExponentOffset(-0.052,0.01,"y")


                CMS_lumi.lumiTextSize = 0.4
                CMS_lumi.cmsTextSize = 0.4
                CMS_lumi.extraOverCmsTextSize = 0.9

                c1_top.SetMargin(0.1, 0.1, 0.1, 0.1) # left, right, bottom, top

                xaxis = hs.GetXaxis()
                xaxis.SetTitle(xtitle)
                xaxis.SetNdivisions(6,5,0)
                xaxis.SetTitleSize(0.04)
                xaxis.SetTitleOffset(1.2)
                xaxis.SetLabelSize(0.04)
                
                yaxis = hs.GetYaxis()
                yaxis.SetTitle(ytitle)
                yaxis.SetNdivisions(6,5,0)
                yaxis.SetMaxDigits(3)
                yaxis.SetTitleSize(0.04)
                yaxis.SetLabelSize(0.04)
                yaxis.SetTitleOffset(1.8)

                if mode == NORMALIZED:
                    print(f'maximum in normalized mode: {hs.GetMaximum()}')
                    hs.SetMaximum(0.1)

                tl.Draw()
                c1_top.Modified()
                CMS_lumi.CMS_lumi(self.ROOT, c1_top, 4, 0)
                c1_top.cd()
                c1_top.Update()
                c1_top.RedrawAxis()
        
        else:

            if not isLogy:
                self.ROOT.TGaxis.SetExponentOffset(-0.052,0.01,"y")


            CMS_lumi.lumiTextSize = 0.4
            CMS_lumi.cmsTextSize = 0.4
            CMS_lumi.extraOverCmsTextSize = 0.9

            c1_top.SetMargin(0.1, 0.1, 0.1, 0.1) # left, right, bottom, top

            xaxis = hs.GetXaxis()
            xaxis.SetTitle(xtitle)
            xaxis.SetNdivisions(6,5,0)
            xaxis.SetTitleSize(0.04)
            xaxis.SetTitleOffset(1.2)
            xaxis.SetLabelSize(0.04)
            
            yaxis = hs.GetYaxis()
            yaxis.SetTitle(ytitle)
            yaxis.SetNdivisions(6,5,0)
            yaxis.SetMaxDigits(3)
            yaxis.SetTitleSize(0.04)
            yaxis.SetLabelSize(0.04)
            yaxis.SetTitleOffset(1.8)

            tl.Draw()
            c1_top.Modified()
            CMS_lumi.CMS_lumi(self.ROOT, c1_top, 4, 0)
            c1_top.cd()
            c1_top.Update()
            c1_top.RedrawAxis()

        
     ########################## Save plot ##########################

        # print(f'Number of events: {type(nbOfEventsPerChannel)}')
        # self.printTable(nbOfEventsPerChannel,['QCD'])

        outFormat = '.png'
        self.c1.SetCanvasSize(1200, 1000)

        frame = self.c1.GetFrame()
        if not self.data_file_list and '2j1t' in CMS_lumi.extraText:
            frame = c1_top.GetFrame()
            if mode == NORMALIZED:
                frame = self.c1.GetFrame()
        frame.Draw()

        if underflow_bin: CMS_lumi.outputFileName += '_underflow_'

        if subplot == "R":
            path += "plot_ratio_"+str(self.integer_lumi)
            if not os.path.isdir(path):
                os.mkdir(path)
        elif subplot == "S":
            path += "plot_snb_"+str(self.integer_lumi)
            if not os.path.isdir(path):
                os.mkdir(path)
        table_title += '  ' + str(self.integer_lumi) + 'fb-1'
        self.create_table(table_data, table_title, path) # Create Table with integrals

        if not self.data_file_list:
            if(isLogy):
                c1_top.SaveAs(path+"/"+hist_name+"_logy" + CMS_lumi.outputFileName + outFormat)
                # self.c1.Print(path + '/' + "plots" + outFormat)
            else:
                c1_top.SaveAs(path+"/"+hist_name+"_nology" + CMS_lumi.outputFileName + outFormat)
                # self.c1.Print(path + '/' + "plots" + outFormat)
        else:
            if(isLogy):
                self.c1.SaveAs(path+"/"+hist_name+"_logy" + CMS_lumi.outputFileName + outFormat)
                # self.c1.Print(path + '/' + "plots" + outFormat)
            else:
                self.c1.SaveAs(path+"/"+hist_name+"_nology" + CMS_lumi.outputFileName + outFormat)
                # self.c1.Print(path + '/' + "plots" + outFormat)
        self.c1.Clear()
        CMS_lumi.extraText = ""
        pass