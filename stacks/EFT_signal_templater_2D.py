import ROOT
def unroll_histogram_2d_to_1d(th2d):
    """
    Unroll a TH2D ROOT histogram into a 1D array, labeling only the Phi Star bins.
    
    Parameters:
    th2d (ROOT.TH2D): 2D ROOT histogram.
    
    Returns:
    ROOT.TH1D: 1D ROOT histogram with unrolled bin contents.
    """
    # Get number of bins along each axis
    nbins_x = th2d.GetNbinsX()
    # print(f'# of bins in x = {nbins_x}')

    nbins_y = th2d.GetNbinsY()
    # print(f'# of bins in y = {nbins_y}')

    # Get axis ranges
    x_min = th2d.GetXaxis().GetXmin()
    x_max = th2d.GetXaxis().GetXmax()
    y_min = th2d.GetYaxis().GetXmin()
    y_max = th2d.GetYaxis().GetXmax()

    # print(f'x_min = {x_min}, x_max = {x_max}')
    # print(f'y_min = {y_min}, y_max = {y_max}')

    # Calculate total number of bins for the 1D histogram
    nbins_total = nbins_x * nbins_y

    # Create a 1D histogram to store the unrolled data
    h1d = ROOT.TH1D(th2d.GetName() + "_unrolled", "Unrolled 2D Histogram;PhiStar Bin;Entries", nbins_total, 0, nbins_total)

    hist_x = th2d.ProjectionX("_px", 0, -1, "e")
    hist_y = th2d.ProjectionY("_py", 0, -1, "e")

    for bin_x in range(1, nbins_x + 1):
        for bin_y in range(1, nbins_y + 1):
            # print(f'bin_x = {bin_x}, bin_y = {bin_y}')
            # Calculate the corresponding 1D bin index
            # bin_1d = (bin_x - 1) * nbins_y + (bin_y - 1) + 1
            bin_1d = (bin_y - 1) * nbins_x + (bin_x - 1) + 1

            # print(f'bin1d value: {bin_1d}')
            # Get the content of the 2D bin
            bin_content = th2d.GetBinContent(bin_x, bin_y)
            bin_error = th2d.GetBinError(bin_x, bin_y)
            # print(f'bin_content: {bin_content}, bin_error: {bin_error}')
            # Set the content and error to the corresponding 1D bin
            h1d.SetBinContent(bin_1d, bin_content)
            h1d.SetBinError(bin_1d, bin_error)
    
    # Set custom bin labels for combined Phi Star and BDT bins
    # for bin_x in range(1, nbins_x + 1):
    #         phi_star_bin_center = x_min + (bin_x - 0.5) * (x_max - x_min) / nbins_x
    #         for bin_y in range(1, nbins_y + 1):
    #             if (bin_y - 1) % 5 != 0 or (bin_y - 1) % 0 != 0:
    #                 continue
    #             bdt_bin_center = y_min + (bin_y - 0.5) * (y_max - y_min) / nbins_y
    #             bin_1d = (bin_x - 1) * nbins_y + (bin_y - 1) + 1
    #             # label = f"({phi_star_bin_center:.2f}, {bdt_bin_center:.2f})"
    #             label = '2pi'
    #             # print(f'Label for bin {bin_1d}: {label}')
    #             h1d.GetXaxis().SetBinLabel(bin_1d, label)
    
    return h1d, hist_x, hist_y
def main():

    file_sf_list = [[file, sf] for file, sf in zip(s.mc_file_list, s.sf_list)]

    for i in range(len(file_sf_list)):
        if "EFT" in file_sf_list[i][0]:
            mg5_file = ROOT.TFile.Open(file_sf_list[i][0])
            mg5_sf = file_sf_list[i][1]
            print(f'mg5 scale factor: {mg5_sf}')

        if "PROC_ST_t-channel_top.root" in file_sf_list[i][0]:
            powheg_top_file = ROOT.TFile.Open(file_sf_list[i][0])
            powheg_top_sf = file_sf_list[i][1]
            print(f'powheg_top_sf factor: {powheg_top_sf}')


        if "PROC_ST_t-channel_antitop.root" in file_sf_list[i][0]:
            powheg_antitop_file = ROOT.TFile.Open(file_sf_list[i][0])
            powheg_antitop_sf = file_sf_list[i][1]
            print(f'powheg_antitop_sf scale factor: {powheg_antitop_sf}')


    variable = args.variable_name
    eft_weights_list = [
        ['_LHEWeight_SM', 0],
        ['_LHEWeight_ctwi_m2', -2],
        ['_LHEWeight_ctwi_p2', 2],
    ]
    cut_id = '_cut_0_0_0_0'

    eft_hists_for_parabolic_fit = []
    eft_hists_for_parabolic_fit_projX = []
    eft_hists_for_parabolic_fit_projY = []

    eft_value_for_parabolic_fit = []
    
    for i in range(len(eft_weights_list)):
        histName = variable + eft_weights_list[i][0] + cut_id
        rolled_hist = mg5_file.Get(histName)
        if not rolled_hist:
            print(f'Histogram {histName} not found in {mg5_file}')
            exit(1)
        hist, hist_x, hist_y = unroll_histogram_2d_to_1d(rolled_hist)
        hist.Scale(mg5_sf)
        if 'SM' in histName:
            mg5_SM_hist = hist.Clone()
            # mg5_SM_hist.Scale(mg5_sf)
            # continue
        eft_hists_for_parabolic_fit.append(hist)
        eft_hists_for_parabolic_fit_projX.append(hist_x)
        eft_hists_for_parabolic_fit_projY.append(hist_y)
        eft_value_for_parabolic_fit.append(eft_weights_list[i][1])

    print(f'eft_hists_for_parabolic_fit: {eft_hists_for_parabolic_fit}')
    print(f'eft_value_for_parabolic_fit: {eft_value_for_parabolic_fit}')

    
    # bdt_parabolic_coeffs = s.eft_parabolic_fit(eft_hists_for_parabolic_fit, eft_value_for_parabolic_fit, 'plots/EFTparabolicFit/signal/2Dunrolled', 'PhiStar*BDT Signal Region')

    variable_for_powheg = 'phi_star_new_SR_BDT_output_cut_0_0_0_0'
    powheg_top_hist = powheg_top_file.Get(variable_for_powheg)
    powheg_antitop_hist = powheg_antitop_file.Get(variable_for_powheg)
    if not powheg_top_hist or not powheg_antitop_hist:
        print(f'powheg hist not found')
        exit(1)
    # Scaling before unrolling
    # powheg_top_hist.Scale(powheg_top_sf)
    # powheg_combined_hist = powheg_top_hist.Clone()
    
    # powheg_antitop_hist.Scale(powheg_antitop_sf)
    # powheg_combined_hist.Add(powheg_antitop_hist)

    # powheg_combined_hist_unrolled = unroll_histogram_2d_to_1d(powheg_combined_hist)


    # Scaling after unrolling
    powheg_top_hist_unrolled, powheg_top_hist_unrolled_projX, powheg_top_hist_unrolled_projY = unroll_histogram_2d_to_1d(powheg_top_hist)
    powheg_top_hist_unrolled.Scale(powheg_top_sf)
    powheg_top_hist_unrolled_projX.Scale(powheg_top_sf)
    powheg_top_hist_unrolled_projY.Scale(powheg_top_sf)

    powheg_combined_hist_unrolled = powheg_top_hist_unrolled.Clone()
    powheg_combined_hist_unrolled_projX = powheg_top_hist_unrolled_projX.Clone()
    powheg_combined_hist_unrolled_projY = powheg_top_hist_unrolled_projY.Clone()


    powheg_antitop_hist_unrolled, powheg_antitop_hist_unrolled_projX, powheg_antitop_hist_unrolled_projY = unroll_histogram_2d_to_1d(powheg_antitop_hist)
    powheg_antitop_hist_unrolled.Scale(powheg_antitop_sf)
    powheg_antitop_hist_unrolled_projX.Scale(powheg_top_sf)
    powheg_antitop_hist_unrolled_projY.Scale(powheg_top_sf)

    powheg_combined_hist_unrolled.Add(powheg_antitop_hist_unrolled)
    powheg_combined_hist_unrolled_projX.Add(powheg_antitop_hist_unrolled_projX)
    powheg_combined_hist_unrolled_projY.Add(powheg_antitop_hist_unrolled_projY)

    powheg_combined_hist_ctwi_p1 = powheg_combined_hist_unrolled.Clone()
    powheg_quadratic_template1 = powheg_combined_hist_unrolled.Clone()

    powheg_combined_hist_ctwi_p1_projX = powheg_combined_hist_unrolled_projX.Clone()
    powheg_quadratic_template1_projX = powheg_combined_hist_unrolled_projX.Clone()

    powheg_combined_hist_ctwi_p1_projY = powheg_combined_hist_unrolled_projY.Clone()
    powheg_quadratic_template1_projY = powheg_combined_hist_unrolled_projY.Clone()







    # Extra testing step for quadratic function
    powheg_quadratic_template2 = powheg_combined_hist_unrolled.Clone()

    parabolic_coeffs_2d = s.eft_parabolic_fit(eft_hists_for_parabolic_fit, eft_value_for_parabolic_fit, 'plots/EFTparabolicFit/signal/2Dunrolled', 'all_parabolas_2d.png', 'PhiStar*BDT Signal Region')



    for i in range(1,len(parabolic_coeffs_2d)+1):
        a = parabolic_coeffs_2d[i][0]
        b = parabolic_coeffs_2d[i][1]
        c = parabolic_coeffs_2d[i][2]

        ratio_ctwi_p1_SM = a*(1)**2 + b*1 + c
        ratio_ctwi_m1_SM = a*(-1)**2 + b*(-1) + c
        ratio_ctwi_p2_SM = a*(2)**2 + b*2 + c
        ratio_ctwi_m2_SM = a*(-2)**2 + b*(-2) + c

        # print(f'ratio_ctwi_p1_SM for bin {i} = {ratio_ctwi_p1_SM}')

        powheg_SM_value = powheg_combined_hist_unrolled.GetBinContent(i)

        powheg_eft_scaling_ctwi_p1 = ratio_ctwi_p1_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_m1 = ratio_ctwi_m1_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_p2 = ratio_ctwi_p2_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_m2 = ratio_ctwi_m2_SM * powheg_SM_value

        quadratic_term_entry_1 = 1/2*(powheg_eft_scaling_ctwi_p1 + powheg_eft_scaling_ctwi_m1 - 2*powheg_SM_value)
        quadratic_term_entry_2 = 1/8*(powheg_eft_scaling_ctwi_p2 + powheg_eft_scaling_ctwi_m2 - 2*powheg_SM_value)

        powheg_combined_hist_ctwi_p1.SetBinContent(i, powheg_eft_scaling_ctwi_p1)
        powheg_quadratic_template1.SetBinContent(i, quadratic_term_entry_1)
        powheg_quadratic_template2.SetBinContent(i, quadratic_term_entry_2)
    


    outfile_2D = ROOT.TFile("EFT_templates/EFTtemplate_PhiStar_BDT.root","RECREATE")
    powheg_combined_hist_unrolled.Write("sm")
    powheg_combined_hist_ctwi_p1.Write("sm_lin_quad_ctwi")
    powheg_quadratic_template1.Write("quad_ctwi")
    # powheg_quadratic_template2.Write("quadratic_template2")

    outfile_2D.Close()

    parabolic_coeffs_projX = s.eft_parabolic_fit(eft_hists_for_parabolic_fit_projX, eft_value_for_parabolic_fit, 'plots/EFTparabolicFit/signal/2Dunrolled', 'all_parabolas_projX.png', 'Projected_PhiStar')
    parabolic_coeffs_projY = s.eft_parabolic_fit(eft_hists_for_parabolic_fit_projY, eft_value_for_parabolic_fit, 'plots/EFTparabolicFit/signal/2Dunrolled', 'all_parabolas_projY.png', 'Projected_PhiStar')

    print(f'parabolic_coeffs_projX = {parabolic_coeffs_projX}')

    for i in range(1,len(parabolic_coeffs_projX)):
        a = parabolic_coeffs_projX[i][0]
        b = parabolic_coeffs_projX[i][1]
        c = parabolic_coeffs_projX[i][2]

        ratio_ctwi_p1_SM = a*(1)**2 + b*1 + c
        ratio_ctwi_m1_SM = a*(-1)**2 + b*(-1) + c
        ratio_ctwi_p2_SM = a*(2)**2 + b*2 + c
        ratio_ctwi_m2_SM = a*(-2)**2 + b*(-2) + c

        powheg_SM_value = powheg_combined_hist_unrolled_projX.GetBinContent(i)

        powheg_eft_scaling_ctwi_p1 = ratio_ctwi_p1_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_m1 = ratio_ctwi_m1_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_p2 = ratio_ctwi_p2_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_m2 = ratio_ctwi_m2_SM * powheg_SM_value

        quadratic_term_entry_1 = 1/2*(powheg_eft_scaling_ctwi_p1 + powheg_eft_scaling_ctwi_m1 - 2*powheg_SM_value)

        powheg_combined_hist_ctwi_p1_projX.SetBinContent(i, powheg_eft_scaling_ctwi_p1)
        powheg_quadratic_template1_projX.SetBinContent(i, quadratic_term_entry_1)

    outfile_PhiStar = ROOT.TFile("EFT_templates/EFTtemplate_PhiStar.root","RECREATE")
    powheg_combined_hist_unrolled_projX.Write("sm")
    powheg_combined_hist_ctwi_p1_projX.Write("sm_lin_quad_ctwi")
    powheg_quadratic_template1_projX.Write("quad_ctwi")

    outfile_PhiStar.Close()


    for i in range(1,len(parabolic_coeffs_projY)):
        a = parabolic_coeffs_projY[i][0]
        b = parabolic_coeffs_projY[i][1]
        c = parabolic_coeffs_projY[i][2]

        ratio_ctwi_p1_SM = a*(1)**2 + b*1 + c
        ratio_ctwi_m1_SM = a*(-1)**2 + b*(-1) + c
        ratio_ctwi_p2_SM = a*(2)**2 + b*2 + c
        ratio_ctwi_m2_SM = a*(-2)**2 + b*(-2) + c

        powheg_SM_value = powheg_combined_hist_unrolled_projY.GetBinContent(i)

        powheg_eft_scaling_ctwi_p1 = ratio_ctwi_p1_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_m1 = ratio_ctwi_m1_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_p2 = ratio_ctwi_p2_SM * powheg_SM_value
        powheg_eft_scaling_ctwi_m2 = ratio_ctwi_m2_SM * powheg_SM_value

        quadratic_term_entry_1 = 1/2*(powheg_eft_scaling_ctwi_p1 + powheg_eft_scaling_ctwi_m1 - 2*powheg_SM_value)

        powheg_combined_hist_ctwi_p1_projY.SetBinContent(i, powheg_eft_scaling_ctwi_p1)
        powheg_quadratic_template1_projY.SetBinContent(i, quadratic_term_entry_1)



    outfile_BDT = ROOT.TFile("EFT_templates/EFTtemplate_BDT.root","RECREATE")
    powheg_combined_hist_unrolled_projY.Write("sm")
    powheg_combined_hist_ctwi_p1_projY.Write("sm_lin_quad_ctwi")
    powheg_quadratic_template1_projY.Write("quad_ctwi")

    outfile_BDT.Close()
 


    hists_to_plot = [
        [powheg_combined_hist_unrolled, ROOT.kRed, 'SM'],
        [powheg_combined_hist_ctwi_p1, ROOT.kBlue, '(SM + Linear + Quad)'],
        [powheg_quadratic_template1, ROOT.kViolet, "Quad"],
        # [powheg_quadratic_template2, ROOT.kBlack, "POWHEG quadratic term (ctwi_pm2)"],
        # [mg5_SM_hist, ROOT.kGreen, 'MadGraph5 (SM)'],
    ]
    s.make_plot(hists_to_plot, 'PhiStar*BDT', 'PhiStar*BDT', 'Events/bin', 'plots/EFTparabolicFit/signal/2Dunrolled/powheg_combined_signal.png')
    # s.make_plot(hists_to_plot, 'BDT Signal Region', 'plots/EFTparabolicFit/signal/powheg_combined_signal_normalized.png', True)


    hists_to_plot = [
        [mg5_SM_hist, ROOT.kRed, 'mg5 top/antitop SM'],
        [eft_hists_for_parabolic_fit[1], ROOT.kBlue, 'mg5 top/antitop ctwi m2'],
        [eft_hists_for_parabolic_fit[2], ROOT.kViolet, "mg5 top/antitop ctwi p2"],
        [powheg_combined_hist_unrolled, ROOT.kGreen, 'powheg top/antitop SM'],
    ]

    s.make_plot(hists_to_plot, 'PhiStar*BDT', 'PhiStar*BDT', 'events/bin', 'plots/EFTparabolicFit/signal/2Dunrolled/mg5_vs_Powheg_SM.png')


    val_hists, val_eft_values = template_validation(powheg_combined_hist_unrolled, powheg_combined_hist_ctwi_p1, powheg_quadratic_template1)

    val_hists_to_plot = [
        [val_hists[0], ROOT.kRed, 'Powheg SM'],
        [val_hists[1], ROOT.kBlue, 'Powheg ctwi=-2'],
        [val_hists[2], ROOT.kGreen, 'Powheg ctwi=+2'],
    ]

    s.make_plot(val_hists_to_plot, 'PhiStar*BDT', '', '', 'plots/EFTparabolicFit/signal/2Dunrolled/validation_hists.png')

    s.eft_parabolic_fit(val_hists, val_eft_values,  'plots/EFTparabolicFit/signal/2Dunrolled', 'validation_parabolas.png', 'BDT*Phistar_powheg')


if __name__ == '__main__':
    main()
