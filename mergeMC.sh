#!/bin/bash
#SBATCH --job-name=MCAnalysis
#SBATCH --output=slurm.out
#SBATCH --error=slurm.err

#SBATCH --cpus-per-task=1
#SBATCH --time=5:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=d.amram@ip2i.in2p3.fr
#SBATCH --ntasks=1600

# Clean the folder before doing it again
date=$(date "+%Y-%m-%d")
echo $date

./removeEmptyFiles.py $date MC

if [ ! -d "results/17/$date/merged" ]; then
    mkdir -p "results/17/$date/merged"
fi

# Merge all MC file per folder
for name in $(ls DSinfo/17/mc)
do
    for cut in _000 _001 _002 _003 _004 _005
    do
        # Verify that at least one file exist
        if find "results/17/$date/test_nbTasksall/${name::-4}/" -type f -name "*$cut.root" -print -quit | grep -q .; then
            # srun -n1 -N1 --exclusive hadd -fk results/17/$date/merged/$name.root results/17/$date/test_nbTasksall/$name/*root &
            hadd -kf results/17/$date/merged/"${name::-4}"$cut.root results/17/$date/test_nbTasksall/"${name::-4}"/*$cut.root &
        fi
    done
done
wait

# Merge all MC in one file for the btag eff
# srun -n1 -N1 --exclusive hadd -fk results/17/$date/merged/PROC_ALL.root results/17/$date/merged/PROC*root &
for cut in _000 _001 _002 _003 _004 _005
do
    hadd -f results/17/$date/merged/PROC_ALL$cut.root results/17/$date/merged/PROC*$cut.root
done

if [ ! -d "results/17/$date/ready" ]; then
    mkdir -p "results/17/$date/ready"
fi
for cut in _000 _001 _002 _003 _004 _005
do
    # idk if it take less time but idc
    cp results/17/$date/merged/PROC*$cut.root results/17/$date/ready/ &
done
