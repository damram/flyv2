#SBATCH --cpus-per-task=1
#SBATCH --time=5:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=d.amram@ip2i.in2p3.fr
#SBATCH --ntasks=1600

#Merge the data beore removeDouble counting
date=$(date "+%Y-%m-%d")
date="2024-10-28"
echo $date

./removeEmptyFiles.py $date DATA

if [ ! -d "results/17/$date/merged" ]; then
    mkdir -p "results/17/$date/merged"
fi

# Merge all DATA file per folder
for name in $(ls DSinfo/17/data)
do
    # srun -n1 -N1 --exclusive hadd -fk results/17/$date/merged/$name.root results/17/$date/test_nbTasksall/$name/*root &
    hadd -f results/17/$date/merged/"${name::-4}".root results/17/$date/test_nbTasksall/"${name::-4}"/*.root &
done
wait

# Then merge in one data file per run
for run in B C D E F
do
    hadd -f results/17/$date/merged/ALLRun2017$run.root results/17/$date/merged/Run2017$run*.root &
done
wait

# Remove double counted events
python3 removeDoubleCounting.py results/17/$date/merged/

if [ ! -d "results/17/$date/ready" ]; then
    mkdir -p "results/17/$date/ready"
fi
# Now book the histograms
for run in B C D E F
do
    ./processCounting.py results/17/$date/merged/ALLRun2017$run.root results/17/$date/ready/Run$run.root jobconfiganalysis_17 0 &
done
wait

hadd -f results/17/$date/ready/RunALL_0000.root results/17/$date/ready/Run*0000.root &
hadd -f results/17/$date/ready/RunALL_0001.root results/17/$date/ready/Run*0001.root &
hadd -f results/17/$date/ready/RunALL_0002.root results/17/$date/ready/Run*0002.root &
hadd -f results/17/$date/ready/RunALL_0010.root results/17/$date/ready/Run*0010.root &
hadd -f results/17/$date/ready/RunALL_0011.root results/17/$date/ready/Run*0011.root &
hadd -f results/17/$date/ready/RunALL_0012.root results/17/$date/ready/Run*0012.root &
hadd -f results/17/$date/ready/RunALL_0020.root results/17/$date/ready/Run*0020.root &
hadd -f results/17/$date/ready/RunALL_0021.root results/17/$date/ready/Run*0021.root &
hadd -f results/17/$date/ready/RunALL_0022.root results/17/$date/ready/Run*0022.root &
wait
echo "Finished."

