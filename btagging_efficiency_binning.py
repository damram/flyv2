
btageff_dataset_dict = {
    'ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8': {
        'extension': '_ST_t-channel_top',
        'pt_bins': [0., 40., 60., 80., 100., 150., 200., 300., 1000.],
        'eta_bins': [0., 0.6, 1.2, 2.4]
    },
    # Add more datasets with their corresponding extensions, pt_bins, and eta_bins
}
