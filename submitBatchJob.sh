#!/bin/bash
#
##nSBATCH --spread-job
# mail-type=BEGIN, END, FAIL, REQUEUE, ALL, STAGE_OUT, TIME_LIMIT_90
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=d.amram@ip2i.in2p3.fr
#SBATCH --time=6:00:00
#SBATCH --output=slurm.out
#SBATCH --error=slurm.err
#SBATCH --job-name=test

export X509_USER_PROXY=$HOME/x509up_u2395
export EOS_MGM_URL=root://lyoeos.in2p3.fr

inputFile=$(cat ${1})

root='.root'

COUNTER=1

for line in $inputFile
do
    outputFile=$(echo $line | tr "/" "\n")
    for place in $outputFile
    do

        if [[ "$place" == *"$root"* ]]; then
            # srun -n1 --exclusive ./processonefile.py "$line" ${2}$place ${4} > ${2}$(echo $place | tr ".root" "\n").out &
            srun -n1 --exclusive --exact ./processonefile.py "$line" ${2}$place ${4} > ${2}$(echo $place | tr ".root" "\n").out 0 && echo ${2}$place '(' $COUNTER $SLURM_JOB_ID.$SLURM_PROCID ')' done at $(date +"%T") &
            echo $COUNTER $SLURM_JOB_ID.$SLURM_PROCID
            let COUNTER++
        fi

    done
done
wait
