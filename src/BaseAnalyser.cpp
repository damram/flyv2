/*
 * BaseAnalyser.cpp
 *
 *  Created on: May 6, 2022
 *      Author: suyong
 *      Developper: cdozen
 */

#include "Math/GenVector/VectorUtil.h"
#include "BaseAnalyser.h"
#include "utility.h"
#include <fstream>
#include "correction.h"
#include <TLorentzVector.h>

using namespace TMVA::Experimental;
using correction::CorrectionSet;


BaseAnalyser::BaseAnalyser(TTree *t, std::string outfilename)
:NanoAODAnalyzerrdframe(t, outfilename)
{
    //initiliaze the HLT names in your analyzer class
    HLT2018Names= {"Name1","Name2"};
	HLT2016Names= {"Name1","Name2"};
    HLT2017Names= {"HLT_IsoMu27","HLT_IsoMu30", "HLT_Mu50", "HLT_Mu55",
    //SingleMuon
    "HLT_Ele32_WPTight_Gsf","HLT_Ele35_WPTight_Gsf","HLT_Ele38_WPTight_Gsf","HLT_Ele40_WPTight_Gsf",
    //SingleElectron 
    "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8", "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8","HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ_Mass3p8",
    //DoubleMuon
    "HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL", "HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ", "HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ",
    //MuonElectron
    "HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL","HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ",
    //DoubleElectron
    "HLT_TripleMu_10_5_5_DZ", "HLT_TripleMu_5_3_3_Mass3p8to60_DZ", "HLT_TripleMu_12_10_5",
    //TripleMuon
    "HLT_DiMu9_Ele9_CaloIdL_TrackIdL", "HLT_DiMu9_Ele9_CaloIdL_TrackIdL_DZ", "HLT_Mu8_DiEle12_CaloIdL_TrackIdL", "HLT_Mu8_DiEle12_CaloIdL_TrackIdL_DZ",
    //DoubleMuonElectron
    "HLT_Ele16_Ele12_Ele8_CaloIdL_TrackIdL",
    //TripleElectron
    "HLT_Ele115_CaloIdVT_GsfTrkIdT",
    "HLT_Photon200"};
}

// Define your cuts here
void BaseAnalyser::defineCuts(bool stepone)
{
	if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
	auto Nentry = _rlm.Count();
	// This is how you can express a range of the first 100 entries
    //_rlm = _rlm.Range(0, 100);
    //auto Nentry_100 = _rlm.Count();
	std::cout<< "-------------------------------------------------------------------" << std::endl;
    cout << "Usage of ranges:\n"
        << " - All entries: " << *Nentry << endl;
		//<< " - Entries from 0 to 100: " << *Nentry_100 << endl;
	std::cout<< "-------------------------------------------------------------------" << std::endl;
    addCuts("Ossf3l", "0");
    if(!stepone){
    addCuts(setHLT(), "00");
    
        addCuts("ZZ_CR", "000");
            if(!_isData){
                addCuts("nNotPromptLeptons==0", "0000");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons==0", "0001");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons!=0", "0002"); // DO i do nConversionLeptons==nNotPromptLeptons + another cat for both fake and conversion ?
            }
            // Fot stack purpose : everything with the same cut number
            else{
                addCuts("1==1", "0000");
                addCuts("1==1", "0001");
                addCuts("1==1", "0002");
            }
        addCuts("WZ_CR", "001");
            if(!_isData){
                addCuts("nNotPromptLeptons==0", "0010");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons==0", "0011");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons!=0", "0012");
            }
            // Fot stack purpose : everything with the same cut number
            else{
                addCuts("1==1", "0010");
                addCuts("1==1", "0011");
                addCuts("1==1", "0012");
            }
        addCuts("Conversion_CR", "002");
            if(!_isData){
                addCuts("nNotPromptLeptons==0", "0020");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons==0", "0021");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons!=0", "0022");
            }
            else{
                addCuts("1==1", "0020");
                addCuts("1==1", "0021");
                addCuts("1==1", "0022");
            }
        addCuts("region_ttz_3l_g", "003");
            if(!_isData){
                addCuts("nNotPromptLeptons==0", "0030");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons==0", "0031");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons!=0", "0032");
            }
            else{
                addCuts("1==1", "0030");
                addCuts("1==1", "0031");
                addCuts("1==1", "0032");
            }
        addCuts("region_ttz_4l_g", "004");
            if(!_isData){
                addCuts("nNotPromptLeptons==0", "0040");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons==0", "0041");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons!=0", "0042");
            }
            else{
                addCuts("1==1", "0040");
                addCuts("1==1", "0041");
                addCuts("1==1", "0042");
            }
        addCuts("region_sig", "005");
            if(!_isData){
                addCuts("nNotPromptLeptons==0", "0050");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons==0", "0051");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons!=0", "0052");
            }
            else{
                addCuts("1==1", "0050");
                addCuts("1==1", "0051");
                addCuts("1==1", "0052");
            }
        addCuts("3L_SR", "006");
            if(!_isData){
                addCuts("nNotPromptLeptons==0", "0060");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons==0", "0061");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons!=0", "0062");
            }
            else{
                addCuts("1==1", "0060");
                addCuts("1==1", "0061");
                addCuts("1==1", "0062");
            }
        addCuts("4L_SR", "007");
            if(!_isData){
                addCuts("nNotPromptLeptons==0", "0070");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons==0", "0071");
                addCuts("nNotPromptLeptons!=0 && nConversionLeptons!=0", "0072");
            }
            else{
                addCuts("1==1", "0070");
                addCuts("1==1", "0071");
                addCuts("1==1", "0072");
            }

    }
}
void BaseAnalyser::defineCutsHLT()
{
    	if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
	auto Nentry = _rlm.Count();
	// This is how you can express a range of the first 100 entries
    //_rlm = _rlm.Range(0, 100);
    //auto Nentry_100 = _rlm.Count();
	std::cout<< "-------------------------------------------------------------------" << std::endl;
    cout << "Usage of ranges:\n"
        << " - All entries: " << *Nentry << endl;
		//<< " - Entries from 0 to 100: " << *Nentry_100 << endl;
	std::cout<< "-------------------------------------------------------------------" << std::endl;

    addCuts(setHLT(), "0");
    addCuts("true","00");
}
//===============================Find Good Electrons===========================================//
//: Define Good Electrons in rdata frame
//=============================================================================================//

void BaseAnalyser::selectElectrons()
{
    cout << "select good electrons" << endl;
    if (debug){
    std::cout<< "================================//=================================" << std::endl;
    std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
    std::cout<< "================================//=================================" << std::endl;
    }

    //_rlm = _rlm.Define("ElectronPt10_4vecs", ::generate_4vec, {"Electron_pt", "Electron_eta", "Electron_phi", "Electron_mass"});
    // General cuts
    _rlm = _rlm.Define("ElectronSCBorE", "(abs(Electron_eta+Electron_deltaEtaSC)<1.4442 || abs(Electron_eta+Electron_deltaEtaSC)>1.5660) && abs(Electron_eta+Electron_deltaEtaSC)<2.5")
               .Define("ElectronAddCuts", "abs(Electron_dxy) < 0.05 && abs(Electron_dz) < 0.1 && Electron_sip3d < 8 && Electron_sieie < 0.030 && Electron_hoe < 0.07 && Electron_eInvMinusPInv < 0.005 && Electron_eInvMinusPInv > -0.05 && Electron_lostHits < 2 && Electron_miniPFRelIso_all < 0.4")
               .Define("tightElectronID", ElectronID(4))
               .Define("mediumID", ElectronID(3))
               .Define("mediumElectronID", "mediumID || tightElectronID")
               .Define("looseID", ElectronID(2))
               .Define("looseElectronID", "looseID || mediumElectronID")
               .Define("ElPt10", "Electron_pt>10.0");
    _rlm = defineNameElectron(_rlm, "ElPt10", _isData);

    _rlm = _rlm.Define("ElLmva", "Electron_pt>10.0 && abs(Electron_eta)<2.5 && Electron_miniPFRelIso_all < 0.4 && Electron_lostHits < 2 && abs(Electron_dxy) < 0.05 && abs(Electron_dz) < 0.1 && Electron_sip3d < 8 && looseElectronID");
    _rlm = defineNameElectron(_rlm, "ElLmva", _isData);
    _rlm = _rlm.Define("ElLmva_cutbased", "Electron_cutBased[ElLmva]")
               .Define("IsTightElLmva", "ElLmva_cutbased==4");


    _rlm = _rlm.Define("goodElectron", "ElLmva");
    _rlm = defineNameElectron(_rlm, "goodElectron", _isData);


   // Loose electrons definition
    _rlm = _rlm.Define("looseElectronsID", ElectronID(2))
               .Define("looseElectrons", "ElectronSCBorE && Electron_pt>10.0 && abs(Electron_eta)<2.5 && looseElectronsID")
               .Define("looseElectrons_pt", "Electron_pt[looseElectrons]")
               .Define("looseElectrons_eta", "Electron_eta[looseElectrons]")
               .Define("looseElectrons_phi", "Electron_phi[looseElectrons]")
               .Define("looseElectrons_mass", "Electron_mass[looseElectrons]")
               .Define("NlooseElectrons", "int(looseElectrons_pt.size())")
               .Define("looseElectrons_4Vecs", ::generate_4vec, {"looseElectrons_pt", "looseElectrons_eta", "looseElectrons_phi", "looseElectrons_mass"});
}
//===============================Find Good Muons===============================================//
//: Define Good Muons in rdata frame
//=============================================================================================//
void BaseAnalyser::selectMuons()
{

    cout << "select good muons" << endl;
    if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
    //All Muon
    _rlm = _rlm.Define("MuonPt10_4vecs", ::generate_4vec, {"Muon_pt", "Muon_eta", "Muon_phi", "Muon_mass"});
    //General cuts
    _rlm = _rlm.Define("tightMuonID", MuonID(4))
               .Define("mediumMuonID", MuonID(3))
               .Define("looseMuonID", MuonID(2))
               .Define("MuPt10", "Muon_pt>10")
               .Define("etaPtMuon", "Muon_pt > 10.0 && abs(Muon_eta)<2.4")
               .Define("MuonAddCuts", "abs(Muon_dxy) < 0.05 && abs(Muon_dz) < 0.1 && Muon_sip3d < 8 && Muon_miniPFRelIso_all < 0.4");

    _rlm = defineNameMuon(_rlm, "MuPt10", _isData);
    _rlm = _rlm.Define("MuLmva", "Muon_pt>10.0 && abs(Muon_eta)<2.4 && Muon_miniPFRelIso_all < 0.4 && abs(Muon_dxy) < 0.05 && abs(Muon_dz) < 0.1 && Muon_sip3d < 8");
    _rlm = defineNameMuon(_rlm, "MuLmva", _isData);
    _rlm = _rlm.Define("MuLmva_cutbased", "Muon_tightId[MuLmva]")
               .Define("IsTightMuLmva", "MuLmva_cutbased==true");


    _rlm = _rlm.Define("mediumMuon", "etaPtMuon && mediumMuonID && MuonAddCuts")
               .Define("mediumMuon_pt", "Muon_pt[mediumMuon]")
               .Define("mediumMuon_eta", "Muon_eta[mediumMuon]")
               .Define("mediumMuon_phi", "Muon_phi[mediumMuon]")
               .Define("mediumMuon_mass", "Muon_mass[mediumMuon]")
               .Define("NmediumMuon", "int(mediumMuon_pt.size())")
               .Define("mediumMuon_4vecs", ::generate_4vec, {"mediumMuon_pt", "mediumMuon_eta", "mediumMuon_phi", "mediumMuon_mass"});

    // Loose muon selection
    _rlm = _rlm.Define("looseMuon", "etaPtMuon && looseMuonID")
               .Define("looseMuon_pt", "Muon_pt[looseMuon]")
               .Define("looseMuon_eta", "Muon_eta[looseMuon]")
               .Define("looseMuon_phi", "Muon_phi[looseMuon]")
               .Define("looseMuon_mass", "Muon_mass[looseMuon]")
               .Define("NlooseMuon", "int(looseMuon_pt.size())")
               .Define("looseMuon_4vecs", ::generate_4vec, {"looseMuon_pt", "looseMuon_eta", "looseMuon_phi", "looseMuon_mass"});

    _rlm = _rlm.Define("goodMuon", "looseMuon");
    _rlm = defineNameMuon(_rlm, "goodMuon", _isData);
}

void BaseAnalyser::selectLeptons()
{
    //!!!Bit of a mess, try to clean this out

    // Conditions on leptons pt
    _rlm = _rlm.Define("goodLeptons_pt_nosort", ::addVecFloats, {"goodElectron_pt", "goodMuon_pt"})
               .Define("goodLeptons_pt", ::sortVec, {"goodLeptons_pt_nosort"})
               .Define("MaxPt", "goodLeptons_pt[0]")
               .Define("Pt40_20_10", ::Pt40_20_10, {"goodLeptons_pt"});

    // Global leptons variables
    _rlm = _rlm.Define("goodLeptons_eta", ::addVecFloats, {"goodElectron_eta", "goodMuon_eta"})
               .Define("goodLeptons_4vecs", ::addVecFourVec2, {"goodElectron_4vecs", "goodMuon_4vecs"})
               .Define("goodLeptons_energy", ::vecEnergies, {"goodLeptons_4vecs"});

    // Conditions on leptons flavors
    _rlm = _rlm.Define("OSSFpair", "goodMuonOSSF + goodElectronOSSF")
               .Define("Ossf3l", "OSSFpair > 0 && goodLeptons_pt.size()>2")
               .Define("Is2OSSFfromZ_20", ::Is2OSSFfromZ, {"goodElectron_charge", "goodElectron_4vecs","goodMuon_charge", "goodMuon_4vecs"});
  
    // Reconstruct OSSF pairs
    _rlm = _rlm.Define("LeptonsOSSF_4vec_high", ::VecPtLeptonsOSSF_v2, {"goodElectron_charge", "goodMuon_charge", "goodMuon_4vecs", "goodElectron_4vecs", "True", "False"})
               .Define("LeptonsOSSF_4vec1_high", "LeptonsOSSF_4vec_high[0]")
               .Define("LeptonsOSSF_4vec2_high", "LeptonsOSSF_4vec_high[1]")
               .Define("LeptonsOSSF_4vec_low", ::VecPtLeptonsOSSF_v2, {"goodElectron_charge", "goodMuon_charge", "goodMuon_4vecs", "goodElectron_4vecs","True", "False"})
               .Define("LeptonsNoZ_Pt_high", "LeptonsOSSF_4vec_low[0].Pt()")
               .Define("Z_4vec", ::sum_4vec, {"LeptonsOSSF_4vec1_high", "LeptonsOSSF_4vec2_high"})
               .Define("Z_pt", "Z_4vec.Pt()")
               .Define("invMassOSSF_pt", ::calculate_invMass_syst, {"LeptonsOSSF_4vec_high"})
               .Define("is10fromz", ::is10FromZ, {"invMassOSSF_pt"})
               .Define("deltaROSSF_pt", ::calculate_deltaR_syst, {"LeptonsOSSF_4vec_high"});

    // Collection of all the leptons
    _rlm = _rlm.Define("all_leptons", ::addVecFourVec2, {"goodElectron_4vecs","goodMuon_4vecs"})
               .Define("all_leptons_M", calculate_invMass_syst, {"all_leptons"})
               .Define("nAllLeptons", "int(all_leptons.size())")
               .Define("all_leptons_loose", ::addVecFourVec2, {"looseElectrons_4Vecs","looseMuon_4vecs"})
               .Define("all_leptons_eta", ::addVecFloats, {"goodElectron_eta","goodMuon_eta"});
    
    // leptons
    _rlm = _rlm.Define("leadingLepton_pt", "goodLeptons_pt.size()>0 ? goodLeptons_pt[0] : -99")
               .Define("sub_leadingLepton_pt", "goodLeptons_pt.size()>1 ? goodLeptons_pt[1] : -99")
               .Define("trailing_lepton_pt", "goodLeptons_pt.size()>2 ? goodLeptons_pt[2] : -99")
               .Define("leadingLepton_eta", ::leading, {"goodLeptons_pt_nosort", "goodLeptons_eta"})
               .Define("subLeadingLepton_eta", ::subleading, {"goodLeptons_pt_nosort", "goodLeptons_eta"})
               .Define("trailingLepton_eta", ::trailing, {"goodLeptons_pt_nosort", "goodLeptons_eta"})
               .Define("leadingLepton_energy", ::leading, {"goodLeptons_pt_nosort", "goodLeptons_energy"})
               .Define("subLeadingLepton_energy", ::subleading, {"goodLeptons_pt_nosort", "goodLeptons_energy"})
               .Define("trailingLepton_energy", ::trailing, {"goodLeptons_pt_nosort", "goodLeptons_energy"})
               .Define("maxdRll", ::maxdR, {"goodLeptons_4vecs", "goodLeptons_4vecs"});

    // To have access to MC truth. Will be updated with data driven method
    if(!_isData){
    _rlm = _rlm.Define("genDressedLepton_pt", "GenDressedLepton_pt")
               .Define("genDressedLepton_eta", "GenDressedLepton_eta")
               .Define("genDressedLepton_phi", "GenDressedLepton_phi")
               .Define("genDressedLepton_m", "GenDressedLepton_mass")
               .Define("genDressedLepton_4vec", ::generate_4vec, {"genDressedLepton_pt", "genDressedLepton_eta", "genDressedLepton_phi", "genDressedLepton_m"})
               .Define("ngenDressed", ::CountGenDressed, {"all_leptons", "genDressedLepton_4vec"})
               .Define("TrueElectron", ::isThisPrompt, {"goodElectron", "Electron_genPartIdx", "GenPart_pdgId", "GenPart_genPartIdxMother"})
               .Define("TrueMuon", ::isThisPrompt, {"goodMuon", "Muon_genPartIdx", "GenPart_pdgId", "GenPart_genPartIdxMother"})
               .Define("nTrueMuon", "int(TrueMuon.size())")
               .Define("nTrueElectron", "int(TrueElectron.size())")
               .Define("associatedLeptons", ::associatedLeptons, {"all_leptons", "genDressedLepton_4vec", "True"})
               .Define("nTrueLeptons", "int(TrueElectron.size()+TrueMuon.size())") // previously associatedLeptons.size()
               .Define("nFakeLeptons", "int( all_leptons.size() - nTrueLeptons )")
               .Define("nPromptLeptons", "int(goodElectronPromptPt.size() + goodMuonPromptPt.size())")
               .Define("nNotPromptLeptons", "int(all_leptons.size() - nPromptLeptons)")
               .Define("nConversionLeptons", "int(goodElectronConversionPt.size() + goodMuonConversionPt.size())")
               .Define("nNotConversionLeptons", "int(all_leptons.size() - nConversionLeptons)");

    // Test for saving it
    _rlm = _rlm.Define("genPart_pt", "GenPart_pt");
    }
}
//=================================Select Jets=================================================//
//check the twiki page :    https://twiki.cern.ch/twiki/bin/view/CMS/JetID
//to find jetId working points for the purpose of  your analysis.
    //jetId==2 means: pass tight ID, fail tightLepVeto
    //jetId==6 means: pass tight ID and tightLepVeto ID.
//=============================================================================================//

void BaseAnalyser::selectJets(bool BTagEff)
{

    cout << "select good jets" << endl;
    if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }

    // Tight jets selection
    _rlm = _rlm.Define("tightJetsID", JetID(6))
               .Define("tightJets", "Jet_pt>30.0 && abs(Jet_eta)<2.4 && tightJetsID && Jet_neHEF < 0.9 && Jet_neEmEF < 0.9 && Jet_nConstituents > 1 && Jet_chHEF > 0")
               .Define("tightJets_pt", "Jet_pt[tightJets]")
               .Define("tightJets_eta", "Jet_eta[tightJets]")
               .Define("tightJets_phi", "Jet_phi[tightJets]")
               .Define("tightJets_mass", "Jet_mass[tightJets]")
               .Define("tightJets_idx", ::good_idx, {"tightJets"})
               .Define("tightJets_btag", "Jet_btagDeepB[tightJets]")
               .Define("tightJets_4vecs", ::generate_4vec, {"tightJets_pt", "tightJets_eta", "tightJets_phi", "tightJets_mass"})
               .Define("NtightJets", "tightJets_pt.size()");


    // Tight (deepCSV) bjets selection
    _rlm = _rlm.Define("btagcuts", "tightJets_btag>0.4506")
               .Define("good_bjetpt", "tightJets_pt[btagcuts]")
               .Define("good_bjeteta", "tightJets_eta[btagcuts]")
               .Define("good_bjetphi", "tightJets_phi[btagcuts]")
               .Define("good_bjetmass", "tightJets_mass[btagcuts]")
               .Define("Ngood_bjets", "int(good_bjetpt.size())")
               .Define("good_bjet4vecs", ::generate_4vec, {"good_bjetpt", "good_bjeteta", "good_bjetphi", "good_bjetmass"});

    // Clean the jets
    _rlm = _rlm.Define("cleanJets", ::checkoverlap, {"tightJets_4vecs","all_leptons_loose"});
	_rlm = _rlm.Define("Clean_jetpt", "tightJets_pt[cleanJets]")
               .Define("Clean_jeteta", "tightJets_eta[cleanJets]")
               .Define("Clean_jetphi", "tightJets_phi[cleanJets]")
               .Define("Clean_jetmass", "tightJets_mass[cleanJets]")
               .Define("Clean_jetbtag", "tightJets_btag[cleanJets]")
               .Define("NcleanJets", "int(Clean_jetpt.size())")
               .Define("cleanjet4vecs", ::generate_4vec, {"Clean_jetpt", "Clean_jeteta", "Clean_jetphi", "Clean_jetmass"})
               .Define("Clean_jetHT", "Sum(Clean_jetpt)");

    _rlm = _rlm.Define("leadingJets_pt", ::leading, {"Clean_jetpt", "Clean_jetpt"})
               .Define("subLeadingJets_pt", ::subleading, {"Clean_jetpt", "Clean_jetpt"})
               .Define("trailingJets_pt", ::trailing, {"Clean_jetpt", "Clean_jetpt"})
               .Define("leadingJets_eta", ::leading, {"Clean_jetpt", "Clean_jeteta"})
               .Define("subLeadingJets_eta", ::subleading, {"Clean_jetpt", "Clean_jeteta"})
               .Define("trailingJets_eta", ::trailing, {"Clean_jetpt", "Clean_jeteta"})
               .Define("leadingJets_mass", ::leading, {"Clean_jetpt", "Clean_jetmass"})
               .Define("subLeadingJets_mass", ::subleading, {"Clean_jetpt", "Clean_jetmass"})
               .Define("leadingJets_btag", ::leading, {"Clean_jetpt", "Clean_jetbtag"})
               .Define("subLeadingJets_btag", ::subleading, {"Clean_jetpt", "Clean_jetbtag"});

    // Clean the bjets
	_rlm = _rlm.Define("btagcuts2", "Clean_jetbtag>0.4506")
               .Define("Clean_bjetpt", "Clean_jetpt[btagcuts2]")
               .Define("Clean_bjeteta", "Clean_jeteta[btagcuts2]")
               .Define("Clean_bjetphi", "Clean_jetphi[btagcuts2]")
               .Define("Clean_bjetmass", "Clean_jetmass[btagcuts2]")
               .Define("NcleanBJets", "int(Clean_bjetpt.size())")
               .Define("Clean_bjetHT", "Sum(Clean_bjetpt)")
               .Define("cleanbjet4vecs", ::generate_4vec, {"Clean_bjetpt", "Clean_bjeteta", "Clean_bjetphi", "Clean_bjetmass"});

    _rlm = _rlm.Define("leadingCleanbjets_pt", ::leading, {"Clean_bjetpt", "Clean_bjetpt"})
               .Define("leadingCleanbjets_eta", ::leading, {"Clean_bjetpt", "Clean_bjeteta"});

    // Variable for efficiency
    _rlm = _rlm.Define("tightJet_btagDeepFlavB", "Jet_btagDeepFlavB[tightJets]")
               .Define("CleanJet_btagDeepFlavB", "tightJet_btagDeepFlavB[cleanJets]")
               .Define("Selected_clean_jet_btagDeepFlavB", "CleanJet_btagDeepFlavB");
    if(!_isData){
        _rlm = _rlm.Define("tightJet_hadronFlavour", "Jet_hadronFlavour[tightJets]")
                   .Define("CleanJet_hadronFlavour", "tightJet_hadronFlavour[cleanJets]")
                   .Define("Selected_clean_bjet_hadronFlavour","CleanJet_hadronFlavour[btagcuts2]")
                   .Define("Selected_clean_jet_hadronFlavour", "CleanJet_hadronFlavour");
    }

    // For bdt
    _rlm = _rlm.Define("minDrlb", ::mindR, {"all_leptons", "cleanbjet4vecs"})
               .Define("maxdRjj", ::maxdR, {"cleanjet4vecs", "cleanjet4vecs"})
               .Define("Clean_jeteta_sorted", ::sortVec, {"Clean_jeteta"})
               .Define("maxjeteta", ::leading, {"Clean_jeteta", "Clean_jeteta"})
               .Define("maxmjj", ::maxM, {"cleanjet4vecs", "cleanjet4vecs"});

    // All objets' variables

    _rlm = _rlm.Define("maxptJetLep", "leadingJets_pt > leadingLepton_pt ? leadingJets_pt : leadingLepton_pt");

    // _rlm = _rlm.Define("allSyst_woMet", ::addVecFourVec2, {"cleanjet4vecs", "all_leptons"})
    //            .Define("allSyst", ::addVecFourVec2, {"allSyst_woMet", "Met_4vec"}); // Only for 3l final state

    if(BTagEff == 1 && !_isData)
    {
        _rlm = _rlm.Define("Selected_clean_bjet_btagpass_bcflav","Selected_clean_bjet_hadronFlavour != 0")
                   .Define("Selected_clean_bjet_btagpass_bcflav_pt","Clean_bjetpt[Selected_clean_bjet_btagpass_bcflav]")
                   .Define("Selected_clean_bjet_btagpass_bcflav_eta","abs(Clean_bjeteta[Selected_clean_bjet_btagpass_bcflav])");
        _rlm = _rlm.Define("Selected_clean_jet_all_bcflav","Selected_clean_jet_hadronFlavour != 0")
                   .Define("Selected_clean_jet_all_bcflav_pt","Clean_jetpt[Selected_clean_jet_all_bcflav]")
                   .Define("Selected_clean_jet_all_bcflav_eta","abs(Clean_jeteta[Selected_clean_jet_all_bcflav])");
        _rlm = _rlm.Define("Selected_clean_bjet_btagpass_lflav","Selected_clean_bjet_hadronFlavour == 0")
                   .Define("Selected_clean_bjet_btagpass_lflav_pt","Clean_bjetpt[Selected_clean_bjet_btagpass_lflav]")
                   .Define("Selected_clean_bjet_btagpass_lflav_eta","abs(Clean_bjeteta[Selected_clean_bjet_btagpass_lflav])");
        _rlm = _rlm.Define("Selected_clean_jet_all_lflav","Selected_clean_jet_hadronFlavour == 0")
                   .Define("Selected_clean_jet_all_lflav_pt","Clean_jetpt[Selected_clean_jet_all_lflav]")
                   .Define("Selected_clean_jet_all_lflav_eta","abs(Clean_jeteta[Selected_clean_jet_all_lflav])");
    }

}

void BaseAnalyser::saveForRoc(){
    addVartoStore("goodMuonMvaScore");
    addVartoStore("isPromptgoodMuon");

    addVartoStore("MuPt10MvaScore");
    addVartoStore("isPromptMuPt10");

    addVartoStore("ElPt10MvaScore");
    addVartoStore("isPromptElPt10");

    addVartoStore("ElLmvaMvaScore");
    addVartoStore("isPromptElLmva");
    addVartoStore("IsTightElLmva");

    addVartoStore("MuLmvaMvaScore");
    addVartoStore("isPromptMuLmva");
    addVartoStore("IsTightMuLmva");

    addVartoStore("closestJetBTagMuon");
    addVartoStore("closestJetBTagEl");

    //For the bdt
    addVartoStore("NcleanJets"); 
    addVartoStore("NcleanBJets");
    addVartoStore("leadingLepton_pt");
    addVartoStore("sub_leadingLepton_pt");
    addVartoStore("trailing_lepton_pt");
    addVartoStore("MET_pt");
    addVartoStore("invMassOSSF_pt");
    addVartoStore("deltaROSSF_pt");
    addVartoStore("evWeight");
    addVartoStore("leadingLepton_eta");
    addVartoStore("subLeadingLepton_eta");
    addVartoStore("leadingLepton_energy");
    addVartoStore("subLeadingLepton_energy");
    addVartoStore("minDrlb");
    addVartoStore("leadingJets_pt");
    addVartoStore("leadingJets_eta");
    addVartoStore("leadingJets_mass");
    addVartoStore("subLeadingJets_pt");
    addVartoStore("subLeadingJets_eta");
    addVartoStore("subLeadingJets_mass");
    addVartoStore("leadingCleanbjets_pt");
    addVartoStore("Clean_jetHT");
    
}

void BaseAnalyser::applyMva(){
    _rlm = _rlm.Define("closestJetBTagMuon", ::closestJetCSV, {"tightJets_4vecs", "goodMuon_4vecs", "tightJet_btagDeepFlavB"});
    _rlm = _rlm.Define("closestJetBTagMuPt10", ::closestJetCSV, {"tightJets_4vecs", "MuPt10_4vecs", "tightJet_btagDeepFlavB"});
    _rlm = _rlm.Define("closestJetBTagElPt10", ::closestJetCSV, {"tightJets_4vecs", "ElPt10_4vecs", "tightJet_btagDeepFlavB"});
    _rlm = _rlm.Define("closestJetBTagElLmva", ::closestJetCSV, {"tightJets_4vecs", "ElLmva_4vecs", "tightJet_btagDeepFlavB"});
    _rlm = _rlm.Define("closestJetBTagMuLmva", ::closestJetCSV, {"tightJets_4vecs", "MuLmva_4vecs", "tightJet_btagDeepFlavB"});
    _rlm = _rlm.Define("closestJetBTagEl", ::closestJetCSV, {"tightJets_4vecs", "goodElectron_4vecs", "tightJet_btagDeepFlavB"});
    _rlm = _rlm.Define("goodMuonMvaWp", ::getMvaWpMuon, {"goodMuon_pdgId", "goodMuon_pt", "goodMuon_eta", "goodMuon_jetNDauCharged", "goodMuon_miniPFRelIso_chg", "goodMuon_miniPFRelIso_all", "goodMuon_jetPtRelv2", "goodMuon_jetRelIso", "goodMuon_pfRelIso03_all", "closestJetBTagMuon", "goodMuon_sip3d", "goodMuon_dxy", "goodMuon_dz", "goodMuon_segmentComp"});
    _rlm = _rlm.Define("goodMuonMvaScore", ::getMvaScoreMuon, {"goodMuon_pdgId", "goodMuon_pt", "goodMuon_eta", "goodMuon_jetNDauCharged", "goodMuon_miniPFRelIso_chg", "goodMuon_miniPFRelIso_all", "goodMuon_jetPtRelv2", "goodMuon_jetRelIso", "goodMuon_pfRelIso03_all", "closestJetBTagMuon", "goodMuon_sip3d", "goodMuon_dxy", "goodMuon_dz", "goodMuon_segmentComp"});
    _rlm = _rlm.Define("goodElMvaWp", ::getMvaWpEl, {"goodElectron_pdgId", "goodElectron_pt", "goodElectron_eta", "goodElectron_jetNDauCharged", "goodElectron_miniPFRelIso_chg", "goodElectron_miniPFRelIso_all", "goodElectron_jetPtRelv2", "goodElectron_jetRelIso", "goodElectron_pfRelIso03_all", "closestJetBTagEl", "goodElectron_sip3d", "goodElectron_dxy", "goodElectron_dz", "goodElectron_mvaFall17V2noIso", "goodElectron_lostHits"});
    _rlm = _rlm.Define("goodElMvaScore", ::getMvaScoreEl, {"goodElectron_pdgId", "goodElectron_pt", "goodElectron_eta", "goodElectron_jetNDauCharged", "goodElectron_miniPFRelIso_chg", "goodElectron_miniPFRelIso_all", "goodElectron_jetPtRelv2", "goodElectron_jetRelIso", "goodElectron_pfRelIso03_all", "closestJetBTagEl", "goodElectron_sip3d", "goodElectron_dxy", "goodElectron_dz", "goodElectron_mvaFall17V2noIso", "goodElectron_lostHits"});
    _rlm = _rlm.Define("passMva", ::passMva, {"goodMuonMvaWp", "goodElMvaWp"});
    // _rlm = _rlm.Define("passMva", "one");
    // std::cout<<"Carefull. TopLMVA isn't applied. This is for bdt training only. Reapply when analysing the data."<<std::endl;

    //Efficiency
    if(!_isData){
        _rlm = _rlm.Define("elTightMva", "goodElMvaWp==4")
                .Define("muTightMva", "goodMuonMvaWp==4")
                .Define("elTightMva_genPartFlav", "goodElectron_genPartFlav[elTightMva]")
                .Define("muTightMva_genPartFlav", "goodMuon_genPartFlav[muTightMva]")
                .Define("elPromptMva","elTightMva_genPartFlav==1")
                .Define("muPromptMva","muTightMva_genPartFlav==1")
                .Define("elTightMva_pt", "goodElectron_pt[elTightMva]")
                .Define("muTightMva_pt", "goodMuon_pt[muTightMva]")
                .Define("elPromptMva_pt", "elTightMva_pt[elPromptMva]")
                .Define("muPromptMva_pt", "muTightMva_pt[muPromptMva]")
                .Define("nPromptMva", "int(elPromptMva_pt.size() + muPromptMva_pt.size())")
                .Define("nNotPromptMva", "int(elTightMva_genPartFlav.size() + muTightMva_genPartFlav.size() - nPromptMva)");
        // Test for el>10GeV uniquement
        // _rlm = _rlm.Define("ElPt10_genPartFlav", "Electron_genPartFlav[ElPt10]")
        //            .Define("ElPt10Prompt", "ElPt10_genPartFlav==1")
        //            .Define("nElPt10Prompt", "Sum(ElPt10Prompt)")
        //            .Define("nElPt10NotPrompt", "int(ElPt10Prompt.size() - nElPt10Prompt)");
        // // Test for mu>10GeV uniquement
        // _rlm = _rlm.Define("MuPt10_genPartFlav", "Muon_genPartFlav[MuPt10]")
        //            .Define("MuPt10Prompt", "MuPt10_genPartFlav==1")
        //            .Define("nMuPt10Prompt", "Sum(MuPt10Prompt)")
        //            .Define("nMuPt10NotPrompt", "int(MuPt10Prompt.size() - nMuPt10Prompt)");
        // Mva For those leptons :
        // _rlm = _rlm.Define("muon10MvaWp", ::getMvaWpMuon, {"Muon_pdgId", "Muon_pt", "Muon_eta", "Muon_jetNDauCharged", "Muon_miniPFRelIso_chg", "Muon_miniPFRelIso_all", "Muon_jetPtRelv2", "Muon_jetRelIso", "Muon_pfRelIso03_all", "closestJetBTagMuon", "Muon_sip3d", "goodMuon_dxy", "goodMuon_dz", "goodMuon_segmentComp"});
        // _rlm = _rlm.Define("el10MvaWp", ::getMvaWpEl, {"goodElectron_pdgId", "goodElectron_pt", "goodElectron_eta", "goodElectron_jetNDauCharged", "goodElectron_miniPFRelIso_chg", "goodElectron_miniPFRelIso_all", "goodElectron_jetPtRelv2", "goodElectron_jetRelIso", "goodElectron_pfRelIso03_all", "closestJetBTagEl", "goodElectron_sip3d", "goodElectron_dxy", "goodElectron_dz", "goodElectron_mvaFall17V2noIso", "goodElectron_lostHits"});
    }
    //RocCurve
    if(!_isData){
        _rlm = _rlm.Define("MuPt10MvaScore", ::getMvaScoreMuon, {"MuPt10_pdgId", "MuPt10_pt", "MuPt10_eta", "MuPt10_jetNDauCharged", "MuPt10_miniPFRelIso_chg", "MuPt10_miniPFRelIso_all", "MuPt10_jetPtRelv2", "MuPt10_jetRelIso", "MuPt10_pfRelIso03_all", "closestJetBTagMuPt10", "MuPt10_sip3d", "MuPt10_dxy", "MuPt10_dz", "MuPt10_segmentComp"});
        _rlm = _rlm.Define("ElPt10MvaScore", ::getMvaScoreEl, {"ElPt10_pdgId", "ElPt10_pt", "ElPt10_eta", "ElPt10_jetNDauCharged", "ElPt10_miniPFRelIso_chg", "ElPt10_miniPFRelIso_all", "ElPt10_jetPtRelv2", "ElPt10_jetRelIso", "ElPt10_pfRelIso03_all", "closestJetBTagElPt10", "ElPt10_sip3d", "ElPt10_dxy", "ElPt10_dz", "ElPt10_mvaFall17V2noIso", "ElPt10_lostHits"});
        _rlm = _rlm.Define("ElLmvaMvaScore", ::getMvaScoreEl, {"ElLmva_pdgId", "ElLmva_pt", "ElLmva_eta", "ElLmva_jetNDauCharged", "ElLmva_miniPFRelIso_chg", "ElLmva_miniPFRelIso_all", "ElLmva_jetPtRelv2", "ElLmva_jetRelIso", "ElLmva_pfRelIso03_all", "closestJetBTagElLmva", "ElLmva_sip3d", "ElLmva_dxy", "ElLmva_dz", "ElLmva_mvaFall17V2noIso", "ElLmva_lostHits"});
        _rlm = _rlm.Define("MuLmvaMvaScore", ::getMvaScoreMuon, {"MuLmva_pdgId", "MuLmva_pt", "MuLmva_eta", "MuLmva_jetNDauCharged", "MuLmva_miniPFRelIso_chg", "MuLmva_miniPFRelIso_all", "MuLmva_jetPtRelv2", "MuLmva_jetRelIso", "MuLmva_pfRelIso03_all", "closestJetBTagMuLmva", "MuLmva_sip3d", "MuLmva_dxy", "MuLmva_dz", "MuLmva_segmentComp"});

    }

}

void BaseAnalyser::defineRegion()
{
  cout << "define Region" << endl;
  if (debug){
      std::cout<< "================================//=================================" << std::endl;
      std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
      std::cout<< "================================//=================================" << std::endl;
  }

    // Define 4-leptons region
    _rlm = _rlm.Define("region_4l", "NgoodElectron+NgoodMuon == 4 && (Sum(goodMuon_charge)+Sum(goodElectron_charge)) == 0 && OSSFpair == 1 && MaxPt > 40 && passMva")
               .Define("region_4l_2jp_0b", "region_4l && NcleanJets > 1 && NcleanBJets == 0")//0b
               .Define("region_4l_2jp_1b", "region_4l && NcleanJets > 1 && NcleanBJets == 1")
               .Define("region_4l_2jp_0pb", "region_4l && NcleanJets > 1 && NcleanBJets >0");//1b

    // Define 3-leptons region
     _rlm = _rlm.Define("region_3l", "NgoodElectron+NgoodMuon == 3 && OSSFpair > 0 && Pt40_20_10 && is10fromz && passMva")
                .Define("region_3l_1j", "region_3l && NcleanJets==1")//no b selection
                .Define("region_3l_2j", "region_3l && NcleanJets==2")
                .Define("region_3l_3j", "region_3l && NcleanJets==3")
                .Define("region_3l_4j", "region_3l && NcleanJets==4")
                .Define("region_3l_4pj", "region_3l && NcleanJets>4")
                .Define("region_3l_1j_0b", "region_3l && NcleanJets==1 && NcleanBJets == 0")//0b
                .Define("region_3l_2j_0b", "region_3l && NcleanJets==2 && NcleanBJets == 0")
                .Define("region_3l_3j_0b", "region_3l && NcleanJets==3 && NcleanBJets == 0")
                .Define("region_3l_4pj_0b", "region_3l && NcleanJets>=4 && NcleanBJets == 0")
                .Define("region_3l_2j_1b", "region_3l && NcleanJets==2 && NcleanBJets == 1")//1b
                .Define("region_3l_3j_1b", "region_3l && NcleanJets==3 && NcleanBJets == 1")
                .Define("region_3l_4j_1b", "region_3l && NcleanJets==4 && NcleanBJets == 1")
                .Define("region_3l_4pj_1b", "region_3l && NcleanJets>4 && NcleanBJets == 1")
                .Define("region_3l_2j_1pb", "region_3l && NcleanJets==2 && NcleanBJets > 1")//>1b
                .Define("region_3l_3j_1pb", "region_3l && NcleanJets==3 && NcleanBJets > 1")
                .Define("region_3l_3pj_1pb", "region_3l && NcleanJets>2 && NcleanBJets > 0") // just for this one >0b
                .Define("region_3l_4j_1pb", "region_3l && NcleanJets==4 && NcleanBJets > 1")
                .Define("region_3l_4pj_1pb", "region_3l && NcleanJets>4 && NcleanBJets > 1");
    
    //Signal 3l region
    _rlm = _rlm.Define("3L_SR", "NgoodElectron+NgoodMuon == 3 && OSSFpair > 0 && Pt40_20_10 && passMva && ((NcleanBJets >= 1 && NcleanJets >=2) || (NcleanBJets == 0 && NcleanJets >=1))");

    //Signal 4l region
    _rlm = _rlm.Define("4L_SR", "NgoodElectron+NgoodMuon == 4 && (Sum(goodMuon_charge)+Sum(goodElectron_charge)) == 0 && OSSFpair == 1 && MaxPt > 40 && passMva && NcleanJets >= 2");

    // Define Njets/bjet for 3l ttZ region (try to do it smartly)
    _rlm = _rlm.Define("region_ttz_3l_g", "region_3l && NcleanJets>0 && passMva")
               .Define("region_ttz_3l_0b", "region_3l_1j_0b ? 0 : region_3l_2j_0b ? 1 : region_3l_3j_0b ? 2 : region_3l_4pj_0b ? 3 : -99")//no lep njet - 1
               .Define("region_ttz_3l_1b", "region_3l_2j_1b ? 4 : region_3l_3j_1b ? 5 : region_3l_4j_1b ? 6 : region_3l_4pj_1b ? 7 : -99")
               .Define("region_ttz_3l_1pb", "region_3l_2j_1pb ? 8 : region_3l_3j_1pb ? 9 : region_3l_4j_1pb ? 10 : region_3l_4pj_1pb ? 11 : -99")
               .Define("region_ttz_3l", "region_ttz_3l_0b != -99 ? region_ttz_3l_0b : region_ttz_3l_1b != -99 ? region_ttz_3l_1b : region_ttz_3l_1pb")
               .Define("region_ttz_3l_0e", "NgoodElectron==0 ? region_ttz_3l : -99")//0electron
               .Define("region_ttz_3l_1e", "NgoodElectron==1 ? region_ttz_3l : -99")//1electron
               .Define("region_ttz_3l_2e", "NgoodElectron==2 ? region_ttz_3l : -99")//2electron
               .Define("region_ttz_3l_3e", "NgoodElectron==3 ? region_ttz_3l : -99");//3electron

    // Define Njet/bjets for 4l ttZ region (try to do it smartly)
    _rlm = _rlm.Define("region_ttz_4l_g", "region_4l && NcleanJets>1 && passMva")
               .Define("region_ttz_4l", "region_4l_2jp_0b ? 0 : region_4l_2jp_0pb ? 1 : -99");

    // Define Njet/bjet for 3-4-l ttZ region
    _rlm = _rlm.Define("region_ttz", "region_ttz_3l != -99 ? region_ttz_3l : region_ttz_4l + 11");

    // Define Njet/bjet for signal region
    _rlm = _rlm.Define("region_sig", "region_4l_2jp_0pb || region_3l_3pj_1pb")
               .Define("region_3L_SR", "region_3l_3pj_1pb ? NgoodElectron : -99")
               .Define("region_4L_SR", "region_4l_2jp_0pb ? NgoodElectron : -99");

    // Define Nelectron for WZ region -> This is stupid, just do N electron
    _rlm = _rlm.Define("WZ_CR", "region_3l && NcleanBJets==0 && passMva && fabs(invMassOSSF_pt-Zmass)<10 && MET_pt > 50") // I removed the MET cut
               .Define("region_WZ", "NgoodElectron");

    // Define Nelectron for ZZ region -> This is stupid, just do N electron
    _rlm = _rlm.Define("ZZ_CR", "NgoodElectron+NgoodMuon == 4 && Is2OSSFfromZ_20 && passMva && MaxPt > 40 && MET_pt < 50") // I removed MET_pt < 50
               .Define("region_4l_ZZ", "NgoodElectron/2")
               .Define("invMassRegion4lZZ", "ZZ_CR ? invMassOSSF_pt : -99"); //because otherwise the cut is not specified in cut-section it crashes
              //Lepton supposed to pass the ttZ 4L Lepton MVA

    // Define Ne for conversion region -> This is stupid, just do N electron
    _rlm = _rlm.Define("Conversion_CR", "fabs(all_leptons_M-Zmass)<10 && NgoodElectron+NgoodMuon == 3 && passMva")
               .Define("region_conversion", "NgoodElectron");


    _rlm = _rlm.Define("region_infZ", "invMassOSSF_pt<70")
               .Define("region_Z", "invMassOSSF_pt>81 && invMassOSSF_pt < 101")
               .Define("region_supZ", "invMassOSSF_pt>110");

}
void BaseAnalyser::defineObservable(){
    _rlm = _rlm.Define("nJetsObservable_0bj", "NcleanJets < 4 ? NcleanJets : 4")
               .Define("nJetsObservable_1bj", "NcleanJets < 5 ? NcleanJets : 5")
               .Define("nJetsObservable_2bj", "NcleanJets < 5 ? NcleanJets : 5")
               .Define("nJetsObservable", "NcleanBJets==0 ? nJetsObservable_0bj-1 : NcleanBJets==1 ? nJetsObservable_1bj+2 : nJetsObservable_1bj+6");
    _rlm = _rlm.Define("nJets_Obsnonprompt", "NcleanJets < 3 ? 0 : NcleanJets > 3 ? 2 : 1")
               .Define("nBjets_Obsnonprompt", "NcleanBJets > 1 ? 2 : NcleanBJets");
;
    _rlm = _rlm.Define("Observable_3l", "nJetsObservable")
               .Define("Observable_4l", "NcleanBJets > 0 ? 1 : 0")
               .Define("Observable_ZZ", "(NcleanBJets==0 && NcleanJets==1) ? 0 : (NcleanBJets==0 && NcleanJets>1) ? 1 : NcleanBJets>0 && NcleanJets==1 ? 2 : NcleanBJets>0 && NcleanJets>1 ? 3 : -99")
               .Define("Observable_WZ", "NcleanJets < 4 ? NcleanJets : 3")
               .Define("Observable_nonprompt", "nBjets_Obsnonprompt==0 ? nJets_Obsnonprompt : nBjets_Obsnonprompt==1 ? nJets_Obsnonprompt + 3 : nJets_Obsnonprompt + 6");
    addVartoStore("Observable.*");
}
void BaseAnalyser::selectMET()
{
    if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }

    _rlm = _rlm.Define("goodMET_sumET","MET_sumEt>800")
               .Define("goodMET_pt","MET_pt>5")
               .Define("goodMET_eta","MET_eta[goodMET]")
               .Define("goodMET_phi","MET_phi[goodMET]")
               .Define("NgoodMET","int(goodMET_pt.size())");
}


void BaseAnalyser::removeOverlaps()
{
    // Clean the jets
    _rlm = _rlm.Define("cleanJets", ::checkoverlap, {"tightJets_4vecs","all_leptons_loose"});
	_rlm = _rlm.Define("Clean_jetpt", "tightJets_pt[cleanJets]")
               .Define("Clean_jeteta", "tightJets_eta[cleanJets]")
               .Define("Clean_jetphi", "tightJets_phi[cleanJets]")
               .Define("Clean_jetmass", "tightJets_mass[cleanJets]")
               .Define("Clean_jetbtag", "tightJets_btag[cleanJets]")
               .Define("NcleanJets", "int(Clean_jetpt.size())")
               .Define("cleanjet4vecs", ::generate_4vec, {"Clean_jetpt", "Clean_jeteta", "Clean_jetphi", "Clean_jetmass"})
               .Define("Clean_jetHT", "Sum(Clean_jetpt)");

    

    // Clean the bjets
	_rlm = _rlm.Define("btagcuts2", "Clean_jetbtag>0.4506")
               .Define("Clean_bjetpt", "Clean_jetpt[btagcuts2]")
               .Define("Clean_bjeteta", "Clean_jeteta[btagcuts2]")
               .Define("Clean_bjetphi", "Clean_jetphi[btagcuts2]")
               .Define("Clean_bjetmass", "Clean_jetmass[btagcuts2]")
               .Define("NcleanBJets", "int(Clean_bjetpt.size())")
               .Define("Clean_bjetHT", "Sum(Clean_bjetpt)")
               .Define("cleanbjet4vecs", ::generate_4vec, {"Clean_bjetpt", "Clean_bjeteta", "Clean_bjetphi", "Clean_bjetmass"});

}


//=============================define variables==================================================//
void BaseAnalyser::defineMoreVars(bool BTagEff)
{
    if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
    

    //================================Store variables in tree=======================================//
    // define variables that you want to store
    //==============================================================================================//
    if (!BTagEff){
        addVartoStore("run");
        addVartoStore("genWeight");
        addVartoStore("luminosityBlock");
        addVartoStore("event");
        addVartoStore("evWeight");

        //electron
        addVartoStore("nElectron");
        addVartoStore("Electron_charge");
        addVartoStore("goodElectron");
        addVartoStore("NgoodElectron");
        addVartoStore("goodElectronOSSF");
        addVartoStore("goodElectron_eta");
        addVartoStore("goodElectron_phi");
        addVartoStore("goodElectron_mass");
        //muon
        addVartoStore("nMuon");
        addVartoStore("Muon_charge");
        addVartoStore("Muon_mass");
        addVartoStore("NgoodMuon");
        addVartoStore("goodMuonOSSF");
        addVartoStore("goodMuon_eta");
        addVartoStore("goodMuon_phi");
        addVartoStore("goodMuon_mass");

        //leptons
        addVartoStore("OSSFpair");
        addVartoStore("MaxPt");
        addVartoStore("invMassOSSF_pt");
        addVartoStore("deltaROSSF_pt");
        addVartoStore("Is2OSSFfromZ_20");
        addVartoStore("leadingLepton_pt");
        addVartoStore("sub_leadingLepton_pt");
        addVartoStore("trailing_lepton_pt");
        addVartoStore("all_leptons_M");
        addVartoStore("nAllLeptons");
        addVartoStore("all_leptons_eta");

        if(!_isData){
            addVartoStore("ngenDressed");
            //Fake True leptons
            addVartoStore("nTrueLeptons");
            addVartoStore("nFakeLeptons");
            //addVartoStore("GenPart_statusFlags");
        }

        //region
        addVartoStore("region_4l_ZZ");
        addVartoStore("region_WZ");
        addVartoStore("region_conversion");
        addVartoStore("region_ttz_3l");
        addVartoStore("region_ttz_4l");

        // Test cuts
        addVartoStore("ZZ_CR");

        //jet
        addVartoStore("nJet");
        addVartoStore("Jet_pt");
        addVartoStore("NtightJets");
        addVartoStore("tightJets_pt");
        addVartoStore("Clean_jetpt");
        addVartoStore("tightJets_eta");
        addVartoStore("tightJets_phi");
        addVartoStore("tightJets_mass");

        //Combine

        if(!_isData){
            std::string year2 = _year;
            if(_year =="2016preVFP" || _year == "2016postVFP")
            {
                year2 = "2016";
            }
            addVartoStore("evWeight_wobtagSF");
            addVartoStore("totbtagSF");
            addVartoStore("evWeight");
            addVartoStore("syst_elec_recoUp");
            addVartoStore("syst_elec_recoDown");
            addVartoStore("syst_elec_idUp");
            addVartoStore("syst_elec_idDown");
            addVartoStore("muon_hltUp");
            addVartoStore("muon_hltDown");
            addVartoStore("syst_muon_hltUp");
            addVartoStore("syst_muon_hltDown");
            addVartoStore("stat_muon_hlt_"+year2+"Up");
            addVartoStore("stat_muon_hlt_"+year2+"Down");
            addVartoStore("muon_recoUp");
            addVartoStore("muon_recoDown");
            addVartoStore("syst_muon_recoUp");
            addVartoStore("syst_muon_recoDown");
            addVartoStore("stat_muon_reco_"+year2+"Up");
            addVartoStore("stat_muon_reco_"+year2+"Down");
            addVartoStore("muon_idUp");
            addVartoStore("muon_idDown");
            addVartoStore("syst_muon_idUp");
            addVartoStore("syst_muon_idDown");
            addVartoStore("stat_muon_id_"+year2+"Up");
            addVartoStore("stat_muon_id_"+year2+"Down");
            addVartoStore("muon_isoUp");
            addVartoStore("muon_isoDown");
            addVartoStore("syst_muon_isoUp");
            addVartoStore("syst_muon_isoDown");
            addVartoStore("stat_muon_iso_"+year2+"Up");
            addVartoStore("stat_muon_iso_"+year2+"Down");
            addVartoStore("syst_puUp");
            addVartoStore("syst_puDown");
            addVartoStore("syst_b_correlatedUp");
            addVartoStore("syst_b_correlatedDown");
            addVartoStore("syst_b_uncorrelated_"+year2+"Up");
            addVartoStore("syst_b_uncorrelated_"+year2+"Down");
            addVartoStore("syst_l_correlatedUp");
            addVartoStore("syst_l_correlatedDown");
            addVartoStore("syst_l_uncorrelated_"+year2+"Up");
            addVartoStore("syst_l_uncorrelated_"+year2+"Down");
            addVartoStore("syst_prefiringUp");
            addVartoStore("syst_prefiringDown");
            addVartoStore("mescaleDown");
            addVartoStore("renscaleDown");
            addVartoStore("facscaleDown");
            addVartoStore("facscaleUp");
            addVartoStore("renscaleUp");
            addVartoStore("mescaleUp");

            addVartoStore("isrUp");
            addVartoStore("fsrUp");
            addVartoStore("isrDown");
            addVartoStore("fsrDown");

        }




        // //adding every other var use in the hist for the removing double counting part

        addVartoStore("region_ttz_3l_0e");
        addVartoStore("region_ttz_3l_1e");
        addVartoStore("region_ttz_3l_2e");
        addVartoStore("region_ttz_3l_3e");
        addVartoStore("region_ttz");
        addVartoStore("NcleanJets");
        addVartoStore("NcleanBJets");
        addVartoStore("one");
        addVartoStore("LeptonsNoZ_Pt_high");
        addVartoStore("Z_pt");

        // //add what is used for the cuts
        addVartoStore("Ossf3l");
        addVartoStore("HLT_IsoMu27"); addVartoStore("HLT_IsoMu30"); addVartoStore("HLT_Ele32_WPTight_Gsf"); addVartoStore("HLT_Ele35_WPTight_Gsf"); addVartoStore("HLT_Ele38_WPTight_Gsf"); addVartoStore("HLT_Ele40_WPTight_Gsf"); addVartoStore("HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8"); addVartoStore("HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8"); addVartoStore("HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ_Mass3p8"); addVartoStore("HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL"); addVartoStore("HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ"); addVartoStore("HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ"); addVartoStore("HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL"); addVartoStore("HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL"); addVartoStore("HLT_TripleMu_10_5_5_DZ"); addVartoStore("HLT_TripleMu_5_3_3_Mass3p8to60_DZ"); addVartoStore("HLT_TripleMu_12_10_5"); addVartoStore("HLT_DiMu9_Ele9_CaloIdL_TrackIdL"); addVartoStore("HLT_DiMu9_Ele9_CaloIdL_TrackIdL_DZ"); addVartoStore("HLT_Mu8_DiEle12_CaloIdL_TrackIdL"); addVartoStore("HLT_Mu8_DiEle12_CaloIdL_TrackIdL_DZ"); addVartoStore("HLT_Ele16_Ele12_Ele8_CaloIdL_TrackIdL"); addVartoStore("HLT_Mu50"); addVartoStore("HLT_Mu55"); addVartoStore("HLT_Ele115_CaloIdVT_GsfTrkIdT"); addVartoStore("HLT_Photon200");
        addVartoStore("region.*");
        addVartoStore("sig.*");
        addVartoStore("goodMuonMvaWp");
        addVartoStore("goodElMvaWp");
        addVartoStore("goodElMvaScore");
        addVartoStore("passMva");

        // Save everything that is needed for the histograms
        addVartoStore("Jet_pt");
        addVartoStore("Jet_eta");
        addVartoStore("Jet_area");
        addVartoStore("Jet_rawFactor");
        addVartoStore("fixedGridRhoFastjetAll");
        addVartoStore("Muon_charge");
        addVartoStore("Muon_pt");
        addVartoStore("Muon_eta");
        addVartoStore("Muon_phi");
        addVartoStore("Muon_genPartIdx");

        //addVartoStore("GenPart_pt");
        addVartoStore("Muon_nTrackerLayers");
        addVartoStore("MET_pt");
        addVartoStore("MET_phi");
        addVartoStore("Jet_phi");

        addVartoStore("SR_BDT_output_3l");
        addVartoStore("SR_BDT_output_4l");

        // MC
        if(!_isData){
            addVartoStore("Pileup_nTrueInt");
            addVartoStore("genPart_pt");
        }


        if (!_isData){
            addVartoStore("nTrueElectron");
            addVartoStore("nTrueMuon");
            addVartoStore("goodPromptMuonPt");
            addVartoStore("goodPromptElectronPt");
            addVartoStore("nPromptLeptons");
            addVartoStore("nNotPromptLeptons");
            addVartoStore("nConversionLeptons");
            addVartoStore("nNotConversionLeptons");
            addVartoStore("nPromptMva");
            addVartoStore("nNotPromptMva");
            addVartoStore("nMuPt10Prompt");
            addVartoStore("nElPt10Prompt");
            addVartoStore("nElLmvaPrompt");
            addVartoStore("nMuLmvaPrompt");
            addVartoStore("Electron_genPartFlav");
            addVartoStore("Muon_genPartFlav");
        }

        addVartoStore("goodMuonMvaScore");
        addVartoStore("isPromptgoodMuon");

        addVartoStore("MuPt10MvaScore");
        addVartoStore("isPromptMuPt10");

        addVartoStore("ElPt10MvaScore");
        addVartoStore("isPromptElPt10");

        addVartoStore("ElLmvaMvaScore");
        addVartoStore("isPromptElLmva");
        addVartoStore("IsTightElLmva");

        addVartoStore("MuLmvaMvaScore");
        addVartoStore("isPromptMuLmva");
        addVartoStore("IsTightMuLmva");

        addVartoStore("leadingLepton_eta");
        addVartoStore("subLeadingLepton_eta");
        addVartoStore("leadingLepton_energy");
        addVartoStore("subLeadingLepton_energy");

        addVartoStore("closestJetBTagMuon");
        addVartoStore("closestJetBTagEl");

        addVartoStore("minDrlb");
        addVartoStore("leadingJets_pt");
        addVartoStore("leadingJets_eta");
        addVartoStore("leadingJets_mass");
        addVartoStore("subLeadingJets_pt");
        addVartoStore("subLeadingJets_eta");
        addVartoStore("subLeadingJets_mass");
        addVartoStore("leadingCleanbjets_pt");
        addVartoStore("Clean_jetHT");

    }

    // For BDT
    addVartoStore("trailingLepton_eta");
    addVartoStore("maxdRll");
    addVartoStore("maxdRjj");
    addVartoStore("maxjeteta");
    addVartoStore("maxmjj");
    addVartoStore("maxptJetLep");
    addVartoStore("trailingJets_pt");
    addVartoStore("subleadingJets_eta");
    addVartoStore("trailingJets_eta");
    addVartoStore("leadingJets_pt");
    addVartoStore("subleadingJets_pt");
    addVartoStore("leadingJets_eta");



    if(BTagEff == 1 && !_isData)
    {
        addVartoStore("Selected_clean_bjet_btagpass_bcflav");
        addVartoStore("Selected_clean_bjet_btagpass_bcflav_pt");
        addVartoStore("Selected_clean_bjet_btagpass_bcflav_eta");
        addVartoStore("Selected_clean_jet_all_bcflav");
        addVartoStore("Selected_clean_jet_all_bcflav_pt");
        addVartoStore("Selected_clean_jet_all_bcflav_eta");
        addVartoStore("Selected_clean_bjet_btagpass_lflav");
        addVartoStore("Selected_clean_bjet_btagpass_lflav_pt");
        addVartoStore("Selected_clean_bjet_btagpass_lflav_eta");
        addVartoStore("Selected_clean_jet_all_lflav");
        addVartoStore("Selected_clean_jet_all_lflav_pt");
        addVartoStore("Selected_clean_jet_all_lflav_eta");
        addVartoStore("evWeight_wobtagSF");
    }

}

void BaseAnalyser::setupCuts_and_Hists(std::string year, std::string process, bool BTagEff, bool Counting, bool Combine, bool JEC, const std::vector<std::string>& Cuts, const std::vector<std::string>& Masses, std::string second_cut){
    for(auto _unc_name : _jercunctag)
    {
        string jec_unc_up_reg_idx = acut.idx.substr(0,2)+std::to_string(jes_unc_idx);
        string jec_unc_down_reg_idx = acut.idx.substr(0,2)+std::to_string(jes_unc_idx+1);
        string jet_pt_name;
        string jet_mass_name;
        string met_pt_name;
        string met_phi_name;

        if(acut.idx.find(jec_unc_up_reg_idx) == 0 && (acut.idx.length() == jec_unc_up_reg_idx.length() || (acut.idx.length() > jec_unc_up_reg_idx.length() && acut.idx[jec_unc_up_reg_idx.length()] == '_')))
        {
            jet_pt_name = "Jet_pt_"+_unc_name+"_up";
            jet_mass_name = "Jet_mass_"+_unc_name+"_up";
            *rnext = rnext->Redefine("Jet_pt_corr",jet_pt_name);
            *rnext = rnext->Redefine("Jet_mass_corr",jet_mass_name);

            met_pt_name = "MET_pt_corr_"+_unc_name+"_up";
            met_phi_name = "MET_phi_corr_"+_unc_name+"_up";
            *rnext = rnext->Redefine("MET_pt_corr",met_pt_name);
            *rnext = rnext->Redefine("MET_phi_corr",met_phi_name);
            *rnext = redefineVars(*rnext, year, process);
            *rnext = rnext->Redefine(cutname, acut.cutdefinition);
        }
        if(acut.idx.find(jec_unc_down_reg_idx) == 0 && (acut.idx.length() == jec_unc_down_reg_idx.length() || (acut.idx.length() > jec_unc_down_reg_idx.length() && acut.idx[jec_unc_down_reg_idx.length()] == '_')))
        {
            jet_pt_name = "Jet_pt_"+_unc_name+"_down";
            jet_mass_name = "Jet_mass_"+_unc_name+"_down";
            *rnext = rnext->Redefine("Jet_pt_corr",jet_pt_name);
            *rnext = rnext->Redefine("Jet_mass_corr",jet_mass_name);

            met_pt_name = "MET_pt_corr_"+_unc_name+"_down";
            met_phi_name = "MET_phi_corr_"+_unc_name+"_down";
            *rnext = rnext->Redefine("MET_pt_corr",met_pt_name);
            *rnext = rnext->Redefine("MET_phi_corr",met_phi_name);
            *rnext = redefineVars(*rnext, year, process);
            *rnext = rnext->Redefine(cutname, acut.cutdefinition);
        }

    }
}

ROOT::RDF::RNode BaseAnalyser::redefineVars(RNode _rlm, std::string year, std::string process)
{
    //Here, redefine all the varaible that depend on Jets' pt. This include evweight bc of btag weight.

}

void BaseAnalyser::bookHists(bool BTagEff, const std::vector<std::string>& weights)
{
    if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
    //add x and y axis title: add1DHist( {"hnevents", "hist_title; x_axis title; y_axis title", 2, -0.5, 1.5}, "one", "evWeight", "");
    if (!BTagEff){
        add1DHist( {"hRegion4lZZ", "ZZ-4l control region", 3, 0, 3}, "region_4l_ZZ", "evWeight", "00");
        add1DHist( {"hRegionWZ", "WZ control region", 4, 0, 4}, "region_WZ", "evWeight", "00");
        add1DHist( {"hRegionconversion", "Conversion control region", 4, 0, 4}, "region_conversion", "evWeight", "00");

        add1DHist( {"hRegion4lTTZ", "4l selection region", 2, 0, 2}, "region_ttz_4l", "evWeight", "00");
        add1DHist( {"hRegion3lTTZ", "3l selection region", 12, 0, 12}, "region_ttz_3l", "evWeight", "00");

        add1DHist( {"hRegion3lTTZ0e", "3l-0e selection region", 12, 0, 12}, "region_ttz_3l_0e", "evWeight", "00");
        add1DHist( {"hRegion3lTTZ1e", "3l-1e selection region", 12, 0, 12}, "region_ttz_3l_1e", "evWeight", "00");
        add1DHist( {"hRegion3lTTZ2e", "3l-2e selection region", 12, 0, 12}, "region_ttz_3l_2e", "evWeight", "00");
        add1DHist( {"hRegion3lTTZ3e", "3l-3e selection region", 12, 0, 12}, "region_ttz_3l_3e", "evWeight", "00");

        add1DHist( {"hRegionTTZ", "3l-4l TTZ selection region", 14, 0, 14}, "region_ttz", "evWeight", "00");


        add1DHist( {"hNnotcleanJets", "Jets", 7, 0, 7}, "NtightJets", "evWeight", "00");
        add1DHist( {"hNtightJets", "Jets", 7, 0, 7}, "NcleanJets", "evWeight", "00");
        add1DHist( {"hNgood_bjets", "BJets", 4, 0, 4}, "NcleanBJets", "evWeight", "00");
        add1DHist( {"hNgood_bjets_2", " BJets", 2, 0, 2}, "NcleanBJets", "evWeight", "00");
        add1DHist( {"hNtightJets_2", "Jets", 3, 0, 3}, "NcleanJets", "evWeight", "00");
        add1DHist( {"hTightJetsPt", "Jets", 40, 0, 200}, "Clean_jetpt", "evWeight", "00");



        if(!_isData){
            add1DHist( {"hSumOfWeights", "Sum of Weight", 1, 1, 2}, "one", "genWeight", "");
        }
        else{
            add1DHist( {"hSumOfWeights", "Sum of Weight", 1, 1, 2}, "one", "evWeight", "");
        }

        add1DHist( {"hLeadingLepton_pt", "Leading lepton p_T", 13, 40, 300}, "leadingLepton_pt", "evWeight", "00");
        add1DHist( {"hSub_leadingLepton_pt", "Sub-leading lepton p_T", 18, 20, 200}, "sub_leadingLepton_pt", "evWeight", "00");
        add1DHist( {"hTrailing_lepton_pt", "Trailing lepton p_T", 11, 10, 120}, "trailing_lepton_pt", "evWeight", "00");

        //Kinematic more

        add1DHist( {"hZ_pt", "Z Pt", 16, 0, 400}, "Z_pt", "evWeight", "00");
        add1DHist( {"hLeptonsNoZ_Pt_high", "non-Z higher Pt", 20, 0, 200}, "LeptonsNoZ_Pt_high", "evWeight", "00");
        add1DHist( {"hRegion_3L_SR", "nElectrons 3l", 4, 0, 4}, "region_3L_SR", "evWeight", "00");
        add1DHist( {"hRegion_4L_SR", "nElectrons 4l", 5, 0, 5}, "region_4L_SR", "evWeight", "00");

        add1DHist( {"hinvMassOSSF_pt", "Inv Mass high Pt", 100, 10, 300}, "invMassOSSF_pt", "evWeight", "00");

        //lepton related
        add1DHist( {"hngoodElectron", "Nelectrons", 5, 0, 5}, "NgoodElectron", "evWeight", "00");
        if(!_isData){
            add1DHist( {"hngenDressed", "GenDressed", 10, 0, 10}, "ngenDressed", "evWeight", "00");
            add1DHist( {"nTrueMuon", "nTrueMuon", 5, 0, 5}, "nTrueMuon", "evWeight", "00");
            add1DHist( {"nTrueElectron", "nTrueElectron", 5, 0, 5}, "nTrueElectron", "evWeight", "00");
        }

        add1DHist( {"hNgoodMuon", "NgoodMuon", 10, 0, 10}, "NgoodMuon", "evWeight", "00");

        // Add hist to find which one does not work
        add1DHist( {"hgoodElectron_eta", "goodElectron_eta", 20, -2.5, 2.5}, "goodElectron_eta", "evWeight", "00");
        add1DHist( {"hgoodMuon_eta", "goodMuon_eta", 20, -2.5, 2.5}, "goodMuon_eta", "evWeight", "00");
        add1DHist( {"hgoodMuon_phi", "goodMuon_phi", 20, -3.15, 3.15}, "goodMuon_phi", "evWeight", "00");
        add1DHist( {"hgoodElectron_phi", "goodElectron_phi", 20, -3.15, 3.15}, "goodElectron_phi", "evWeight", "00");

        add1DHist( {"htightJets_eta", "tightJets_eta", 20, -2.5, 2.5}, "tightJets_eta", "evWeight", "00");
        add1DHist( {"htightJets_phi", "tightJets_phi", 20, -3.15, 3.15}, "tightJets_phi", "evWeight", "00");
        add1DHist( {"htightJets_mass", "tightJets_mass", 20, 0, 40}, "tightJets_mass", "evWeight", "00");    

        add1DHist( {"hall_leptons_eta", "all_leptons_eta", 20, -2.5, 2.5}, "all_leptons_eta", "evWeight", "00");
        add1DHist( {"hgoodMuonMvaWp", "goodMuonMvaWp", 5, 0, 5}, "goodMuonMvaWp", "evWeight", "00");
        add1DHist( {"hgoodElMvaWp", "goodElMvaWp", 5, 0, 5}, "goodElMvaWp", "evWeight", "00");

        add1DHist( {"hZZ_CR", "ZZ_CR", 2, 0, 2}, "ZZ_CR", "evWeight", "00");

        add1DHist( {"goodElectron", "goodElectron", 2, 0, 2}, "goodElectron", "evWeight", "0");

        // LMVA efficiency
        if(!_isData){
            add1DHist( {"hnPromptMva", "nPromptMva", 5, 0, 5}, "nPromptMva", "evWeight", "00");
            add1DHist( {"hnNotPromptMva", "nNotPromptMva", 5, 0, 5}, "nNotPromptMva", "evWeight", "00");


            add1DHist( {"hnPromptLeptons", "nPromptLeptons", 5, 0, 5}, "nPromptLeptons", "evWeight", "00");
            add1DHist( {"hnNotPromptLeptons", "nNotPromptLeptons", 5, 0, 5}, "nNotPromptLeptons", "evWeight", "00");
            add1DHist( {"hnElPt10Prompt", "nElPt10Prompt", 5, 0, 5}, "nElPt10Prompt", "evWeight", "00");
            add1DHist( {"hnMuPt10Prompt", "nMuPt10Prompt", 5, 0, 5}, "nMuPt10Prompt", "evWeight", "00");
            
        }        
    }
    if(BTagEff == 1 && !_isData)
    {
        add2DHist_vbin( {"Eta_Pt_bJets_BtagPass_bcFlav","Eta * Pt of bJets passing Btag (bc flavour); Eta of the bjet (GeV); Pt of the bjet (GeV);", 3, 0., 2.5, 8, 0., 1000.}, "Selected_clean_bjet_btagpass_bcflav_eta","Selected_clean_bjet_btagpass_bcflav_pt","evWeight_wobtagSF","00");
        add2DHist_vbin( {"Eta_Pt_Jets_All_bcFlav","Eta * Pt of Jets (bc flavour); Eta of the jet (GeV); Pt of the jet (GeV);", 3, 0.0, 2.5, 8, 0., 1000.}, "Selected_clean_jet_all_bcflav_eta","Selected_clean_jet_all_bcflav_pt","evWeight_wobtagSF","00");
        add2DHist_vbin( {"Eta_Pt_bJets_BtagPass_lFlav","Eta * Pt of bJets passing Btag (light flavour); Eta of the bjet (GeV); Pt of the bjet (GeV);", 3, 0.0, 2.5, 8, 0., 1000.}, "Selected_clean_bjet_btagpass_lflav_eta","Selected_clean_bjet_btagpass_lflav_pt","evWeight_wobtagSF","00");
        add2DHist_vbin( {"Eta_Pt_Jets_All_lFlav","Eta * Pt of Jets (light flavour); Eta of the jet (GeV); Pt of the jet (GeV);", 3, 0.0, 2.5, 8, 0., 1000.}, "Selected_clean_jet_all_lflav_eta","Selected_clean_jet_all_lflav_pt","evWeight_wobtagSF","00");
    }

    // Combine

    add2DHist_vbin_manual( {"bdtvsmll_manual_3l", "bdtvsmll_manual_3l", 6, 0, 1, 10, 10, 200}, "invMassOSSF_pt", "SR_BDT_output_3l", "evWeight", "00");
    add2DHist( {"bdtvsmll_3l", "bdtvsmll_3l", 6, 0, 1, 3, 10, 200}, "invMassOSSF_pt", "SR_BDT_output_3l", "evWeight", "00");
    add2DHist_vbin_manual( {"bdtvsmll_manual_4l", "bdtvsmll_manual_4l", 6, 0, 1, 10, 10, 200}, "invMassOSSF_pt", "SR_BDT_output_4l", "evWeight", "00");
    add2DHist( {"bdtvsmll_4l", "bdtvsmll_4l", 6, 0, 1, 3, 10, 200}, "invMassOSSF_pt", "SR_BDT_output_4l", "evWeight", "00");

    add1DHist( {"hObservable_3l", "Observable_3l", 11, 0, 11}, "Observable_3l", "evWeight", "00");
    add1DHist( {"hObservable_3l", "Observable_4l", 2, 0, 2}, "Observable_4l", "evWeight", "00");
    add1DHist( {"hObservable_ZZ", "Observable_ZZ", 4, 0, 4}, "Observable_ZZ", "evWeight", "00");
    add1DHist( {"hObservable_WZ", "Observable_WZ", 4, 0, 4}, "Observable_WZ", "evWeight", "00");
    add1DHist( {"hObservable_nonprompt", "Observable_nonprompt", 9, 0, 9}, "Observable_nonprompt", "evWeight", "00");

    if(!_isData){
        add1DHist( {"hObservable_3l_evWeight", "Observable_3l", 12, 0, 12}, "Observable_3l", "evWeight", "00");
        add1DHist( {"hObservable_4l_evWeight", "Observable_4l", 2, 0, 2}, "Observable_4l", "evWeight", "00");
        add1DHist( {"hObservable_ZZ_evWeight", "Observable_ZZ", 4, 0, 4}, "Observable_ZZ", "evWeight", "00");
        add1DHist( {"hObservable_WZ_evWeight", "Observable_WZ", 4, 0, 4}, "Observable_WZ", "evWeight", "00");
        add1DHist( {"hObservable_nonprompt_evWeight", "Observable_nonprompt", 9, 0, 9}, "Observable_nonprompt", "evWeight", "00");
        
        for(const auto& weight : weights)
        {
            add1DHist( {Form("hObservable_3l_%s",weight.c_str()),"Observable_3l", 12, 0, 12}, "Observable_3l", weight,"00");
            add1DHist( {Form("hObservable_4l_%s",weight.c_str()),"Observable_4l", 2, 0, 2}, "Observable_4l", weight,"00");
            add1DHist( {Form("hObservable_ZZ_%s",weight.c_str()), "Observable_ZZ", 4, 0, 4}, "Observable_ZZ", weight, "00");
            add1DHist( {Form("hObservable_WZ_%s",weight.c_str()), "Observable_WZ", 4, 0, 4}, "Observable_WZ", weight, "00");
            add1DHist( {Form("hObservable_nonprompt_%s",weight.c_str()), "Observable_nonprompt", 9, 0, 9}, "Observable_nonprompt", weight, "00");
        }

    }
    else{
        add1DHist( {"data_obs_3l_evWeight", "Observable_3l", 12, 0, 12}, "Observable_3l", "evWeight", "00");
        add1DHist( {"data_obs_4l_evWeight", "Observable_4l", 2, 0, 2}, "Observable_4l", "evWeight", "00");
        add1DHist( {"data_obs_ZZ_evWeight", "Observable_ZZ", 4, 0, 4}, "Observable_ZZ", "evWeight", "00");
        add1DHist( {"data_obs_WZ_evWeight", "Observable_WZ", 4, 0, 4}, "Observable_WZ", "evWeight", "00");
        add1DHist( {"data_obs_nonprompt_evWeight", "Observable_nonprompt", 4, 0, 4}, "Observable_nonprompt", "evWeight", "00");
    }



}

void BaseAnalyser::calculateEvWeight(){

  //Scale Factors for BTag ID	
  int _case = 1;
  std::string year2 = _year;
    if(_year =="2016preVFP" || _year == "2016postVFP")
    {
        year2 = "2016";
    }
  std::vector<std::string> Jets_vars_names = {"Selected_clean_jet_hadronFlavour", "Clean_jeteta", "Clean_jetpt", "Selected_clean_jet_btagDeepFlavB"};  
  if(_case !=1){
    Jets_vars_names.emplace_back("Selected_jetbtag");
  }
  std::string output_btag_column_name = "btag_SF_";
  _rlm = calculateBTagSF(_rlm, Jets_vars_names, _case, 0.3040, "M", output_btag_column_name);
 
  //Scale Factors for Muon HLT, RECO, ID and ISO
  std::vector<std::string> Muon_vars_names = {"goodMuon_eta", "goodMuon_pt"};
  std::string output_mu_column_name = "muon_SF_";
  _rlm = calculateMuSF(_rlm, Muon_vars_names, output_mu_column_name);

  //Scale Factors for Electron RECO and ID
  std::vector<std::string> Electron_vars_names = {"goodElectron_eta", "goodElectron_pt"};
  std::string output_ele_column_name = "ele_SF_";
  _rlm = calculateEleSF(_rlm, Electron_vars_names, output_ele_column_name);

   //Prefiring Weight for 2016 and 2017
  _rlm = applyPrefiringWeight(_rlm);

  //_rlm = _rlm.Define("totbtagSF", "btag_SF_bcflav_central * btag_SF_lflav_central"); 
  //Total event Weight:


  //Total event Weight:
  //_rlm = _rlm.Define("evWeight", " pugenWeight * prefiring_SF_central");

      _rlm = _rlm.Define("Selected_electron_number", "NgoodElectron")
                 .Define("Selected_muon_number", "NgoodMuon");

      _rlm = _rlm.Define("evWeight_wobtagSF", "pugenWeight * prefiring_SF_central * muon_SF_central * ele_SF_central")
               .Define("totbtagSF", "btag_SF_bcflav_central * btag_SF_lflav_central")
               .Define("evWeight", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_elec_recoUp", "(Selected_electron_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_reco_sfup * ele_SF_id_sf:evWeight")
               .Define("syst_elec_recoDown", "(Selected_electron_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_reco_sfdown * ele_SF_id_sf:evWeight")
               .Define("syst_elec_idUp", "(Selected_electron_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_reco_sf * ele_SF_id_sfup:evWeight")
               .Define("syst_elec_idDown", "(Selected_electron_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_reco_sf * ele_SF_id_sfdown:evWeight")
               .Define("muon_hltUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_systup * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_hltDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_systdown * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_hltUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf + muon_SF_hlt_syst) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_hltDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf - muon_SF_hlt_syst) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("stat_muon_hlt_"+year2+"Up", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf + muon_SF_hlt_stat) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("stat_muon_hlt_"+year2+"Down", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * (muon_SF_hlt_sf - muon_SF_hlt_stat) * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_recoUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_systup * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_recoDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_systdown * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_recoUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf + muon_SF_reco_syst) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_recoDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf - muon_SF_reco_syst) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("stat_muon_reco_"+year2+"Up", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf + muon_SF_reco_stat) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("stat_muon_reco_"+year2+"Down", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * (muon_SF_reco_sf - muon_SF_reco_stat) * muon_SF_id_sf * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_idUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_systup * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_idDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_systdown * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_idUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf + muon_SF_id_syst) * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("syst_muon_idDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf - muon_SF_id_syst) * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("stat_muon_id_"+year2+"Up", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf + muon_SF_id_stat) * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("stat_muon_id_"+year2+"Down", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * (muon_SF_id_sf - muon_SF_id_stat) * muon_SF_iso_sf * ele_SF_central:evWeight")
               .Define("muon_isoUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_systup * ele_SF_central:evWeight")
               .Define("muon_isoDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * muon_SF_iso_systdown * ele_SF_central:evWeight")
               .Define("syst_muon_isoUp", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf + muon_SF_iso_syst) * ele_SF_central:evWeight")
               .Define("syst_muon_isoDown", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf - muon_SF_iso_syst) * ele_SF_central:evWeight")
               .Define("stat_muon_iso_"+year2+"Up", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf + muon_SF_iso_stat) * ele_SF_central:evWeight")
               .Define("stat_muon_iso_"+year2+"Down", "(Selected_muon_number != 0) ? pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_hlt_sf * muon_SF_reco_sf * muon_SF_id_sf * (muon_SF_iso_sf - muon_SF_iso_stat) * ele_SF_central:evWeight")
               .Define("syst_puUp", "genWeight * puWeight_plus * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_puDown", "genWeight * puWeight_minus * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_b_correlatedUp", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_up_correlated * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_b_correlatedDown", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_down_correlated * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_b_uncorrelated_"+year2+"Up", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_up_uncorrelated * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_b_uncorrelated_"+year2+"Down", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_down_uncorrelated * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_l_correlatedUp", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_up_correlated * muon_SF_central * ele_SF_central")
               .Define("syst_l_correlatedDown", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_down_correlated * muon_SF_central * ele_SF_central")
               .Define("syst_l_uncorrelated_"+year2+"Up", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_up_uncorrelated * muon_SF_central * ele_SF_central")
               .Define("syst_l_uncorrelated_"+year2+"Down", "pugenWeight * prefiring_SF_central * btag_SF_bcflav_central * btag_SF_lflav_down_uncorrelated * muon_SF_central * ele_SF_central")
               .Define("syst_prefiringUp", "pugenWeight * prefiring_SF_up * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central")
               .Define("syst_prefiringDown", "pugenWeight * prefiring_SF_down * btag_SF_bcflav_central * btag_SF_lflav_central * muon_SF_central * ele_SF_central");

        if(isDefined("LHEScaleWeight")){
            _rlm = addTheorySystematics(_rlm);

            _rlm = _rlm.Define("mescaleDown", "evWeight * LHEScaleWeight[0]")
                    .Define("renscaleDown", "evWeight * LHEScaleWeight[1]")
                    .Define("facscaleDown", "evWeight * LHEScaleWeight[3]")
                    .Define("facscaleUp", "evWeight * LHEScaleWeight[5]")
                    .Define("renscaleUp", "evWeight * LHEScaleWeight[7]")
                    .Define("mescaleUp", "evWeight * LHEScaleWeight[8]");
        }
        else{
            _rlm = _rlm.Define("mescaleDown", "evWeight")
                    .Define("renscaleDown", "evWeight")
                    .Define("facscaleDown", "evWeight")
                    .Define("facscaleUp", "evWeight")
                    .Define("renscaleUp", "evWeight")
                    .Define("mescaleUp", "evWeight");
        }

        _rlm = _rlm.Define("isrUp", "evWeight * PSWeight[0]")
               .Define("fsrUp", "evWeight * PSWeight[1]")
               .Define("isrDown", "evWeight * PSWeight[2]")
               .Define("fsrDown", "evWeight * PSWeight[3]");




}
//MET

void BaseAnalyser::setTree(TTree *t, std::string outfilename, bool BTagEff, const std::vector<std::string>& weights)
{
	if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }

	_rd = ROOT::RDataFrame(*t);
	_rlm = RNode(_rd);
	_outfilename = outfilename;
	_hist1dinfovector.clear();
	_th1dhistos.clear();
	_varstostore.clear();
	_hist1dinfovector.clear();
	_selections.clear();
    bool stepone=1;
    std::cout<<"it's used"<<std::endl;
	this->setupAnalysis(BTagEff, stepone, weights);
}

//================================Clean Object Definitions====================================//

void BaseAnalyser::setupObjects(bool BTagEff)
{
    // Object selection will be defined in sequence.
	// Clean objects will be stored in new vectors.
    _rlm = _rlm.Define("one", "1.0")
               .Define("zero", "0")
               .Define("True", "true")
               .Define("False", "false");
	selectElectrons();
	selectMuons();
    selectLeptons();
	selectJets(BTagEff);
    if(!_isData){
	  this->calculateEvWeight(); // PU, genweight and BTV and Mu and Ele
	}
    evaluate_and_store_mva("results/17/2024-06-26/ready/BDT_model_3l.root", "3l");
    evaluate_and_store_mva("results/17/2024-06-26/ready/BDT_model_4l.root", "4l");
    applyMva();
    defineRegion();
    defineObservable();

}//Beware that i change that for test purpose. DO NOT FORGET

void BaseAnalyser::evaluate_and_store_mva(const std::string &SR_BDT_fname, const std::string &region){
  //RBDT<> bdt("myBDT", "/gridgroup/cms/efillaudeau/CMSSW_12_3_0/src/fly_Arnab/results/18/2024-04-16/muChannel_BDT_nbTasks1/BDT_model.root");
  //_rlm = _rlm.Define("BDT_output",  Compute<8, float>(bdt)[0], {"sphericity", "specJet_eta", "Wboson_transversMass", "top_mass", "deltaEta_muon_b_jet", "deltaR_light_b_jets", "TightBJet_leading_pt", "lep_pt"});
_rlm = _rlm.Define("SR_BDT_output_"+region, [this, SR_BDT_fname](const int &nJets, const int &nBjets, const float &leadingPt, const float &subleadingPt, const float &trailingPt, const float &metPt, const float &mll, const float& dr) {
float bdtscore = -999;
//std::vector<float> inputVec = {s, std::abs(ase), wtm, tm, dembj, drlbj, tbjpt, lpt};
std::vector<float>  inputVec = {
    nJets,
    nBjets,
    leadingPt,
    subleadingPt,
    trailingPt,
    metPt,
    static_cast<float>(mll),
    static_cast<float>(dr)
};


//std::cout << "Input Vector for BDT Compute: ";
for (const auto& val : inputVec) {
    if (std::isnan(val) || std::isinf(val) || std::abs(val) > 9999) {
    std::cerr << "\nInvalid input detected (NaN, Inf, or >999): " << val << std::endl;
    return bdtscore; // Return an error code or handle this case appropriately
    }
    //std::cout << val << " ";
}
//std::cout << std::endl;
//static  RBDT<> bdt("myBDT", "data/BDT_Models/SMSingleTop_vs_Rest_BDT_model.root");
static  RBDT<> bdt("myBDT", SR_BDT_fname);

auto result = bdt.Compute(inputVec)[0];
//auto result = bdt.Compute({1,2,2,3,4,5,1,3})[0];

//std::cout << "The BDT Output Score is " << result << "!!!!!!!!!!!!!!" << std::endl;
bdtscore = result;
return bdtscore;

//return bdt.Compute({s, std::abs(ase), wtm, tm, dembj, drlbj, tbjpt, lpt})[0];
    }, {"NcleanJets", "NcleanBJets", "leadingLepton_pt", "sub_leadingLepton_pt", "trailing_lepton_pt", "MET_pt", "invMassOSSF_pt", "deltaROSSF_pt"});
}

void BaseAnalyser::setupAnalysis(bool BTagEff, bool stepone, const std::vector<std::string>& weights)
{
	if (debug){
        std::cout<< "================================//=================================" << std::endl;
        std::cout<< "Line : "<< __LINE__ << " Function : " << __FUNCTION__ << std::endl;
        std::cout<< "================================//=================================" << std::endl;
    }
	
 	cout<<"year===="<< _year<< "==runtype=== " <<  _runtype <<endl;

    //==========================================event/gen/ weights==========================================//
    // Event weight for data it's always one. For MC, it depends on the sign
    //=====================================================================================================//

	
	if (_isData && !isDefined("evWeight"))
	{
		_rlm = _rlm.Define("evWeight", [](){
				return 1.0;
			}, {} );
	}
	if(!_isData && !isDefined("evWeight")) // Only use genWeight
	  {
	    //_rlm = _rlm.Define("evWeight", "genWeight");
	    
	    //std::cout<<"Using evWeight = genWeight"<<std::endl;

	    auto sumgenweight = _rd.Sum("genWeight");
	    string sumofgenweight = Form("%f",*sumgenweight);
	    _rlm = _rlm.Define("genEventSumw",sumofgenweight.c_str());
	    std::cout<<"Sum of genWeights = "<<sumofgenweight.c_str()<<std::endl;
	  }
	
	defineCuts(stepone);
	defineMoreVars(BTagEff);
	bookHists(BTagEff, weights);
	setupCuts_and_Hists();
	setupTree();

}
