/*
 * utility.cpp
 *
 *  Created on: Dec 4, 2018
 *      Author: suyong
 */
#include "utility.h"
#include "TMatrixDSym.h"
#include "TVectorT.h"
#include "Math/SpecFuncMathMore.h"
#include "correction.h"
#include "Math/GenVector/VectorUtil.h"
#include "Math/GenVector/Rotation3D.h"
#include "Math/Math.h"
#include<cmath>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <sstream>
// For lmva
#include <xgboost/c_api.h>
#include<map>
// For me only. Maybe i'll put it somewhere else someday. This need some change in the compilation i guess
#include <Python.h>



// Utility function to generate fourvector objects for thigs that pass selections

using namespace std;

FourVectorVec generate_4vec(floats &pt, floats &eta, floats &phi, floats &mass)
{
	const int nsize = pt.size();
	FourVectorVec fourvecs;
	fourvecs.reserve(nsize);
	for (auto i=0; i<nsize; i++)
	{
		fourvecs.emplace_back(pt[i], eta[i], phi[i], fabs(mass[i]));
	}

	return fourvecs;
}

floats weightv(floats &x, float evWeight)
{
	const int nsize = x.size();
	floats weightvector(nsize, evWeight);
	return weightvector;
}

floats sphericity(FourVectorVec &p)
{
	TMatrixDSym NormMomTensor(3);

	NormMomTensor = 0.0;
	double p2sum = 0.0;
	for (auto x: p)
	{
		p2sum += x.P2();
		double mom[3] = {x.Px(), x.Py(), x.Pz()};
		for (int irow=0; irow<3; irow++)
		{
			for (int icol=irow; icol<3; icol++)
			{
				NormMomTensor(irow, icol) += mom[irow] * mom[icol];
			}
		}
	}
	NormMomTensor *= (1.0/p2sum);
	TVectorT<double> Qrev;
	NormMomTensor.EigenVectors(Qrev);
	floats Q(3);
	for (auto i=0; i<3; i++) Q[i] = Qrev[2-i];

	return Q;
}


double foxwolframmoment(int l, FourVectorVec &p, int minj, int maxj)
{   // PRD 87, 073014 (2013)
	double answer = 0.0;

	double ptsum=0.0;

	if (maxj==-1) // process everything
	{
		maxj = p.size();
	}
	//for (auto x: p)
	for (auto i=minj; i<maxj; i++)
	{
		auto x = p[i];
		ptsum += x.Pt();
		//for (auto y: p)
		for (auto j=minj; j<maxj; j++)
		{
			auto y = p[j];
			double wij = x.Pt() * y.Pt();
			double cosdOmega = x.Vect().Dot(y.Vect()) / (x.P() * y.P());
			if (cosdOmega>1.0) cosdOmega=1.0;
			if (cosdOmega<-1.0) cosdOmega=-1.0;
			answer += wij * ROOT::Math::legendre(l, cosdOmega);
		}
	}
	answer /= ptsum*ptsum;
	if (fabs(answer)>1.0) std::cout << "FW>1 " << answer << std::endl;
	return answer;
}


// case 1: Evaluate central SFs 
//fixedWP correction with mujets (here medium WP)
// evaluate('systematic', 'working_point', 'flavor', 'abseta', 'pt')
floats btv_case1(std::unique_ptr<correction::CorrectionSet>& cset, std::string type, std::string sys, std::string wp, ints& hadflav, floats& etas, floats& pts)
{
  floats scalefactors_case1;
  const auto nvecs = pts.size();
  //cout << "NVECS======== " << nvecs << endl;
  scalefactors_case1.reserve(nvecs);
  const auto abs_etas = [etas]() {
    floats res;
    res.reserve(etas.size());
    std::transform(etas.begin(), etas.end(), std::back_inserter(res), [](const auto& e) { return std::fabs(e); });
    return res;
  }();
  const auto cast_pts = [pts]() {
    floats res;
    res.reserve(pts.size());
    std::transform(pts.begin(), pts.end(), std::back_inserter(res), [](const auto& p) { return static_cast<float>(p); });
    return res;
  }();
  float bc_jets = 1.0;
  
  for (auto i = 0; i < int(nvecs); i++) {
    //std::cout << "sys: " << sys << ", wp: " << wp << ", hadflav: " << hadflav[i] << ", etas: " << abs_etas[i] << ", pts: " << cast_pts[i] << '\n';
    if(abs_etas[i]<=2.4){
      if (hadflav[i] != 0) {
	bc_jets *= cset->at("deepJet_mujets")->evaluate({sys, wp, hadflav[i], abs_etas[i], cast_pts[i]});
	//scalefactors_case1.emplace_back(bc_jets);
	//std::cout << "\njet SFs from deepJe_mujets at medium WP\n";
	//std::cout << "SF b/c jets : " << bc_jets << '\n';
      } else{ 
	//const auto bc_jets = cset->at("deepJet_incl")->evaluate({sys, wp, hadflav[i], abs_etas[i], cast_pts[i]});
	bc_jets *= cset->at("deepJet_incl")->evaluate({sys, wp, hadflav[i], abs_etas[i], cast_pts[i]});
      }
    }
    scalefactors_case1.emplace_back(bc_jets);
    //std::cout << "\njet SFs from deepJet_incl at medium WP\n";
    //std::cout << "SF light jets : " << bc_jets << '\n';
    //}
    //
    
  }

  return scalefactors_case1;
}

// case 2: Evaluate varied SFs 
//fixedWP correction uncertainty (here tight WP and comb SF)
// evaluate('systematic', 'working_point', 'flavor', 'abseta', 'pt')
floats btv_case2(std::unique_ptr<correction::CorrectionSet>& cset, std::string type, std::string sys, std::string wp, ints& hadflav, floats& etas, floats& pts)
{
  floats scalefactors_case2;
  const auto nvecs = pts.size();
  //cout << "NVECS======== " << nvecs << endl;
  scalefactors_case2.reserve(nvecs);
  const auto abs_etas = [etas]() {
    floats res;
    res.reserve(etas.size());
    std::transform(etas.begin(), etas.end(), std::back_inserter(res), [](const auto& e) { return std::fabs(e); });
    return res;
  }();
  const auto cast_pts = [pts]() {
    floats res;
    res.reserve(pts.size());
    std::transform(pts.begin(), pts.end(), std::back_inserter(res), [](const auto& p) { return static_cast<float>(p); });
    return res;
  }();
  for (auto i = 0; i < int(nvecs); i++) {

    float bweight = 1.0;
     
    if (hadflav[i] != 0) {
      //std::string type = "deepJet_comb" ;
      const auto bc_jets = cset->at("deepJet_comb")->evaluate({sys, wp, hadflav[i], abs_etas[i], cast_pts[i]});
      bweight = bc_jets;
      //std::cout << "\njet SFs up_correlated for comb at tight WP\n";
      //std::cout << "SF b/c : " << bc_jets << '\n';
    } else{ 
      //std::string type = "depJet_incl" ;
      const auto bc_jets = cset->at("deepJet_incl")->evaluate({sys, wp, hadflav[i], abs_etas[i], cast_pts[i]});
      bweight = bc_jets;
      
      //std::cout << "\njet up_correlated for comb at tight  WP\n";
      //std::cout << "SF light jets : " << bc_jets << '\n';
    }
    scalefactors_case2.emplace_back(bweight);
  }

  return scalefactors_case2;

}


floats btvcorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string type, std::string sys, ints &hadflav,floats &etas,floats &pts,   floats &btags)
{
  floats scalefactors;
  auto nvecs = pts.size();
  scalefactors.reserve(nvecs);
  for (auto i=0; i<int(nvecs); i++)
    {
      //std::cout << "sys: " << sys << ", hadflav: " << hadflav[i] << ", etas: " << fabs(float(etas[i])) << ", pts: " << float(pts[i]) <<" btag discriminator : " << float(btags[i]) << '\n';
      //for 2018UL working points l: 0.0494, m: 0.2770, t: 0.7264
      //if (btags[i]>0.7264){
      float sfi = cset->at(type)->evaluate({sys, int(hadflav[i]), fabs(float(etas[i])), float(pts[i]), float(btags[i])});
      scalefactors.emplace_back(sfi);
      cout<<" jets central scale factors == "<< sfi << endl;
      //}
    }
  return scalefactors;
}


float pucorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string name, std::string syst, float ntruepileup)
{
	return float(cset->at(name)->evaluate({ntruepileup, syst.c_str()}));
}

ints good_idx(ints good)
{
	ints out;
	for(int i = 0; i < int(good.size()); i++){
		if( good[i] ){
			out.emplace_back(i);
		}
	}
	return out;

}

float calculate_deltaEta( FourVector &p1, FourVector &p2){
	return p1.Eta() - p2.Eta();
}
float calculate_deltaPhi( FourVector &p1, FourVector &p2){
	return ROOT::Math::VectorUtil::DeltaPhi(p1, p2);
}

float calculate_deltaPhi_scalars(double &phi1, double &phi2){
	return ROOT::VecOps::DeltaPhi(phi1, phi2);
}

float calculate_deltaR( FourVector &p1, FourVector &p2){
	return ROOT::Math::VectorUtil::DeltaR(p1, p2);
}
float calculate_deltaR_syst( FourVectorVec &vec){
	float dr = ROOT::Math::VectorUtil::DeltaR(vec[0], vec[1]);
	if(abs(dr) > 10) cout<<vec[0]<<" / "<<vec[1]<<endl; 
	return ROOT::Math::VectorUtil::DeltaR(vec[0], vec[1]);
}
float calculate_invMass( FourVector &p1, FourVector &p2){
	return ROOT::Math::VectorUtil::InvariantMass(p1, p2);
}
FourVector sum_4vec( FourVector &p1, FourVector &p2){
	return p1+p2;
}
//Get indices that sort the object vectors in descending order
floats sort_discriminant( floats discr, floats obj ){
	auto sorted_discr = Reverse(Argsort(discr));
	floats out;
	for (auto idx : sorted_discr){
		out.emplace_back(obj[idx]);
	}
	return out;
}
FourVector select_leadingvec( FourVectorVec &v ){
	FourVector vout;
	if(v.size() > 0) return v[0];
	else return vout;
}

void PrintVector(floats myvector){
	for (size_t i = 0; i < myvector.size(); i++){
		std::cout<<myvector[i]<<"\n";
	}

}


//======================Muons SF===============================================================================//
//muonID_SF
floats muoncorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string type, std::string year, std::string runtype,floats &etas, floats &pts, std::string sys)
{
  string Data_Type = year + "_" + runtype;
    floats sf_muon;
    auto nvecs = pts.size();
    //cout<<"NVECS====== == "<< nvecs << endl;
    sf_muon.reserve(nvecs);
    
    if (etas.size() != nvecs) {
        throw std::invalid_argument("etas and pts vectors must have the same size!");
    }
    
    for (auto i=0; i<int(nvecs); i++)
    {
        //std::cout << "year: " << year  << ", etas: " << fabs(float(etas[i])) << ", pts: " << float(pts[i]) << " sys : " << sys << '\n';
        
        if (pts[i] < 0 ) {
            throw std::invalid_argument("Invalid value of pT detected!");
        }
        
        if (fabs(float(etas[i])) > 2.5) {
            throw std::invalid_argument("Invalid value of eta detected!");
        }
		//for muons pt<15 
		/*if (pts[i] < 15 || fabs(float(etas[i])) > 2.4) {
        sf_muon.emplace_back(-1);
        continue;
    	}*/
	
        float sfm = cset->at(type)->evaluate({Data_Type, fabs(float(etas[i])), float(pts[i]), sys});
        sf_muon.emplace_back(sfm);

    }
    return sf_muon;
}


//======================Function to check the number of objects with pT greather than x========================//
ints pTcounter(floats vec)
{
	ints output;
	int counter = 0;
	for(auto pt: vec)
	{
		if (pt>10.0) counter++;
	}
	output.emplace_back(counter);
	return output;
}




//=====================build pair example=============================================//
//W reconstruction : 2 jets
//====================================================================================//
floats w_reconstruction (FourVectorVec &jets){
floats out;
float dijetMass;
float dijetMass_out;
float dijetDR;
const float Mass_W = 80.9; //W mass 
const float Width_W = 10.8; //W width
float X_Min = 99999;
float X_Recwidth_W; 

//Loop on all selected jets
	for(unsigned int j1 = 0; j1<jets.size()-1; j1++){
		for(unsigned int j2 = j1+1; j2<jets.size(); j2++){
			 //select 2 jets
			dijetMass = (jets[j1]+jets[j2]).M();
			  //select a best W candidate in min width in a event
			X_Recwidth_W = std::pow((Mass_W-dijetMass)/Width_W,2);
			if (X_Recwidth_W<X_Min){
				dijetDR = ROOT::Math::VectorUtil::DeltaR(jets[j1],jets[j2]);
				X_Min = X_Recwidth_W;
				dijetMass_out = dijetMass;
			}

		}
	}
	out.push_back(dijetMass_out);   //0:w_mass
	out.push_back(dijetDR);  
	return out;
}


floats compute_DR (FourVectorVec &muons, ints goodMuons_charge){
	floats out;
	float mu_ss_DR;
	float mu_os_DR;
	//std::cout<<"Muonsize: " << muons.size()<<std::endl;
	if(muons.size()>0)
	//Loop on all selected muons
	for(unsigned int mu1 = 0; mu1<muons.size()-1; mu1++){
		for(unsigned int mu2 = mu1+1; mu2<muons.size(); mu2++){
			 //select 2 muons with same sign
			 if (goodMuons_charge[mu1]!=goodMuons_charge[mu2]) continue; //check charge of muons
			mu_ss_DR = ROOT::Math::VectorUtil::DeltaR(muons[mu1],muons[mu2]);
			 //select 2 muons with same sign
			if (goodMuons_charge[mu1]==goodMuons_charge[mu2]) continue;
			mu_os_DR = ROOT::Math::VectorUtil::DeltaR(muons[mu1],muons[mu2]);

		}
	}
	out.push_back(mu_ss_DR);		//0: same sign dimuon DR
	out.push_back(mu_os_DR);        //1: opposite sign dimuon dR
	return out;

}



FourVectorVec addVecFourVec(FourVector &A, FourVector &B, FourVector &C) // 
{
	FourVectorVec all;
	all.push_back(A);
	all.push_back(B);
	all.push_back(C);
	return all;
}



FourVector generate_single_4vec(double &pt, double &eta, double &phi, double &mass)
{
	FourVector fourvecs;

	fourvecs.SetPt(pt);
	fourvecs.SetEta(eta);
	fourvecs.SetPhi(phi);
	fourvecs.SetM(fabs(mass));

	return fourvecs;
}


doubles calculateDeltaR_group(FourVectorVec &jets, FourVector &lepton)
{
	std::vector<double> deltaR(jets.size());
	for(auto ajet: jets)
	{
		deltaR.emplace_back(calculate_deltaR(ajet,lepton));
	}

	return deltaR;
}


TLorentzVector generate_TLorentzVector(double &pt, double &eta, double &phi, double &mass) 
{
   TLorentzVector TLorentzVec;
   TLorentzVec.SetPtEtaPhiM(pt, eta, phi, mass);
   return TLorentzVec;
};


/* -------------- W boson Reconstruction for SingleTop t-channel ------------------------*/


float calculateLambda(TLorentzVector &lepton, float met_pt, float met_phi)
{
   float met_px = met_pt*TMath::Cos(met_phi);
   float met_py = met_pt*TMath::Sin(met_phi);
   float mW = 80.4;

   float lambda = (pow(mW,2))/2 + met_px*lepton.Px() + met_py*lepton.Py();
   return lambda;

}

float calculateDelta(TLorentzVector &lepton, float met_pt, float lambda)
{
    // float delta = 4*lambda*lambda*lepton.Pz()*lepton.Pz() - 4*lepton.Pt()*lepton.Pt()*(lepton.E()*lepton.E()*met_pt*met_pt - lambda*lambda);

	float delta1 = pow(lambda*lepton.Pz(),2);
	float delta2 = pow(lepton.Pt(),2)*(pow(lepton.E(),2)*pow(met_pt,2)-pow(lambda,2));

	float delta = 4*(delta1-delta2);

    return delta;
}


float calculate_nu_z(TLorentzVector &lepton, float lambda, float delta, float met_pt, float met_phi)
{
  float p_nu_z = 0.0;
  if(delta>=0)
    {
      float p_nu_z_plus = calculate_nu_z_plus(lepton, lambda, delta);
      float p_nu_z_minus = calculate_nu_z_minus(lepton, lambda, delta);
      
      p_nu_z = p_nu_z_plus < p_nu_z_minus ? p_nu_z_plus : p_nu_z_minus;
      
    }
  
  if(delta<0)
    {
      p_nu_z = calculate_nu_z_complex(lepton, met_pt, met_phi);
      
    }
  return p_nu_z;
}



float calculate_nu_z_plus(TLorentzVector &lepton, float lambda, float delta)
{
   double p_nu_z_plus = (lambda*lepton.Pz())/(pow(lepton.Pt(),2)) + (sqrt(delta))/(2*pow(lepton.Pt(),2));
   return p_nu_z_plus;
}

float calculate_nu_z_minus(TLorentzVector &lepton, float lambda, float delta)
{
   double p_nu_z_minus = (lambda*lepton.Pz())/(pow(lepton.Pt(),2)) - (sqrt(delta))/(2*pow(lepton.Pt(),2));
   return p_nu_z_minus;
}



float calculate_nu_z_complex(TLorentzVector &lepton, float met_pt, float met_phi)
{
  //float met_px; 
  //float met_py;
  float mW = 80.4;
  
  float reco_nu_pt_up = sqrt(2)*abs(mW + lepton.Pt()/sqrt(2));
  float reco_nu_pt_down = sqrt(2)*abs(mW - lepton.Pt()/sqrt(2));
  
  float reco_nu_pt;
  float reco_nu_phi;
  
  if (reco_nu_pt_down < 0)
    {
      reco_nu_pt = reco_nu_pt_up;
    }
  else if (abs(met_pt - reco_nu_pt_down) < abs(met_pt - reco_nu_pt_up))
    {
      reco_nu_pt = reco_nu_pt_down;
    }
  else if (abs(met_pt - reco_nu_pt_down) > abs(met_pt - reco_nu_pt_up))
    {
      reco_nu_pt = reco_nu_pt_up;
    }
  
  float cosThetaLepNu = (mW*mW - reco_nu_pt*reco_nu_pt - lepton.Pt()*lepton.Pt())/(2*lepton.Pt()*reco_nu_pt);
  
  
  if (abs(cosThetaLepNu)>1 && abs(met_pt - reco_nu_pt_down) < abs(met_pt - reco_nu_pt_up) && reco_nu_pt_up > 0 )
    {
      reco_nu_pt = reco_nu_pt_up;
    }
  if (abs(cosThetaLepNu)>1 && abs(met_pt - reco_nu_pt_down) > abs(met_pt - reco_nu_pt_up) && reco_nu_pt_down > 0)
    {
      reco_nu_pt = reco_nu_pt_down;
    }
  
  cosThetaLepNu = (mW*mW - reco_nu_pt*reco_nu_pt - lepton.Pt()*lepton.Pt())/(2*lepton.Pt()*reco_nu_pt);
  
  if(cosThetaLepNu>=0) reco_nu_phi = lepton.Phi() + acos(cosThetaLepNu);
  if(cosThetaLepNu<0) reco_nu_phi = lepton.Phi() + (2*M_PI - acos(cosThetaLepNu));
  if (isnan(reco_nu_phi)) return -999.9;
  
  //met_px = reco_nu_pt*TMath::Cos(reco_nu_phi); 
  //met_py = reco_nu_pt*TMath::Sin(reco_nu_phi);
  
  float lambda = mW*mW/2 + reco_nu_pt*lepton.Pt()*cosThetaLepNu;
  
  float met_pz = lambda*lepton.Pz()/pow(lepton.Pt(),2);
  
  return met_pz;
}


float calculate_nu_energy(float met_pt, float met_phi, float met_pz)
{
   float met_px = met_pt*TMath::Cos(met_phi); 
   float met_py = met_pt*TMath::Sin(met_phi);

   float nu_energy = TMath::Sqrt(met_px*met_px+met_py*met_py+met_pz*met_pz);

   return nu_energy;
}

TLorentzVector get_neutrino_TL4vec(float met_pt, float met_phi, float met_pz, float met_energy)
{
    float met_px = met_pt*TMath::Cos(met_phi); 
    float met_py = met_pt*TMath::Sin(met_phi);

	TLorentzVector neutrino_TL4vec;

	neutrino_TL4vec.SetPxPyPzE(met_px, met_py, met_pz, met_energy);

	return neutrino_TL4vec;
}


TLorentzVector reconstructWboson_TL4vec(TLorentzVector &lepton, TLorentzVector &neutrino)
{
	TLorentzVector Wboson_TL4vec = lepton + neutrino;
	return Wboson_TL4vec;
}

float MaxPt(floats &pt){
	float ptMax=0;
	for(int i=0; i<pt.size(); i++){
		if(pt[i]>ptMax) ptMax = pt[i];
	}
	return ptMax;
}

FourVectorVec HighPtLeptonsOSSF (ints E_charge, floats E_pt, ints M_charge, floats M_pt, FourVectorVec M_4vec, FourVectorVec E_4vec){
	//return 4vec of the 2 ossf
	//this suppose that there is at least 1

	//!!!That is scalar high pt, maybe try with vectorial
	FourVectorVec ossfCouple;
	if(E_charge.size()+M_charge.size()==3){

		if(E_charge.size()==2){
			ossfCouple.push_back(E_4vec[0]);
			ossfCouple.push_back(E_4vec[1]);
			return ossfCouple;
		}
		if(M_charge.size()==2){
			ossfCouple.push_back(M_4vec[0]);
			ossfCouple.push_back(M_4vec[1]);
			return ossfCouple;
		}

	}

	float EptP, EptM=-1;
	int EposP, EposM=-1;
	float MptP, MptM=-1;
	int MposP, MposM=-1;

	for (unsigned int E=0; E<E_pt.size(); E++){
		if (E_charge[E]>0 && E_pt[E]>EptP){
			EptP=E_pt[E]; EposP=E;
		}
		if (E_charge[E]<0 && E_pt[E]>EptM){
			EptM=E_pt[E]; EposM=E;
		}
	}
	for (unsigned int M=0; M<M_pt.size(); M++){
		if (M_charge[M]>0 && M_pt[M]>MptP){
			MptP=M_pt[M]; MposP=M;
		}
		if (M_charge[M]<0 && M_pt[M]>MptM){
			MptM=M_pt[M]; MposM=M;
		}
	}
	if (EptM==-1 || EptP==-1){
		//no E couple means at least 1 M couple
		ossfCouple.push_back(M_4vec[MposP]);
		ossfCouple.push_back(M_4vec[MposM]);
		return ossfCouple;
	}
	if (MptM==-1 || MptP==-1){
		ossfCouple.push_back(E_4vec[EposP]);
		ossfCouple.push_back(E_4vec[EposM]);
		return ossfCouple;
	}
//if 2 couples E and M
	if (EptP+EptM > MptP + MptM){
		ossfCouple.push_back(E_4vec[EposP]);
		ossfCouple.push_back(E_4vec[EposM]);
		return ossfCouple;
	}
	ossfCouple.push_back(M_4vec[MposP]);
	ossfCouple.push_back(M_4vec[MposM]);
	return ossfCouple;
}

FourVectorVec VecPtLeptonsOSSF (ints E_charge, ints M_charge, FourVectorVec M_4vec, FourVectorVec E_4vec, bool high, bool solo){
	// Warning : If the OSSF couple exist, then it will return it. If not, it would be a SSSF couple. Be careful when using this function
	FourVectorVec ossfCouple;

	if(E_charge.size()+M_charge.size()==3){

		if(E_charge.size()==2){
			ossfCouple.push_back(E_4vec[0]);
			ossfCouple.push_back(E_4vec[1]);
			return ossfCouple;
		}
		if(M_charge.size()==2){
			ossfCouple.push_back(M_4vec[0]);
			ossfCouple.push_back(M_4vec[1]);
			return ossfCouple;
		}

	}
	float pt=0;
	vector <int> posM={-1,-1};
	vector <int> posE={-1,-1};
	//Muon selection
	if (M_charge.size()>1){
		for(int i=0; i<M_charge.size()-1; i++){
			for(int j=i+1; j<M_charge.size(); j++){
				if(M_charge[i]!=M_charge[j]){
					if (high){						
						if(pt<(M_4vec[i]+M_4vec[j]).Pt()){
							pt=(M_4vec[i]+M_4vec[j]).Pt();
							posM[0]=i; posM[1]=j;
						}
					}
					else{
						if(pt>(M_4vec[i]+M_4vec[j]).Pt() || pt==0){
							pt=(M_4vec[i]+M_4vec[j]).Pt();
							posM[0]=i; posM[1]=j;
						}
					}
				}
			}
		}
	}
	//Electron selection
	if (E_charge.size()>1){
		for(int i=0; i<E_charge.size()-1; i++){
			for(int j=i+1; j<E_charge.size();j++){
				if(E_charge[i]!=E_charge[j]){
					if (high){
						if(pt<(E_4vec[i]+E_4vec[j]).Pt()){
							pt=(E_4vec[i]+E_4vec[j]).Pt();
							posE[0]=i; posE[1]=j;
						};
					}
					else{
						if(pt>(E_4vec[i]+E_4vec[j]).Pt() || pt==0){
							pt=(E_4vec[i]+E_4vec[j]).Pt();
							posE[0]=i; posE[1]=j;
						}
					}
				}
			}
		}
	}
	if(E_charge.size()>1 && M_charge.size()>1){
		if( ( ( (M_4vec[posM[0]]+M_4vec[posM[1]]).Pt() > (E_4vec[posE[0]]+E_4vec[posE[1]]).Pt() ) && high) || ( ( (M_4vec[posM[0]]+M_4vec[posM[1]]).Pt() < (E_4vec[posE[0]]+E_4vec[posE[1]]).Pt() ) && !high) ){
			ossfCouple.push_back(M_4vec[posM[0]]);
			ossfCouple.push_back(M_4vec[posM[1]]);
		}
		else{
			ossfCouple.push_back(E_4vec[posE[0]]);
			ossfCouple.push_back(E_4vec[posE[1]]);
		}
	}
	if(E_charge.size()>2){
			ossfCouple.push_back(E_4vec[posE[0]]);
			ossfCouple.push_back(E_4vec[posE[1]]);
	}
	if(M_charge.size()>2){
			ossfCouple.push_back(M_4vec[posM[0]]);
			ossfCouple.push_back(M_4vec[posM[1]]);
	}


	if(solo){
		FourVectorVec noZhightpt;
		int first=-1;
		int second =-1;
		for(int i=0; i<(E_charge.size()); i++){
			if(i!=posE[0] && i!=posE[1]){
				if(first==-1) first==i;
				else second==i;
			}
		}
		if(first!=-1) noZhightpt.push_back(E_4vec[first]);
		if(second!=-1) noZhightpt.push_back(E_4vec[second]);

		for(int i=0; i<(M_charge.size()); i++){
			if(i!=posM[0] && i!=posM[1]){
				if(first==-1) first==i;
				else second==i;
			}
		}
		if(first!=-1) noZhightpt.push_back(M_4vec[first]);
		if(second!=-1) noZhightpt.push_back(M_4vec[second]);
		if(noZhightpt.size()==2){
			if(noZhightpt[0].Pt() < noZhightpt[1].Pt()){
				FourVector temp = noZhightpt[0];
				noZhightpt[0]=noZhightpt[1];
				noZhightpt[1]=temp;
			}
		}

		if(noZhightpt.size()<=2) return noZhightpt;
		if(noZhightpt.size()>2) cout<<"Error VecPtLeptonsOSSF"<<endl;
	}
	return ossfCouple;

}

// Function definition for selecting the Opposite-Sign Same-Flavor (OSSF) lepton pair
FourVectorVec VecPtLeptonsOSSF_v2(ints E_charge, ints M_charge, FourVectorVec M_4vec, FourVectorVec E_4vec, bool high, bool solo) {
    // Vector to store the OSSF lepton pair
    FourVectorVec ossfCouple;

    //------ 3 leptons
    if (E_charge.size() + M_charge.size() == 3) {
        // If two electron charges are present, select them
        if (E_charge.size() == 2) {
            ossfCouple.push_back(E_4vec[0]);
            ossfCouple.push_back(E_4vec[1]);
            return ossfCouple;
        }
        // If two muon charges are present, select them
        else if (M_charge.size() == 2) {
            ossfCouple.push_back(M_4vec[0]);
            ossfCouple.push_back(M_4vec[1]);
            return ossfCouple;
        }
		//----- If 3 same flavor, find the highest pt couple
		else if (E_charge.size()==3){
			float maxPt = 0;
			vector<int> posE = {-1, -1};
			for (int i = 0; i < E_charge.size() - 1; i++) {
            	for (int j = i + 1; j < E_charge.size(); j++) {
					if (E_charge[i] != E_charge[j]) {
						float pairPt = (E_4vec[i] + E_4vec[j]).Pt();
						if ((high && pairPt > maxPt) || (!high && (pairPt < maxPt || maxPt == 0))) {
							maxPt = pairPt;
							posE = {i, j};
						}
					}
            	}
        	}
			ossfCouple.push_back(E_4vec[posE[0]]);
			ossfCouple.push_back(E_4vec[posE[1]]);
			return ossfCouple;
		}
		else if (M_charge.size()==3){
			float maxPt = 0;
			vector<int> posM = {-1, -1};
			for (int i = 0; i < M_charge.size() - 1; i++) {
            	for (int j = i + 1; j < M_charge.size(); j++) {
					if (M_charge[i] != M_charge[j]) {
						float pairPt = (M_4vec[i] + M_4vec[j]).Pt();
						if ((high && pairPt > maxPt) || (!high && (pairPt < maxPt || maxPt == 0))) {
							maxPt = pairPt;
							posM = {i, j};
						}
					}
            	}
        	}
			ossfCouple.push_back(M_4vec[posM[0]]);
			ossfCouple.push_back(M_4vec[posM[1]]);
			return ossfCouple;
		}
    }
	//-----4+ leptons case
    float maxPt = 0;
    // Positions of muon and electron pairs
    vector<int> posM = {-1, -1};
    vector<int> posE = {-1, -1};

    // Selecting muon pair with highest Pt
    if (M_charge.size() > 1) {
        for (int i = 0; i < M_charge.size() - 1; i++) {
            for (int j = i + 1; j < M_charge.size(); j++) {
                if (M_charge[i] != M_charge[j]) {
                    float pairPt = (M_4vec[i] + M_4vec[j]).Pt();
                    if ((high && pairPt > maxPt) || (!high && (pairPt < maxPt || maxPt == 0))) {
                        maxPt = pairPt;
                        posM = {i, j};
                    }
                }
            }
        }
    }

    // Selecting electron pair with highest Pt
    if (E_charge.size() > 1) {
        for (int i = 0; i < E_charge.size() - 1; i++) {
            for (int j = i + 1; j < E_charge.size(); j++) {
                if (E_charge[i] != E_charge[j]) {
                    float pairPt = (E_4vec[i] + E_4vec[j]).Pt();
                    if ((high && pairPt > maxPt) || (!high && (pairPt < maxPt || maxPt == 0))) {
                        maxPt = pairPt;
                        posE = {i, j};
                    }
                }
            }
        }
    }

    // Selecting the OSSF pair based on maximum Pt
    if (E_charge.size() > 1 && M_charge.size() > 1) {
        if((M_4vec[posM[0]] + M_4vec[posM[1]]).Pt() > (E_4vec[posE[0]] + E_4vec[posE[1]]).Pt()){
			if(high){
				ossfCouple.push_back(M_4vec[posM[0]]);
				ossfCouple.push_back(M_4vec[posM[1]]);
				return ossfCouple;
			}
			else{
				ossfCouple.push_back(E_4vec[posE[0]]);
				ossfCouple.push_back(E_4vec[posE[1]]);
				return ossfCouple;
			}
		}
		else{
			if(high){
				ossfCouple.push_back(E_4vec[posE[0]]);
				ossfCouple.push_back(E_4vec[posE[1]]);
				return ossfCouple;
			}
			else{
				ossfCouple.push_back(M_4vec[posM[0]]);
				ossfCouple.push_back(M_4vec[posM[1]]);
				return ossfCouple;
			}
        
    	}
	}
	else if (E_charge.size() <= 1){
		ossfCouple.push_back(M_4vec[posM[0]]);
        ossfCouple.push_back(M_4vec[posM[1]]);
		return ossfCouple;
	}
	else if (M_charge.size() <= 1){
		ossfCouple.push_back(E_4vec[posE[0]]);
        ossfCouple.push_back(E_4vec[posE[1]]);
		return ossfCouple;
	}
	else{
		cout<<"Probleme here in VecPtLeptonsOSSF_v2"<<endl;
	}

    // If solo option is enabled, select the two leptons with highest Pt that are not in the OSSF pair
	// This has not been verified !
    if (solo) {
        FourVectorVec noZhightpt;
        for (int i = 0; i < E_charge.size(); i++) {
            if (i != posE[0] && i != posE[1]) {
                noZhightpt.push_back(E_4vec[i]);
            }
        }
        for (int i = 0; i < M_charge.size(); i++) {
            if (i != posM[0] && i != posM[1]) {
                noZhightpt.push_back(M_4vec[i]);
            }
        }
        // Sorting leptons by decreasing Pt
        sort(noZhightpt.begin(), noZhightpt.end(), [](const FourVector& a, const FourVector& b) {
            return a.Pt() > b.Pt();
        });
        // Selecting the first two leptons
        if (noZhightpt.size() >= 2) {
            ossfCouple.push_back(noZhightpt[0]);
            ossfCouple.push_back(noZhightpt[1]);
        }
    }

    // Return the OSSF lepton pair
    return ossfCouple;
}


bool Pt40_20_10(floats lep_pt){
	return (lep_pt[0]>40 && lep_pt[1]>20 && lep_pt[2]>10);
}

bool Is2OSSFfromZ(ints E_charge, FourVectorVec Elec, ints M_charge, FourVectorVec Muon){


	if (E_charge.size()+M_charge.size()!=4) return 0;
	bool nOSSF=false;
	float masse=20;

	if(E_charge.size() == 4){
		if(E_charge[0]==-E_charge[1] && ( fabs(calculate_invMass(Elec[0], Elec[1]) - Zmass) < masse )){
			if(E_charge[2]==-E_charge[3] && ( fabs(calculate_invMass(Elec[2], Elec[3]) - Zmass) < masse )) nOSSF=true;
		}
		else if(E_charge[0]==-E_charge[2] && ( fabs(calculate_invMass(Elec[0], Elec[2]) - Zmass) < masse )){
			if(E_charge[1]==-E_charge[3] && ( fabs(calculate_invMass(Elec[1], Elec[3]) - Zmass) < masse )) nOSSF=true;
		}
		else if(E_charge[0]==-E_charge[3] && ( fabs(calculate_invMass(Elec[0], Elec[3]) - Zmass) < masse )){
			if(E_charge[1]==-E_charge[2] && ( fabs(calculate_invMass(Elec[1], Elec[2]) - Zmass) < masse )) nOSSF=true;
		}
	}
	else if(M_charge.size() == 4){
		if(M_charge[0]==-M_charge[1] && ( fabs(calculate_invMass(Muon[0], Muon[1]) - Zmass) < masse )){
			if(M_charge[2]==-M_charge[3] && ( fabs(calculate_invMass(Muon[2], Muon[3]) - Zmass) < masse )) nOSSF=true;
		}
		else if(M_charge[0]==-M_charge[2] && ( fabs(calculate_invMass(Muon[0], Muon[2]) - Zmass) < masse )){
			if(M_charge[1]==-M_charge[3] && ( fabs(calculate_invMass(Muon[1], Muon[3]) - Zmass) < masse )) nOSSF=true;
		}
		else if(M_charge[0]==-M_charge[3] && ( fabs(calculate_invMass(Muon[0], Muon[3]) - Zmass) < masse )){
			if(M_charge[1]==-M_charge[2] && ( fabs(calculate_invMass(Muon[1], Muon[2]) - Zmass) < masse )) nOSSF=true;
		}
	}
	else if(M_charge.size()==2 && E_charge.size()==2){
		if(M_charge[0]==-M_charge[1] && ( abs(calculate_invMass(Muon[0], Muon[1]) - Zmass) < masse ) && E_charge[0]==-E_charge[1] && ( abs(calculate_invMass(Elec[0], Elec[1])-Zmass) < masse) ){
			nOSSF=true;
		}
	}
	return nOSSF;


}

floats sortVec (floats &V){
	return ROOT::VecOps::Sort(V, [](double x, double y) {return 1/x < 1/y;});
}

int testSortVec(floats &V){
	int nwrong=0;
	for (int i=0; i<V.size()-1;i++){
		if(V[i]<V[i+1]) nwrong++;
	}
	return nwrong;
}

floats addVecFloats (floats A, floats B){
	floats C;
	for (int i=0; i<A.size(); i++) C.push_back(A[i]);
	for (int i=0; i<B.size(); i++) C.push_back(B[i]);
	return C;
}

FourVectorVec addVecFourVec2 (FourVectorVec &A, FourVectorVec &B){
	FourVectorVec C;
	for (int i=0; i<A.size(); i++) C.push_back(A[i]);
	for (int i=0; i<B.size(); i++) C.push_back(B[i]);
	return C;
}

bools CheckOverlaps(FourVectorVec &jets, FourVectorVec &leps)
{       
        bools out;
        doubles mindrlepton;
        bool flag = false;
        for (auto ajet: jets){
                flag = false;
                auto mindr = 6.0;
                for (auto alepton: leps){
                        auto dr = ROOT::Math::VectorUtil::DeltaR(ajet, alepton);
                        if(dr < mindr){ 
                                mindr = dr;
                        }
                }
                if(mindr > 0.4){
                        flag = true;
                }
                else{
                        flag = false;
                }
                out.emplace_back(flag);
        }
        return out;
}

int CountGenDressed(FourVectorVec &leptons, FourVectorVec &gendressed){
	vector<int> used;
	bool pass=false;
	for(int i=0; i<leptons.size(); i++){
		for(int j=0; j<gendressed.size(); j++){
			if(calculate_deltaR(leptons[i], gendressed[j])<0.1){
				pass=false;
				for(int n=0; n<used.size();n++){
					if(j==used[n]) pass=true;
				}
				if(!pass){
					used.push_back(j);
				}
			}
		}
	}
	return used.size();
}

FourVectorVec associatedLeptons(FourVectorVec &leptons, FourVectorVec &gendressed, bool nature){
	FourVectorVec associated;
	FourVectorVec notAssociated;
	vector <vector <float>> bigTab; //[deltaR, gendressed, leptons]
	bool usedlB, useddlB;
	vector <int> usedl, useddl;
	float minDr=0.1;

	//there is smarter ways but i'm tired
	for(int i = 0; i<leptons.size();i++){
		for(int j=0;j<gendressed.size();j++){
			bigTab.push_back({calculate_deltaR(leptons[i], gendressed[j]), j, i});
		}
	}

	sort(bigTab.begin(), bigTab.end());

	for(int i=0; i<bigTab.size();i++){
		while(bigTab[i][0]<minDr){ // Tag the leptons as associated if Dr<minDr
			usedlB, useddlB=false;
			for(int j=0;j<usedl.size();j++){
				if(bigTab[i][2]==usedl[j]) usedlB=true;
			}
			for(int j=0;j<useddl.size();j++){
				if(bigTab[i][1]==useddl[j]) useddlB=true;
			}
			if (!usedlB && !useddlB){
				associated.push_back(leptons[bigTab[i][2]]);
				usedl.push_back(bigTab[i][2]);
				useddl.push_back(bigTab[i][1]);
			}
			i++;
			if(i==bigTab.size()) break;
		}
	}
	return associated;

}

ints isThisTrue(ints &goodLeptons, ints &Lepton_genPartIdx, ints &GenPart_statusFlags){	
	ints Trues;
	for(int i=0; i<goodLeptons.size();i++){
		if(Lepton_genPartIdx[goodLeptons[i]] >= 0){
			if(GenPart_statusFlags[Lepton_genPartIdx[goodLeptons[i]]]>>0 &1){
				Trues.push_back(goodLeptons[i]);
			}
		}
	}
	return Trues;
}

ints isThisPrompt(ints &goodLeptons, ints &Lepton_genPartIdx, ints &GenPart_pdgId, ints &GenPart_genPartIdxMother){
	ints Trues;
	for(int i=0; i<goodLeptons.size();i++){
		if(Lepton_genPartIdx[goodLeptons[i]] >= 0){
			int genId=Lepton_genPartIdx[goodLeptons[i]];
			int motherId=GenPart_genPartIdxMother[Lepton_genPartIdx[goodLeptons[i]]];
			while(GenPart_pdgId[genId]==GenPart_pdgId[motherId]){
				genId=motherId;
				motherId=GenPart_genPartIdxMother[genId];
			}
			int pdgID=abs(GenPart_pdgId[genId]);
			int pdgIDmother=abs(GenPart_pdgId[motherId]);
			//std::cout<<"pdgID is "<<pdgID<<" and MotherID is "<<pdgIDmother<<endl;
			if((pdgID==11 || pdgID==13 ||pdgID==15) && (pdgIDmother == 23 || pdgIDmother == 24 || pdgIDmother == 25)){
				Trues.push_back(goodLeptons[i]);
				//std::cout<<"pdgIDok : "<<pdgIDmother<<endl;
			}
			else{
				//std::cout<<"pdgIDnotok : "<<pdgIDmother<<endl;
			}
		}
	}
	//std::cout<<Trues.size()<<std::endl;
	return Trues;
}

int nPrompt(ints &goodLeptons, ints &Lepton_genPartIdx, ints &GenPart_pdgId, ints &GenPart_genPartIdxMother){
	int Trues=0;
	for(int i=0; i<goodLeptons.size();i++){
		if(Lepton_genPartIdx[goodLeptons[i]] >= 0){
			int genId=Lepton_genPartIdx[goodLeptons[i]];
			int motherId=GenPart_genPartIdxMother[Lepton_genPartIdx[goodLeptons[i]]];
			while(GenPart_pdgId[genId]==GenPart_pdgId[motherId]){
				genId=motherId;
				motherId=GenPart_genPartIdxMother[genId];
			}
			int pdgID=abs(GenPart_pdgId[genId]);
			int pdgIDmother=abs(GenPart_pdgId[motherId]);
			//std::cout<<"pdgID is "<<pdgID<<" and MotherID is "<<pdgIDmother<<endl;
			if((pdgID==11 || pdgID==13 ||pdgID==15) && (pdgIDmother == 23 || pdgIDmother == 24 || pdgIDmother == 25)){
				Trues++;
				//std::cout<<"pdgIDok : "<<pdgIDmother<<endl;
			}
			else{
				//std::cout<<"pdgIDnotok : "<<pdgIDmother<<endl;
			}
		}
	}
	return Trues;
}

ints isThisPrompt2(uchars genPartFlav){
	ints Trues;
	for(int i=0; i<genPartFlav.size(); i++){
		int genpart = (int) genPartFlav[i];
		Trues.push_back(genpart==1);
	}
	return Trues;
}

ints isThisConversion(uchars genPartFlav){
	ints Conv;
	for(int i=0; i<genPartFlav.size(); i++){
		int genpart = (int) genPartFlav[i];
		Conv.push_back(genpart==22);
	}
	return Conv;
}

bools passClosestJetCSV(FourVectorVec &jet_4vec, FourVectorVec &lepton_4vec, floats &jets_btag){
	bools passDeepCSV;
	float deepCSV;
	float dr;
	for(int i=0; i<lepton_4vec.size(); i++){
		dr=999;
		for(int j=0;j<jet_4vec.size();j++){
			if(calculate_deltaR(jet_4vec[j], lepton_4vec[i]) < dr){
				dr = calculate_deltaR(jet_4vec[j], lepton_4vec[i]);
				deepCSV = jets_btag[j];
			}
		}
		if(jet_4vec.size()==0) deepCSV=0; // if no jet, then there is no jet's deep csv, so we put it to 0
		passDeepCSV.push_back(deepCSV < 0.8001);
	}
	return passDeepCSV;
}

floats closestJetCSV(FourVectorVec &jet_4vec, FourVectorVec &lepton_4vec, floats &jets_btag){
	floats passDeepCSV;
	float deepCSV;
	float dr;
	for(int i=0; i<lepton_4vec.size(); i++){
		dr=0.4; // As in the lmva
		for(int j=0;j<jet_4vec.size();j++){
			if(calculate_deltaR(jet_4vec[j], lepton_4vec[i]) < dr){
				dr = calculate_deltaR(jet_4vec[j], lepton_4vec[i]);
				deepCSV = jets_btag[j];
			}
		}
		if(jet_4vec.size()==0 || dr == 4) deepCSV=0;
		passDeepCSV.push_back(deepCSV);
	}
	return passDeepCSV;
}

bools checkoverlap(FourVectorVec &tightJets, FourVectorVec &goodlep)
{
	doubles mindrlepton;
	for (auto ajet: tightJets)
	{
		auto mindr = 6.0;
		for (auto alepton: goodlep)
		{
			auto dr = ROOT::Math::VectorUtil::DeltaR(ajet, alepton);
			if (dr < mindr) mindr = dr;
		}
		int out = mindr > 0.4 ? 1 : 0;
		mindrlepton.emplace_back(out);

	}
	return mindrlepton;
}

float calculate_invMass_syst( FourVectorVec &p){ //To verify
	FourVector pf;
	if(p.size()==0) return -1;
	for(int i=0; i<p.size();i++){
		pf+=p[i];
	}
	return pf.M(); 
}

float printit (float w){
	std::cout<<w<<std::endl;
	std::cout<<"TEST"<<std::endl;
	return 1;
}

// --------Part for LMVA-----------
// bools getMVAscore(floats pdgID, floats pt, floats eta, floats jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats segmentComp) {

// 	bools passMva;

// 	for(int i=0; i<pdgID.size(); i++){
// 		std::string command = "python3 LMVA/getMVAscore.py ";
// 		std::string arg = std::to_string(pdgID[i]) +" "+ std::to_string(pt[i])+" "+std::to_string(eta[i]) +" "+ std::to_string(jetNDauCharged[i]) +" "+std::to_string(miniPFRelIso_chg[i]) +" "+ std::to_string(miniPFRelIso_all[i]) +" "+ std::to_string(jetPtRelv2[i]) +" "+ std::to_string(jetPtRatio[i]) +" "+ std::to_string(pfRelIso03_all[i]) +" "+ std::to_string(jetBTagv[i]) +" "+ std::to_string(sip3d[i]) +" "+ std::to_string(dxy[i]) +" "+ std::to_string(dz[i]) +" "+ std::to_string(segmentComp[i]);
// 		FILE *pipe = popen((command + arg).c_str(), "r");

// 		char buffer[128];
// 		std::string output;

// 		while (!feof(pipe)) {
// 			if (fgets(buffer, 128, pipe) != nullptr) {
// 				output = buffer;
// 			}
// 		}

// 		pclose(pipe);

// 		std::istringstream iss(output);
// 		double mvaScore_v1, WP_v1, mvaScore_v2, WP_v2;
// 		iss >> mvaScore_v1 >> WP_v1 >> mvaScore_v2 >> WP_v2;
// 		passMva.emplace_back(mvaScore_v1>0.7); // Change this, it's not the real value
// 		cout<<"done"<<endl;
// 	}

// 	return passMva;
// }

floats getMvaScore(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats segmentComp){
	floats wp;
	for (int i=0; i<pt.size();i++){
		wp.emplace_back(getScoreMuon(0, pdgID[i], pt[i], eta[i], jetNDauCharged[i], miniPFRelIso_chg[i], miniPFRelIso_all[i], jetPtRelv2[i], jetPtRatio[i], pfRelIso03_all[i], jetBTagv[i], sip3d[i], dxy[i], dz[i], segmentComp[i]));
		std::cout<<wp[i]<<std::endl;
	}
	return wp;
}

ints getMvaWpMuon(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats segmentComp){
	ints wp;
	for (int i=0; i<pt.size();i++){
		wp.emplace_back((int)getScoreMuon(1, pdgID[i], pt[i], eta[i], jetNDauCharged[i], miniPFRelIso_chg[i], miniPFRelIso_all[i], jetPtRelv2[i], jetPtRatio[i], pfRelIso03_all[i], jetBTagv[i], sip3d[i], dxy[i], dz[i], segmentComp[i]));
	}
	return wp;
}

floats getMvaScoreMuon(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats segmentComp){
	floats wp;
	for (int i=0; i<pt.size();i++){
		wp.emplace_back(getScoreMuon(0, pdgID[i], pt[i], eta[i], jetNDauCharged[i], miniPFRelIso_chg[i], miniPFRelIso_all[i], jetPtRelv2[i], jetPtRatio[i], pfRelIso03_all[i], jetBTagv[i], sip3d[i], dxy[i], dz[i], segmentComp[i]));
	}
	return wp;
}

ints getMvaWpEl(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats mvaFall17V2noIso, uchars lostHits){
	ints wp;
	for (int i=0; i<pt.size();i++){
		wp.emplace_back((int)getScoreEl(1, pdgID[i], pt[i], eta[i], jetNDauCharged[i], miniPFRelIso_chg[i], miniPFRelIso_all[i], jetPtRelv2[i], jetPtRatio[i], pfRelIso03_all[i], jetBTagv[i], sip3d[i], dxy[i], dz[i], mvaFall17V2noIso[i], lostHits[i]));
	}
	return wp;
}

floats getMvaScoreEl(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats mvaFall17V2noIso, uchars lostHits){
	floats wp;
	for (int i=0; i<pt.size();i++){
		wp.emplace_back(getScoreEl(0, pdgID[i], pt[i], eta[i], jetNDauCharged[i], miniPFRelIso_chg[i], miniPFRelIso_all[i], jetPtRelv2[i], jetPtRatio[i], pfRelIso03_all[i], jetBTagv[i], sip3d[i], dxy[i], dz[i], mvaFall17V2noIso[i], lostHits[i]));
	}
	return wp;
}

bool passMva(ints mumva, ints elmva){
	for(int wp : elmva){
		if(wp != 4) return false;
	}
	for(int wp : mumva){
		if(wp !=4 ) return false;
	}
	return true;
}

bool is10FromZ(float invMass){
	return fabs(invMass - Zmass) < 10;
}

ROOT::RDF::RNode defineNameMuon(ROOT::RDF::RNode _rlm, std::string baseName, bool _isData){
	_rlm = _rlm.Define(baseName+"_pt", "Muon_pt["+baseName+"]")
			.Define(baseName+"_eta", "Muon_eta["+baseName+"]")
			.Define(baseName+"_phi", "Muon_phi["+baseName+"]")
			.Define(baseName+"_mass", "Muon_mass["+baseName+"]")
			.Define(baseName+"_charge", "Muon_charge["+baseName+"]")
			.Define(baseName+"_idx", ::good_idx, {""+baseName+""})
			.Define("N"+baseName, "int("+baseName+"_pt.size())")
			.Define(baseName+"OSSF", "(N"+baseName+" - abs(Sum("+baseName+"_charge)))/2")
			.Define(baseName+"_pdgId", "Muon_pdgId["+baseName+"]")
			.Define(baseName+"_jetNDauCharged", "Muon_jetNDauCharged["+baseName+"]")
			.Define(baseName+"_miniPFRelIso_chg", "Muon_miniPFRelIso_chg["+baseName+"]")
			.Define(baseName+"_miniPFRelIso_all", "Muon_miniPFRelIso_all["+baseName+"]")
			.Define(baseName+"_jetPtRelv2", "Muon_jetPtRelv2["+baseName+"]")
			.Define(baseName+"_jetRelIso", "Muon_jetRelIso["+baseName+"]")
			.Define(baseName+"_pfRelIso03_all", "Muon_pfRelIso03_all["+baseName+"]")
			.Define(baseName+"_sip3d", "Muon_sip3d["+baseName+"]")
			.Define(baseName+"_dxy", "Muon_dxy["+baseName+"]")
			.Define(baseName+"_dz", "Muon_dz["+baseName+"]")
			.Define(baseName+"_segmentComp","Muon_segmentComp["+baseName+"]")
			.Define(baseName+"_4vecs", ::generate_4vec, {baseName+"_pt", baseName+"_eta", baseName+"_phi", baseName+"_mass"});

	if(!_isData) _rlm = _rlm.Define(baseName+"_genPartFlav", "Muon_genPartFlav["+baseName+"]");
    if(!_isData) _rlm = _rlm.Define("isPrompt"+baseName, ::isThisPrompt2, {baseName+"_genPartFlav"});
    if(!_isData) _rlm = _rlm.Define(baseName+"PromptPt", baseName+"_pt[isPrompt"+baseName+"]");

	if(!_isData) _rlm = _rlm.Define("isConversion"+baseName, ::isThisConversion, {baseName+"_genPartFlav"});
	if(!_isData) _rlm = _rlm.Define(baseName+"ConversionPt", baseName+"_pt[isConversion"+baseName+"]");

	if (!_isData) {
		_rlm = _rlm.Define("n"+baseName+"Prompt", "Sum(isPrompt"+baseName+")")
				   .Define("n"+baseName+"Conversion", "Sum(isConversion"+baseName+")")
                   .Define("n"+baseName+"NotPrompt", "int(isPrompt"+baseName+".size() - n"+baseName+"Prompt)");
	}

	return _rlm;
}

ROOT::RDF::RNode defineNameElectron(ROOT::RDF::RNode _rlm, std::string baseName, bool _isData){
	    _rlm = _rlm.Define(baseName+"_pt", "Electron_pt["+baseName+"]")
               .Define(baseName+"_eta", "Electron_eta["+baseName+"]")
               .Define(baseName+"_phi", "Electron_phi["+baseName+"]")
               .Define(baseName+"_mass", "Electron_mass["+baseName+"]")
               .Define(baseName+"_idx", ::good_idx, {baseName+""})
               .Define("N"+baseName, "int("+baseName+"_pt.size())")
               .Define(baseName+"_charge", "Electron_charge["+baseName+"]")
               .Define(baseName+"OSSF", "(N"+baseName+" - abs(Sum("+baseName+"_charge)))/2")
               .Define(baseName+"_pdgId", "Electron_pdgId["+baseName+"]")
               .Define(baseName+"_jetNDauCharged", "Electron_jetNDauCharged["+baseName+"]")
               .Define(baseName+"_miniPFRelIso_chg", "Electron_miniPFRelIso_chg["+baseName+"]")
               .Define(baseName+"_miniPFRelIso_all", "Electron_miniPFRelIso_all["+baseName+"]")
               .Define(baseName+"_jetPtRelv2", "Electron_jetPtRelv2["+baseName+"]")
               .Define(baseName+"_jetRelIso", "Electron_jetRelIso["+baseName+"]")
               .Define(baseName+"_pfRelIso03_all", "Electron_pfRelIso03_all["+baseName+"]")
               .Define(baseName+"_sip3d", "Electron_sip3d["+baseName+"]")
               .Define(baseName+"_dxy", "Electron_dxy["+baseName+"]")
               .Define(baseName+"_dz", "Electron_dz["+baseName+"]")
               .Define(baseName+"_mvaFall17V2noIso", "Electron_mvaFall17V2noIso["+baseName+"]")
               .Define(baseName+"_lostHits", "Electron_lostHits["+baseName+"]")
               .Define(baseName+"_4vecs", ::generate_4vec, {baseName+"_pt", baseName+"_eta", baseName+"_phi", baseName+"_mass"});
		
		if(!_isData) _rlm = _rlm.Define(baseName+"_genPartFlav", "Electron_genPartFlav["+baseName+"]");
    	if(!_isData) _rlm = _rlm.Define("isPrompt"+baseName, ::isThisPrompt2, {baseName+"_genPartFlav"});
    	if(!_isData) _rlm = _rlm.Define(baseName+"PromptPt", baseName+"_pt[isPrompt"+baseName+"]");

		if(!_isData) _rlm = _rlm.Define("isConversion"+baseName, ::isThisConversion, {baseName+"_genPartFlav"});
		if(!_isData) _rlm = _rlm.Define(baseName+"ConversionPt", baseName+"_pt[isConversion"+baseName+"]");

		if (!_isData) {
		_rlm = _rlm.Define("n"+baseName+"Prompt", "Sum(isPrompt"+baseName+")")
				   .Define("n"+baseName+"Conversion", "Sum(isConversion"+baseName+")")
				   .Define("n"+baseName+"NotPrompt", "int(isPrompt"+baseName+".size() - n"+baseName+"Prompt - n"+baseName+"Conversion)");
		}

		return _rlm;

}

float leading(floats &particles, floats &variables){
	if(particles.size()!=variables.size()){
		std::cerr << "Wrong arguments for leading function" << std::endl;
	}
	floats pts;
	pts = sortVec(particles);
	for(int i=0; i<particles.size(); i++){
		if(particles[i]==pts[0]){
			return variables[i];
		}
	}
}

float subleading(floats &particles, floats &variables){
	if(particles.size()!=variables.size()){
		std::cerr << "Wrong arguments for subleading function" << std::endl;
	}
	floats pts;
	pts = sortVec(particles);
	for(int i=0; i<particles.size(); i++){
		if(particles[i]==pts[1]){
			return variables[i];
		}
	}
}

float trailing(floats &particles, floats &variables){
	if(particles.size()!=variables.size()){
		std::cerr << "Wrong arguments for trailing function" << std::endl;
	}
	floats pts;
	pts = sortVec(particles);
	for(int i=0; i<particles.size(); i++){
		if(particles[i]==pts[pts.size()-1]){
			return variables[i];
		}
	}
}

float mindR(FourVectorVec &part1, FourVectorVec &part2){
	float mindr=-1;
	for(int i=0; i<part1.size(); i++){
		for(int j=0; j<part2.size(); j++){
			if(calculate_deltaR(part1[i], part2[j])<mindr || mindr==-1) mindr=calculate_deltaR(part1[i], part2[j]);
		}
	}
	return mindr;
}

float maxdR(FourVectorVec &part1, FourVectorVec &part2){
	float maxdr=-1;
	for(int i=0; i<part1.size(); i++){
		for(int j=0; j<part2.size(); j++){
			if(calculate_deltaR(part1[i], part2[j])>maxdr || maxdr==-1) maxdr=calculate_deltaR(part1[i], part2[j]);
		}
	}
	return maxdr;
}

float maxM(FourVectorVec &part1, FourVectorVec &part2){
	float maxM=-1;
	for(int i=0; i<part1.size(); i++){
		for(int j=0; j<part2.size(); j++){
			if(calculate_invMass(part1[i], part2[j])>maxM || maxM==-1) maxM=calculate_invMass(part1[i], part2[j]);
		}
	}
	return maxM;
}

float mindRl1(FourVector &part1, FourVectorVec &part2){
	float mindr=-1;
	for(int j=0; j<part2.size(); j++){
		if(calculate_deltaR(part1, part2[j])<mindr || mindr==-1) mindr=calculate_deltaR(part1, part2[j]);
	}
	return mindr;
}

floats vecEnergies(FourVectorVec &part){
	floats energies;
	for(int i=0;i<part.size();i++){
		energies.emplace_back(part[i].E());
	}
	return energies;

}