/*
 * BaseAnalyser.h
 *
 *  Created on: May 6, 2022
 *      Author: suyong
 *		Developper: cdozen
 */

#ifndef BASEANALYSER_H_
#define BASEANALYSER_H_

#include "NanoAODAnalyzerrdframe.h"
#include "TMVA/RBDT.hxx"
#include "TMVA/RReader.hxx"


class BaseAnalyser: public NanoAODAnalyzerrdframe
{
	public:
		BaseAnalyser(TTree *t, std::string outfilename);
		void defineCuts(bool stepone);		//define a series of cuts from defined variables only. you must implement this in your subclassed analysis 
		void defineMoreVars(bool BTagEff); 	//define higher-level variables from basic ones, you must implement this in your subclassed analysis code
		void bookHists(bool BTagEff, const std::vector<std::string>& weights); 		//book histograms, you must implement this in your subclassed analysis code

		void setTree(TTree *t, std::string outfilename, bool BTagEff, const std::vector<std::string>& weights);
		void setupObjects(bool BTagEff);
		void evaluate_and_store_mva(const std::string &SR_BDT_fname, const std::string &region);
		void setupAnalysis(bool BTagEff, bool stepone, const std::vector<std::string>& weights);
		ROOT::RDF::RNode redefineVars(RNode _rlm, std::string year, std::string process);
		// object selectors
		void selectElectrons();
		void selectMuons();
		void selectJets(bool BTagEff);
		void calculateEvWeight();
		void selectMET();
		void removeOverlaps();
		void defineCutsHLT();
		void selectLeptons();
		void saveForRoc();
		void applyMva();
		void defineRegion();
		void defineObservable();




		bool debug = true;
		bool _jsonOK;
		string _outfilename;


		TFile *_outrootfile;
		vector<string> _outrootfilenames;


};



#endif /* BASEANALYSER_H_ */
