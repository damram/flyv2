#include <iostream>
#include <vector>
#include <cmath>
#include <xgboost/c_api.h>
#include<map>

class MvaTOPReader {
public:
    MvaTOPReader(const std::string& year, const std::vector<std::string>& versions = {"v1", "v2"}) {
        std::string yearstring = "UL18";
        if (year == "UL2016") {
            yearstring = "UL16";
        } else if (year == "UL2016_preVFP") {
            yearstring = "UL16APV";
        } else if (year == "UL2017") {
            yearstring = "UL17";
        } else if (year == "UL2018") {
            yearstring = "UL18";
        }

        versions_ = versions;

        // Working points
        wps_ = {
            {"v1", {0.20, 0.41, 0.64, 0.81}},
            {"v2", {0.59, 0.81, 0.90, 0.94}}
        };

        std::string directory = "mvaWeights/";

        // Load Electron weights
        bst_el_ = loadBoosters("el_TOP", "XGB.weights.bin", yearstring);

        // Load Muon weights
        bst_mu_ = loadBoosters("mu_TOP", "XGB.weights.bin", yearstring);
    }
    ~MvaTOPReader() {
        // Libérer la mémoire des boosters
        XGBoosterFree(bst_el_["v2"]);
        XGBoosterFree(bst_mu_["v1"]);
    }

    std::tuple<float, int> getMvaTOPScore(const std::map<std::string, float>& lep) {
        std::vector<float> results;
        if (std::abs(lep.at("pdgId")) == 11) {
            const float features[] {
                lep.at("pt"), 
                lep.at("eta"), 
                static_cast<float>(lep.at("jetNDauCharged")), // jetNDauChargedMVASel
                lep.at("miniPFRelIso_chg"), // miniRelIsoCharged
                lep.at("miniPFRelIso_all") - lep.at("miniPFRelIso_chg"), // miniRelIsoNeutralVanilla
                lep.at("jetPtRelv2"),
                lep.at("jetPtRatio"), // jetPtRatioVanilla 
                lep.at("pfRelIso03_all"), // relIso0p3Vanilla
                lep.at("jetBTag"),
                lep.at("sip3d"),
                std::log(std::abs(lep.at("dxy"))),
                std::log(std::abs(lep.at("dz"))),
                lep.at("mvaFall17V2noIso"), // eleMvaFall17v2
                static_cast<float>(lep.at("lostHits")) // eleMissingHits
            };
            DMatrixHandle dtest;
            XGDMatrixCreateFromMat(features, 1, 14, NAN, &dtest);
            for (const auto& v : versions_) {
                float mvaScore;
                bst_ulong out_len;
                const float *out_result;
                XGBoosterPredict(bst_el_[v], dtest, 0, 0, 0, &out_len, &out_result);
                mvaScore = out_result[0];
                int WP = 0;
                for (const auto& wp : wps_[v]) {
                    if (mvaScore > wp) {
                        WP += 1;
                    }
                }
                results.push_back(mvaScore);
                results.push_back(WP);
            }
            XGDMatrixFree(dtest);
        } else if (std::abs(lep.at("pdgId")) == 13) {
            const float features[] {
                lep.at("pt"), 
                lep.at("eta"), 
                static_cast<float>(lep.at("jetNDauCharged")), // jetNDauChargedMVASel
                lep.at("miniPFRelIso_chg"), // miniRelIsoCharged
                lep.at("miniPFRelIso_all") - lep.at("miniPFRelIso_chg"), // miniRelIsoNeutralVanilla
                lep.at("jetPtRelv2"),
                lep.at("jetPtRatio"), // jetPtRatioVanilla
                lep.at("pfRelIso03_all"), // relIso0p3DBVanilla
                lep.at("jetBTag"),
                lep.at("sip3d"),
                std::log(std::abs(lep.at("dxy"))),
                std::log(std::abs(lep.at("dz"))),
                lep.at("segmentComp") // segComp
            };
            DMatrixHandle dtest;
            XGDMatrixCreateFromMat(features, 1, 13, NAN, &dtest);
            for (const auto& v : versions_) {
                float mvaScore;
                bst_ulong out_len;
                const float *out_result;
                XGBoosterPredict(bst_mu_[v], dtest, 0, 0, 0, &out_len, &out_result);
                mvaScore = out_result[0];
                int WP = 0;
                for (const auto& wp : wps_[v]) {
                    if (mvaScore > wp) {
                        WP += 1;
                    }
                }
                results.push_back(mvaScore);
                results.push_back(WP);
            }
            XGDMatrixFree(dtest);
        }
        return {results[0], static_cast<int>(results[1])};
    }
    std::map<std::string, std::vector<float>> wps_;
    std::vector<std::string> versions_;
    std::map<std::string, BoosterHandle> bst_el_;
    std::map<std::string, BoosterHandle> bst_mu_;

    std::map<std::string, BoosterHandle> loadBoosters(const std::string& prefix, const std::string& postfix, const std::string& yearstring) {
        std::map<std::string, BoosterHandle> boosters;
        for (const auto& v : versions_) {
            std::string modelFile = "mvaWeights/" + prefix + v + yearstring + "_" + postfix;
            XGBoosterCreate(0, 0, &boosters[v]);
            XGBoosterSetParam(boosters[v], "verbosity", "0");
            XGBoosterLoadModel(boosters[v], modelFile.c_str());
        }
        return boosters;
    }
};

extern "C" float getScoreMuon(bool cases, int pdgID, float pt, float eta, char jetNDauCharged, float miniPFRelIso_chg, float miniPFRelIso_all, float jetPtRelv2, float jetPtRatio, float pfRelIso03_all, float jetBTag, float sip3d, float dxy, float dz, float segmentComp) {
    // Example usage
    MvaTOPReader mvaReader("UL2017", {"v2"});
    std::map<std::string, float> lep = {
        {"pt", pt},
        {"eta", eta},
        {"jetNDauCharged", jetNDauCharged},
        {"miniPFRelIso_chg", miniPFRelIso_chg},
        {"miniPFRelIso_all", miniPFRelIso_all},
        {"jetPtRelv2", jetPtRelv2},
        {"jetPtRatio", jetPtRatio},
        {"pfRelIso03_all", pfRelIso03_all},
        {"jetBTag", jetBTag},
        {"sip3d", sip3d},
        {"dxy", dxy},
        {"dz", dz},
        {"segmentComp", segmentComp},
        {"pdgId", pdgID}
    };
    auto result = mvaReader.getMvaTOPScore(lep);
    XGBoosterFree(mvaReader.bst_mu_["v2"]);

    if(cases) return (float)std::get<1>(result);
    else return (float)std::get<0>(result);
}

extern "C" float getScoreEl(bool cases, int pdgID, float pt, float eta, char jetNDauCharged, float miniPFRelIso_chg, float miniPFRelIso_all, float jetPtRelv2, float jetPtRatio, float pfRelIso03_all, float jetBTag, float sip3d, float dxy, float dz, float mvaFall17V2noIso, char lostHits) {
    // Example usage
    MvaTOPReader mvaReader("UL2017", {"v2"});
    std::map<std::string, float> lep = {
        {"pt", pt},
        {"eta", eta},
        {"jetNDauCharged", jetNDauCharged},
        {"miniPFRelIso_chg", miniPFRelIso_chg},
        {"miniPFRelIso_all", miniPFRelIso_all},
        {"jetPtRelv2", jetPtRelv2},
        {"jetPtRatio", jetPtRatio},
        {"pfRelIso03_all", pfRelIso03_all},
        {"jetBTag", jetBTag},
        {"sip3d", sip3d},
        {"dxy", dxy},
        {"dz", dz},
        {"mvaFall17V2noIso", mvaFall17V2noIso},
        {"lostHits", lostHits},
        {"pdgId", pdgID}
    };
    auto result = mvaReader.getMvaTOPScore(lep);
    XGBoosterFree(mvaReader.bst_mu_["v2"]);

    if(cases) return (float)std::get<1>(result);
    else return (float)std::get<0>(result);
}
