/*
 * utility.h
 *
 *  Created on: Dec 4, 2018
 *      Author: suyong
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#define Zmass 91.187

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RVec.hxx"
#include "Math/Vector4D.h"
#include "correction.h"
#include <string>
#include <TLorentzVector.h>

using floats =  ROOT::VecOps::RVec<float>;
using doubles =  ROOT::VecOps::RVec<double>;
using ints =  ROOT::VecOps::RVec<int>;
using bools = ROOT::VecOps::RVec<bool>;
using uchars = ROOT::VecOps::RVec<unsigned char>;

using FourVector = ROOT::Math::PtEtaPhiMVector;
using FourVectorVec = std::vector<FourVector>;
using FourVectorRVec = ROOT::VecOps::RVec<FourVector>;

ints pTcounter(floats vec);

struct hist1dinfo
{
	ROOT::RDF::TH1DModel hmodel;
	std::string varname;
	std::string weightname;
	std::string mincutstep;
} ;

//for 2D histograms
struct hist2dinfo
{
  ROOT::RDF::TH2DModel hmodel;
  std::string varname1;
  std::string varname2;
  std::string weightname;
  std::string mincutstep;
} ;



struct varinfo
{
	std::string varname;
	std::string vardefinition;
	std::string mincutstep;
};

struct cutinfo
{
	std::string cutdefinition;
	std::string idx;
};


// generates vectors of 4 vectors given vectors of pt, eta, phi, mass
FourVectorVec generate_4vec(floats &pt, floats &eta, floats &phi, floats &mass);

// return a vector size equal to length of x all filled with evWeight value
floats weightv(floats &x, float evWeight);

floats sphericity(FourVectorVec &p);

double foxwolframmoment(int l, FourVectorVec &p, int minj=0, int maxj=-1);

float pucorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string name, std::string syst, float ntruepileup);

ints good_idx(ints good); 
float calculate_deltaEta( FourVector &p1, FourVector &p2);
float calculate_deltaPhi( FourVector &p1, FourVector &p2);
float calculate_deltaPhi_scalars(double &phi1, double &phi2);
float calculate_deltaR( FourVector &p1, FourVector &p2);
float calculate_deltaR_syst( FourVectorVec &vec);
float calculate_invMass( FourVector &p1, FourVector &p2);
FourVector sum_4vec( FourVector &p1, FourVector &p2);
floats sort_discriminant( floats discr, floats obj );
FourVector select_leadingvec( FourVectorVec &v );
void PrintVector(floats myvector);
floats w_reconstruction (FourVectorVec &jets);
floats compute_DR (FourVectorVec &muons, ints goodMuons_charge);
float calculate_MT( FourVectorVec &muons, float met, float metphi);

FourVectorVec addVecFourVec(FourVector &A, FourVector &B, FourVector &C); // 
FourVector generate_single_4vec(double &pt, double &eta, double &phi, double &mass);
doubles calculateDeltaR_group(FourVectorVec &jets, FourVector &lepton);
TLorentzVector generate_TLorentzVector(double &pt, double &eta, double &phi, double &mass) ;

//floats btvcorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string name, std::string syst, floats &pts, floats &etas, ints &hadflav, floats &btags);


/*=====================================BJets SF============================================*/
floats btvcorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string name, std::string syst, ints &hadflav, floats &etas,floats &pts,  floats &btags);

floats btv_case1(std::unique_ptr<correction::CorrectionSet> &cset, std::string type, std::string sys,std::string wp, ints &hadflav, floats &etas, floats &pts );
floats btv_case2(std::unique_ptr<correction::CorrectionSet> &cset, std::string type, std::string sys,std::string wp, ints &hadflav, floats &etas, floats &pts );

floats btv_casetest(std::unique_ptr<correction::CorrectionSet>& cset, std::string type1, std::string sys, std::string wp, ints& hadflav, floats& etas, floats& pts);


/*=====================================Muons SF============================================*/
//floats muoncorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string type, std::string year, floats &etas, floats &pts, std::string sys);
floats muoncorrection(std::unique_ptr<correction::CorrectionSet> &cset, std::string type, std::string year, std::string runtype, floats &etas, floats &pts, std::string sys);

/* --------------- neutrino Reconstruction for SingleTop t-channel ------------------------*/

float calculateLambda(TLorentzVector &lepton, float met_pt, float met_phi);
float calculateDelta(TLorentzVector &lepton, float met_pt, float lambda);

// Reco Pz of neutrino
float calculate_nu_z(TLorentzVector &lepton, float lambda, float delta, float met_pt, float met_phi);


// Real Solutions
float calculate_nu_z_plus(TLorentzVector &lepton, float lambda, float delta);
float calculate_nu_z_minus(TLorentzVector &lepton, float lambda, float delta);

// Complex Solutions
float calculate_nu_z_complex(TLorentzVector &lepton, float met_pt, float met_phi);


float calculate_nu_energy(float met_pt, float met_phi, float met_pz);
TLorentzVector get_neutrino_TL4vec(float met_pt, float met_phi, float met_pz, float met_energy);

/* -------------- W boson Reconstruction for SingleTop t-channel ------------------------*/

TLorentzVector reconstructWboson_TL4vec(TLorentzVector &lepton, TLorentzVector &neutrino);

float MaxPt (floats &pt);
FourVectorVec HighPtLeptonsOSSF (ints E_charge, floats E_pt, ints M_charge, floats M_pt, FourVectorVec M_4vec, FourVectorVec E_4vec); //scalSum
FourVectorVec VecPtLeptonsOSSF (ints E_charge, ints M_charge, FourVectorVec M_4vec, FourVectorVec E_4vec, bool high, bool solo); //vecSum
FourVectorVec VecPtLeptonsOSSF_v2 (ints E_charge, ints M_charge, FourVectorVec M_4vec, FourVectorVec E_4vec, bool high, bool solo); //vecSum
bool Pt40_20_10(floats lep_pt);
bool Is2OSSFfromZ(ints E_charge, FourVectorVec Elec, ints M_charge, FourVectorVec Muon);
floats sortVec (floats &V);
int testSortVec(floats &V);
floats addVecFloats (floats A, floats B);
FourVectorVec addVecFourVec2 (FourVectorVec &A, FourVectorVec &B);
bools CheckOverlaps(FourVectorVec &jets, FourVectorVec &leps);
int CountGenDressed(FourVectorVec &leptons, FourVectorVec &gendressed);
FourVectorVec associatedLeptons(FourVectorVec &leptons, FourVectorVec &gendressed, bool nature);
ints isThisTrue(ints &goodLeptons, ints &Lepton_genPartIdx, ints &GenPart_statusFlags);
ints isThisPrompt(ints &goodLeptons, ints &Lepton_genPartIdx, ints &GenPart_pdgId, ints &GenPart_genPartIdxMother);
ints isThisPrompt2(uchars genPartFlav);
ints isThisConversion(uchars genPartFlav);
int nPrompt(ints &goodLeptons, ints &Lepton_genPartIdx, ints &GenPart_pdgId, ints &GenPart_genPartIdxMother);
bools passClosestJetCSV(FourVectorVec &jet_4vec, FourVectorVec &lepton_4vec, floats &jets_btag);
floats closestJetCSV(FourVectorVec &jet_4vec, FourVectorVec &lepton_4vec, floats &jets_btag);
bools checkoverlap(FourVectorVec &tightJets, FourVectorVec &goodlep);
float calculate_invMass_syst( FourVectorVec &p);
float printit (float w);
bools getMVAscore(floats pdgID, floats pt, floats eta, floats jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats segmentComp);
int getScore(floats m);
extern "C" float getScoreMuon(bool cases, int pdgID, float pt, float eta, char jetNDauCharged, float miniPFRelIso_chg, float miniPFRelIso_all, float jetPtRelv2, float jetPtRatio, float pfRelIso03_all, float jetBTagv, float sip3d, float dxy, float dz, float segmentComp);
extern "C" float getScoreEl(bool cases, int pdgID, float pt, float eta, char jetNDauCharged, float miniPFRelIso_chg, float miniPFRelIso_all, float jetPtRelv2, float jetPtRatio, float pfRelIso03_all, float jetBTag, float sip3d, float dxy, float dz, float mvaFall17V2noIso, char lostHits);
floats getMvaScore(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats segmentComp);
ints getMvaWpMuon(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats segmentComp);
floats getMvaScoreMuon(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats segmentComp);
ints getMvaWpEl(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats mvaFall17V2noIso, uchars lostHits);
floats getMvaScoreEl(ints pdgID, floats pt, floats eta, uchars jetNDauCharged, floats miniPFRelIso_chg, floats miniPFRelIso_all, floats jetPtRelv2, floats jetPtRatio, floats pfRelIso03_all, floats jetBTagv, floats sip3d, floats dxy, floats dz, floats mvaFall17V2noIso, uchars lostHits);
bool passMva(ints mumva, ints elmva);
bool is10FromZ(float invMass);
ROOT::RDF::RNode defineNameMuon(ROOT::RDF::RNode _rlm, std::string baseName, bool _isData);
ROOT::RDF::RNode defineNameElectron(ROOT::RDF::RNode _rlm, std::string baseName, bool _isData);
float leading(floats &particles, floats &variables);
float subleading(floats &particles, floats &variables);
float trailing(floats &particles, floats &variables);
float mindR(FourVectorVec &part1, FourVectorVec &part2);
float maxdR(FourVectorVec &part1, FourVectorVec &part2);
float maxM(FourVectorVec &part1, FourVectorVec &part2);
float mindRl1(FourVector &part1, FourVectorVec &part2);
floats vecEnergies(FourVectorVec &part);

#endif /* UTILITY_H_ */
