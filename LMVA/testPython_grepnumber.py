import re

# Ouvrir le fichier en mode lecture
with open('getDump.txt', 'r') as file:
    content = file.read()

# Initialiser un dictionnaire pour stocker les nombres par indice
numbers_dict = {}

# Utiliser une expression régulière pour extraire les nombres entre [f0<, [f1<, [f2<, ..., [f13< et ]
for i in range(14):
    pattern = r'\[f{0}<(-?\d+\.\d+)\]'.format(i)
    matches = re.findall(pattern, content)
    # Convertir les correspondances en nombres flottants
    numbers = [float(match) for match in matches]
    # Trouver le plus grand et le plus petit nombre
    if numbers:
        max_number = max(numbers)
        min_number = min(numbers)
        numbers_dict[f'f{i}'] = {'max': max_number, 'min': min_number}
    else:
        numbers_dict[f'f{i}'] = {'max': None, 'min': None}

# Afficher les résultats
for key, value in numbers_dict.items():
    if value['max'] is not None and value['min'] is not None:
        print('*'*20)
        print(f"Pour {key}, le plus grand nombre est : {value['max']}")
        print(f"Pour {key}, le plus petit nombre est : {value['min']}")
    else:
        print(f"Aucun nombre trouvé pour {key} entre [f{i}< et ] dans le fichier.")
