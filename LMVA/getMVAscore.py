import os
import xgboost as xgb
import numpy as np
from math import log, log10
import logging
from mvaTOPreader import mvaTOPreader
import ROOT
import sys

# Récupérer les arguments de la ligne de commande
args = sys.argv

electronVars_data = ['pt','eta','phi','pdgId','cutBased','miniPFRelIso_all','miniPFRelIso_chg','pfRelIso03_all','sip3d','convVeto','dxy','dz','charge','deltaEtaSC', 'mvaFall17V2noIso_WPL', 'jetPtRelv2', 'jetRelIso', 'mvaFall17V2Iso_WP90', 'vidNestedWPBitmap','mvaTTH', 'jetRelIso', 'jetIdx', 'sieie', 'hoe', 'eInvMinusPInv', 'pfRelIso04_all', 'mvaFall17V2noIso', 'mvaFall17V2noIso_WP80', 'lostHits', 'jetNDauCharged', 'jetRelIso', 'tightCharge', 'jetBTag']
electronVars = electronVars_data + []

# def getVarValue(c, var, n=-1):
#     try:
#         att = getattr(c, var)
#     except AttributeError:
#         return float('nan')
#     if n>=0:
# #    print "getVarValue %s %i"%(var,n)
#         if n<att.__len__():
#             return att[n]
#         else:
#             return float('nan')
#     return att

# def getObjDict(c, prefix, variables, i):
#     res={var: getVarValue(c, prefix+var, i) for var in variables}
#     res['index']=i
#     return res

# def getElectrons(c, collVars=electronVars):
#     return [getObjDict(c, 'Electron_', collVars, i) for i in range(int(getVarValue(c, 'nElectron')))]

# def getGoodElectrons(c, collVars=electronVars, ele_selector = None):
#     return [l for l in getElectrons(c, collVars) if ( ele_selector is None or ele_selector(l))]

# f = ROOT.TFile.Open("rootFiles/FDA75866-C35A-F84A-A299-4C2A9A4EAAD9.root")
# r = f.Get("Events")
# electrons_pt10 = getGoodElectrons(r) #No eleSelector bc my electrons are already selected
# leptons = electrons_pt10

lept = {
    'pdgId' : 11,
    'pt' : 15,
    'eta' : 1, 
    'jetNDauCharged': '2', # jetNDauChargedMVASel
    'miniPFRelIso_chg': 0.3, # miniRelIsoCharged
    'miniPFRelIso_all': 0.39,
    'jetPtRelv2': 1,
    'jetPtRatio': 1, # jetPtRatioVanilla 
    'pfRelIso03_all': 1, # relIso0p3Vanilla
    'jetBTag': 0.2,
    'sip3d': 2,
    'dxy': 1,
    'dz': 1,
    'mvaFall17V2noIso': 0.5, # eleMvaFall17v2
    'lostHits': '1', # eleMissingHits
}

lept2 = {
    'pdgId' : 13,
    'pt' : 109.58,
    'eta' : 0.15563, 
    'jetNDauCharged': '1', # jetNDauChargedMVASel
    'miniPFRelIso_chg': 0, # miniRelIsoCharged
    'miniPFRelIso_all': 0,
    'jetPtRelv2': 35.25,
    'jetPtRatio': 1/0.0169677, # jetPtRatioVanilla 
    'pfRelIso03_all': 0.0112267, # relIso0p3Vanilla
    'jetBTag': 0,
    'sip3d': 2.08984,
    'dxy': 0.0031833,
    'dz': 0.0087356,
    'segmentComp' : 0.8954467, # segComp
}
leptons = [lept, lept2]

mvaTOPreader_ = mvaTOPreader(year = "UL2017", versions = ["v1","v2"])

for lep in leptons:
    print("PDG ID :", lep['pdgId'])
    mvaScore_v1, WP_v1, mvaScore_v2, WP_v2 = mvaTOPreader_.getmvaTOPScore(lep)
    print(mvaScore_v1, WP_v1, mvaScore_v2, WP_v2)