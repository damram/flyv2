#include <iostream>
#include <cstdio>
#include <cstring>
#include <sstream>

int main() {
    // Remplacez "votre_programme" par le nom de votre programme à exécuter.
    FILE *pipe = popen("python3 getMVAscore.py 13", "r");
    if (!pipe) {
        std::cerr << "Erreur lors de l'ouverture du processus." << std::endl;
        return 1;
    }

    char buffer[128];
    std::string output;

    while (!feof(pipe)) {
        if (fgets(buffer, 128, pipe) != nullptr) {
            output = buffer;
        }
    }

    pclose(pipe);

    // Maintenant, 'output' contient la sortie du programme exécuté.
    std::cout << "Sortie du programme : " << output << std::endl;

        // Utilisation de stringstream pour extraire les nombres
    std::istringstream iss(output);
    double num1, num2, num3, num4;

    // Assurez-vous que la conversion est réussie avant d'utiliser les nombres
    if (iss >> num1 >> num2 >> num3 >> num4) {
        std::cout << "Nombres extraits : " << num1 << ", " << num2 << ", " << num3 << ", " << num4 << std::endl;
    } else {
        std::cerr << "Erreur lors de l'extraction des nombres." << std::endl;
    }

    return 0;
}
