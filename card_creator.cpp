#include "card_creator.hpp"
#include "sample.hpp"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "TH1F.h"
#include "TFile.h"

using namespace std;

unsigned int block_proc = 36; //30;
unsigned int block_syst = 30; //24;
unsigned int block_grp  = 21; //15;

/////////////////////////////////
// Constructor
/////////////////////////////////

Card::Card(){}

/////////////////////////////////
// Private methods
/////////////////////////////////

std::string Card::completeBlock(std::string const& word_p, unsigned int blockSize_p)
{ 

    int size = blockSize_p - word_p.size();
    //cout << "completeBlock " << word_p << " " <<size<<endl;

    if(size <= 0)
    {
        cout<<"Error with "+word_p+" block in datacard"<<endl;
        return "";
    }
    std::string spaces;
    for(int i = 0; i < size; ++i)
    {
        spaces += " ";
    }
    return word_p+spaces;
}

/////////////////////////////////
// Public methods
/////////////////////////////////

void Card::addSeparator()
{
    datacard += "--------------------------------------------------------------------------------------------- \n";
}

void Card::addLine(std::string const& line)
{
    datacard += line + '\n';
}

void Card::addGlobalParameter(namelist const& groupList_p,int numberOfBins)
{
    datacard += "imax "+std::to_string(numberOfBins)+" number of bins\n";
    datacard += "jmax "+std::to_string(groupList_p.size()-1)+" number of background processes\n";
    datacard += "kmax * number of nuisance parameters\n";
}

void Card::addInputsProcess(std::string const& directory_p, std::string const& rootfile_p)
{
    datacard += "shapes * * "+directory_p+rootfile_p+" $PROCESS $PROCESS_$SYSTEMATIC\n";
    datacard += "shapes sig * "+directory_p+rootfile_p+" $PROCESS $PROCESS_$SYSTEMATIC\n";
}

void Card::addChanels(std::string const& observable_p, double numberOfEvents_p)
{
    datacard += "bin "+observable_p+"\n";
    datacard += "observation "+std::to_string(numberOfEvents_p)+"\n";
}

void Card::addProcToCard(std::string const& observable_p, namelist const& groupList_p)
{

    std::string line0 = completeBlock("bin", block_proc);
    std::string line1 = completeBlock("process", block_proc);
    std::string line2 = line1;
    std::string line3 = completeBlock("rate", block_proc);

    for(int i = 0; i < int(groupList_p.size()); ++i)
    {
        line0 += completeBlock(observable_p, block_grp);
        line1 += completeBlock(groupList_p[i], block_grp);
        // if (groupList_p[0] == "signal")
        // {
        line2 += completeBlock(std::to_string(i), block_grp);
        // }
        // else
        // {
        //     line2 += completeBlock(std::to_string(i-1), block_grp);
        // }
        line3 += completeBlock("-1", block_grp);
    }
    datacard += line0+'\n'+line1+'\n'+line2+'\n'+line3+'\n'; 
}

void Card::addProcSystToCard(std::string const& systName_p, std::string const& shape_p, namelist const& groupList_p, std::string const& process_p, std::string const& value_p, std::string const& process2_p)
{

    //cout << "addProcSystToCard "<<systName_p<<" "<<shape_p<<" "<<process_p<< " "<<value_p<<endl;
    datacard += completeBlock(systName_p, block_syst) 
            + completeBlock(shape_p, block_proc-block_syst);

    for(size_t i = 0; i < groupList_p.size(); ++i)
    {
        if(groupList_p[i] == process_p || groupList_p[i] == process2_p)
        {
            datacard += completeBlock(value_p, block_grp);
        }
        else
        {
            datacard += completeBlock("-", block_grp);
        }
    }
    datacard += '\n';
}


void Card::addSystToCard(std::string const& systName_p, std::string const& shape_p, namelist const& groupList_p, std::string const& value_p, std::string exludeProcess)
{
    datacard += completeBlock(systName_p, block_syst) 
             + completeBlock(shape_p, block_proc-block_syst);

    for(size_t i = 0; i < groupList_p.size(); ++i)
    {
	    if (exludeProcess=="" || (exludeProcess!="" && groupList_p[i]!=exludeProcess))
        {
            datacard += completeBlock(value_p, block_grp);
        }
        else if (groupList_p[i]==exludeProcess)
	    {
            datacard += completeBlock("-", block_grp);
        }
    }
    datacard += '\n';

}

void Card::addRateToCard(namelist const& groupList_p, namelist const& groupRateList_p, namelist const& systematicsRate_p, int tt_process)
{
    for(size_t i = 0; i < groupRateList_p.size(); ++i)
    {
        if(i == 0)
        {
            i += 1;
        }
        // cout<<"i = "<<i<<endl;
        std::string line = completeBlock("r"+groupRateList_p[i], block_syst) 
                         + completeBlock("lnN", block_proc-block_syst);
        for(size_t j = 0; j < groupList_p.size(); ++j)
        {
            //cout<<"j = "<<j<<endl;
            if(i == j && groupRateList_p[i] != "tt")
            {
                line += completeBlock(systematicsRate_p[j], block_grp);
            }
            else if(i != 0 && i == j && groupRateList_p[i] == "tt")
            {
                for(int k = 0; k < tt_process; k++)
                {
                    line += completeBlock(systematicsRate_p[j+k], block_grp);
                }
                j += tt_process-1;
            }
            else
            {
                line += completeBlock("-", block_grp);
            }
        }
        datacard += line + '\n';
    }
}

void Card::printCard()
{
    cout << " >> datacard :" << endl;
    cout << datacard << endl;
}

void Card::saveCard(std::string const& name_p)
{
    std::ofstream file(name_p);
    file << datacard;
    file.close();
}

int card_creator(std::string year)
{

    std::string cuts[5] = {"0000", "0010","0020", "0060", "0070"};
    // std::string cuts[1] = {"muel_CR_tt1l"};
    // std::string list_masses[10] = {"600", "625", "650", "675", "700", "800", "900", "1000", "1100", "1200"};

    bool doExpTimeNuisance = false;
    bool doPuTime = true;
    bool doDecorrelateBtag = false;
    int iyear = -1;
    if(year == "2016")
    {
        iyear = 0;
        systematicList = systematicList_2016;
    }
    else if(year == "2017")
    {
        iyear = 1;
        systematicList = systematicList_2017;
    }
    else if(year == "2018")
    {
        iyear = 2;
        systematicList = systematicList_2018;
    }
    int triggerOption = 1; //0: Full trigger syst uncertainties including Nvtx partition, 1: Trigger syst uncertainties without Nvtx partition, 2:Trigger syst uncertainties treated as uncorrelated in time
    int jecOption = -1; //-1: no jec, 0: reduced jec, 1: full jec, 2: reduced jec with pureflavour
    cout << "doExpTimeNuisance = "<<doExpTimeNuisance<<", triggerOption = "<<triggerOption<<", jecOption = "<<jecOption<<endl;

    //Lumi flat uncertainties
    std::string lumi_flat_uncorr_inc[3] = {"1.01", "1.02", "1.015"};
    std::string lumi_flat_corr_inc_2016_2018[3] = {"1.006", "1.009", "1.02"};
    std::string lumi_flat_corr_inc_2017_2018[2] = {"1.006", "1.002"};

    for(int i = 0; i < sizeof(cuts)/sizeof(cuts[0]); i++)
    {
        cout<<"cuts[i] = "<<cuts[i]<<endl;
        std::string observable="signal";
        if(cuts[i]=="0000") observable="ZZ";
        if(cuts[i]=="0010") observable="WZ";
        if(cuts[i]=="0020") observable="Nonprompt";
        if(cuts[i]=="0060") observable="signal_3l";
        if(cuts[i]=="0070") observable="signal_4l";
        //std::string observable="bdtScore";
        std::string name = observable;

        // cout<<"list_masses[j] = "<<list_masses[j]<<endl;
        Card datacard;
        namelist processList_modified = processList;
        namelist processRateList_modified = processRateList;
        namelist systematicRate_modified = systematicRate;
        
        string f_data_name = "./combine/" + year + "/inclusive/inputs/" + observable + "_" + cuts[i] + ".root";
        TFile* f_data = new TFile(f_data_name.c_str(), "UPDATE");
        TH1F* h_signal = (TH1F*) f_data->Get(processList[0].c_str());
        cout<<"Trying to get : "<<processList[0].c_str()<<" from : "<<f_data_name.c_str()<<endl;
        if(h_signal->GetEntries() <=0)
        {
            cout<<"ALERT: NO SIGNAL IN THE CHANNEL "<<cuts[i]<<endl;
        }
        int k = 1;
        int tt_processes = 0;
        for(int l = 1; l < processList.size(); l++)
        {
            // cout<<"process = "<<processList[l].c_str()<<endl;
            TH1F* h_process = (TH1F*) f_data->Get(processList[l].c_str());
            if(h_process->GetEntries() <=0)
            {
                processList_modified.erase(processList_modified.begin() + k);
                systematicRate_modified.erase(systematicRate_modified.begin() + k);
                if(processList[l].find("tt2l") == std::string::npos && processList[l].find("tt1l") == std::string::npos && processList[l].find("tt0l") == std::string::npos)
                {
                    processRateList_modified.erase(processRateList_modified.begin() + k);
                }
            }
            else
            {
                k++;
            }
        }
        for(int l = 1; l < processList_modified.size(); l++)
        {
            // cout<<"process = "<<processList_modified[l].c_str()<<endl;
            if(processList_modified[l].find("tt") != std::string::npos)
            {
                tt_processes++;
            }
            TH1F* h_process = (TH1F*) f_data->Get((processList_modified[l]).c_str());
            for(int n = 0; n < h_process->GetNbinsX(); n++)
            {
                double bin_content = h_process->GetBinContent(n);
                if(bin_content < 0)
                {
                    h_process->SetBinContent(n,0);
                    h_process->SetName((processList_modified[l]).c_str());
                    h_process->Write(h_process->GetName(),TObject::kOverwrite);
                }
            }
            for(int m = 0; m < systematicList.size(); m++)
            {
                // cout<<"systematic = "<<systematicList[m]<<endl;
                TH1F* h_processUp = (TH1F*) f_data->Get((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                for(int n = 0; n < h_processUp->GetNbinsX(); n++)
                {
                    double bin_contentUp = h_processUp->GetBinContent(n);
                    if(bin_contentUp < 0)
                    {
                        h_processUp->SetBinContent(n,0);
                        h_processUp->SetName((processList_modified[l]+"_"+systematicList[m]+"Up").c_str());
                        h_processUp->Write(h_processUp->GetName(),TObject::kOverwrite);
                    }
                }
                TH1F* h_processDown = (TH1F*) f_data->Get((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                for(int n = 0; n < h_processDown->GetNbinsX(); n++)
                {
                    double bin_contentDown = h_processDown->GetBinContent(n);
                    if(bin_contentDown < 0)
                    {
                        h_processDown->SetBinContent(n,0);
                        h_processDown->SetName((processList_modified[l]+"_"+systematicList[m]+"Down").c_str());
                        h_processDown->Write(h_processDown->GetName(),TObject::kOverwrite);
                    }
                }
            }
        }
        cout<<"tt_processes = "<<tt_processes<<endl;
        if(tt_processes == 0)
        {
            processRateList_modified.pop_back();
        }
        TH1F* h_data = (TH1F*) f_data->Get("data_obs");
        double numberOfEvents = h_data->Integral();
        cout << "Number of events in data : " << numberOfEvents << endl;

        datacard.addGlobalParameter(processList_modified);
        datacard.addSeparator();
        cout << "Open input file"<<endl;
        datacard.addInputsProcess("./inputs/"+year+"/", name+"_"+cuts[i]+".root");
        datacard.addSeparator();
        cout << observable << " " << numberOfEvents <<endl;
        datacard.addChanels(observable, numberOfEvents);
        datacard.addSeparator();
        datacard.addProcToCard(observable, processList_modified);
        datacard.addSeparator();
        cout << "Bkgd norm" << endl;
        datacard.addRateToCard(processList_modified, processRateList_modified, systematicRate_modified, tt_processes);
        cout << "Weights nuisances (exp)" <<endl;

        for(std::string const& syst : systematicList)
        {
            if (syst=="stat_muon_iso" || syst=="stat_muon_id")
            {
                datacard.addSystToCard(syst + "_" + year, "shape", processList_modified);
            }
            else if (doDecorrelateBtag && (syst == "syst_b_correlated" || syst == "syst_l_correlated"))
            {
                datacard.addSystToCard(syst + "_" + year, "shape", processList_modified);
            }
            else if (syst == "syst_b_uncorrelated" || syst == "syst_l_uncorrelated")
            {
                datacard.addSystToCard(syst + "_" + year, "shape", processList_modified);
            }
            else
            {
                datacard.addSystToCard(syst, "shape", processList_modified);
            }  
        }

        cout << "Rate nuisances"<<endl;
        datacard.addSystToCard("lumi_uncor_"+year, "lnN", processList_modified, lumi_flat_uncorr_inc[iyear]);
        datacard.addSystToCard("lumi_cor_2016_2018", "lnN", processList_modified, lumi_flat_corr_inc_2016_2018[iyear]);
        if(iyear == 1 || iyear == 2)
        {
            datacard.addSystToCard("lumi_cor_2017_2018", "lnN", processList_modified, lumi_flat_corr_inc_2017_2018[iyear-1]);
        }

        cout << "Jec nuisances"<<endl;
        if (jecOption==0 || jecOption==2)
        {
            datacard.addSystToCard("Absolute_jec", "shape", processList_modified);
            datacard.addSystToCard("Absolute_"+year+"_jec", "shape", processList_modified);
            //datacard.addSystToCard("FlavorQCD_jec", "shape", processList_modified);
            datacard.addSystToCard("BBEC1_jec", "shape", processList_modified);
            datacard.addSystToCard("BBEC1_"+year+"_jec", "shape", processList_modified);
            datacard.addSystToCard("RelativeBal_jec", "shape", processList_modified);
            datacard.addSystToCard("RelativeSample_"+year+"_jec", "shape", processList_modified);
            if (jecOption==0)
            {
                datacard.addSystToCard("FlavorQCD_jec", "shape", processList_modified);
            }
            else if (jecOption==2)
            { 
                datacard.addSystToCard("FlavorPureGluon_jec", "shape", processList_modified);
                datacard.addSystToCard("FlavorPureQuark_jec", "shape", processList_modified);
                datacard.addSystToCard("FlavorPureCharm_jec", "shape", processList_modified);
                datacard.addSystToCard("FlavorPureBottom_jec", "shape", processList_modified);
            }
        }
        else if (jecOption==1)
        {
            std::string listJecFull[13] = {"AbsoluteMPFBias","AbsoluteScale","FlavorQCD","Fragmentation", "PileUpDataMC","PileUpPtBB","PileUpPtEC1","PileUpPtRef","RelativeFSR","RelativePtBB","SinglePionECAL","SinglePionHCAL","RelativeBal"};
            std::string listJecFull_year[7] = {"AbsoluteStat","RelativeJEREC1","RelativePtEC1","RelativeStatEC","RelativeStatFSR","TimePtEta","RelativeSample"};
            for (int ijec=0; ijec<13; ijec++) datacard.addSystToCard(listJecFull[ijec]+"_jec", "shape", processList_modified);
            for (int ijec=0; ijec<7; ijec++) datacard.addSystToCard(listJecFull_year[ijec]+"_"+year+"_jec", "shape", processList_modified);
        }

        datacard.addSeparator();
        datacard.addLine("* autoMCStats 0");
        datacard.saveCard("combine/"+year+"/inclusive/inputs/"+name+"_datacard_"+cuts[i]+".txt");
        // datacard.printCard();
        f_data->Close();
    }

    cout << "Finished !!!" << endl;
    return 0;
}