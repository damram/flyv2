#!/bin/bash
#SBATCH --job-name=MCAnalysis
#SBATCH --output=slurm.out
#SBATCH --error=slurm.err

#SBATCH --cpus-per-task=1
#SBATCH --time=5:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=d.amram@ip2i.in2p3.fr
#SBATCH --ntasks=500

# Clean the folder before doing it again
date=$(date "+%Y-%m-%d")
date="2024-10-28"
echo $date
./removeEmptyFiles.py $date MC

if [ ! -d "results/17/$date/merged" ]; then
    mkdir -p "results/17/$date/merged"
fi

# Merge all MC file per folder
for name in $(ls DSinfo/17/mc)
do
    # Verify that at least one file exist
    if find "results/17/$date/test_nbTasksall/${name::-4}/" -type f -name "*.root" -print -quit | grep -q .; then
        # srun -n1 -N1 --exclusive hadd -fk results/17/$date/merged/$name.root results/17/$date/test_nbTasksall/$name/*root &
        hadd -f results/17/$date/merged/"${name::-4}".root results/17/$date/test_nbTasksall/"${name::-4}"/*.root &
    fi
done
wait

# Merge all MC in one file for the btag eff
# srun -n1 -N1 --exclusive hadd -fk results/17/$date/merged/PROC_ALL.root results/17/$date/merged/PROC*root &
hadd -f results/17/$date/merged/PROC_ALL$cut.root results/17/$date/merged/PROC*$cut.root

if [ ! -d "results/17/$date/ready" ]; then
    mkdir -p "results/17/$date/ready"
fi
# Now book the histograms
for name in $(ls DSinfo/17/mc)
do
    # Verify that at least one file exist
    if find "results/17/$date/merged/" -type f -name "*"${name::-4}".root" -print -quit | grep -q .; then
        srun ./processCounting.py results/17/$date/merged/"${name::-4}".root results/17/$date/ready/"${name::-4}".root jobconfiganalysis_17 0 &
    fi
done
wait
echo "processCounting ended"
total_files=$(ls results/17/$date/ready/PROC* | wc -l)
processed_files=1
for filename in $(ls results/17/$date/ready/PROC*)
do
    # idk if it take less time but idc
    echo "$processed_files / $total_files"
   ./copyHistoSow.py $filename
   ((processed_files++))
done
wait
echo "Finished."
