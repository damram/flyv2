// Run 2samples : 

#include <vector> 
#include <string> 
 
using  namelist = std::vector<std::string>; 
 
#ifndef COMMON_LIST 
#define COMMON_LIST

namelist systematicList {
    "syst_elec_reco",
    "syst_elec_id",
    "syst_muon_hlt",
    "stat_muon_hlt",
    "syst_muon_reco",
    "stat_muon_reco",
    "syst_muon_id",
    "stat_muon_id",
    "syst_muon_iso",
    "stat_muon_iso",
    "syst_pu",
    "syst_b_correlated",
    "syst_b_uncorrelated",
    "syst_l_correlated",
    "syst_l_uncorrelated",
    "syst_prefiring",
    "mescale",
    "renscale",
    "facscale",
    "isr",
    "fsr",
    // "pdfalphas"
};

namelist systematicList_2016 {
    "syst_elec_reco",
    "syst_elec_id",
    "syst_muon_hlt",
    "stat_muon_hlt_2016",
    "syst_muon_reco",
    "stat_muon_reco_2016",
    "syst_muon_id",
    "stat_muon_id_2016",
    "syst_muon_iso",
    "stat_muon_iso_2016",
    "syst_pu",
    "syst_b_correlated",
    "syst_b_uncorrelated_2016",
    "syst_l_correlated",
    "syst_l_uncorrelated_2016",
    "syst_prefiring",
    "mescale",
    "renscale",
    "facscale",
    "isr",
    "fsr",
    // "pdfalphas"
};

namelist systematicList_2017 {
    "syst_elec_reco",
    "syst_elec_id",
    "syst_muon_hlt",
    "stat_muon_hlt_2017",
    "syst_muon_reco",
    "stat_muon_reco_2017",
    "syst_muon_id",
    "stat_muon_id_2017",
    "syst_muon_iso",
    "stat_muon_iso_2017",
    "syst_pu",
    "syst_b_correlated",
    "syst_b_uncorrelated_2017",
    "syst_l_correlated",
    "syst_l_uncorrelated_2017",
    "syst_prefiring",
    "mescale",
    "renscale",
    "facscale",
    "isr",
    "fsr",
    // "pdfalphas"
};

namelist systematicList_2018 {
    "syst_elec_reco",
    "syst_elec_id",
    "syst_muon_hlt",
    "stat_muon_hlt_2018",
    "syst_muon_reco",
    "stat_muon_reco_2018",
    "syst_muon_id",
    "stat_muon_id_2018",
    "syst_muon_iso",
    "stat_muon_iso_2018",
    "syst_pu",
    "syst_b_correlated",
    "syst_b_uncorrelated_2018",
    "syst_l_correlated",
    "syst_l_uncorrelated_2018",
    "syst_prefiring",
    "mescale",
    "renscale",
    "facscale",
    "isr",
    "fsr",
    // "pdfalphas"
};

namelist processList {
    "signal",
    "tt",
    "ttx",
    "singletop",
    "multibosons",
    "wjets",
    "dy",
    "WZ",
    "ZZ",
    "fake",
    "conversion"
};

namelist processRateList {
    "signal",
    "tt",
    "ttx",
    "singletop",
    "multibosons",
    "wjets",
    "dy",
    "WZ",
    "ZZ",
    "fake",
    "conversion"
};

namelist systematicRate {
    "1.04",
    "1.10",
    "1.15",
    "1.1",
    "1.25",
    "1.10",
    "1.08",
    "1.06",
    "1.10",
    "1.06",
    "1.06",
};
#endif 