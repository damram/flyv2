rootlibs:=$(shell root-config --libs)
rootflags:=$(shell root-config --cflags)


CORRECTION_INCDIR:=$(shell correction config --incdir)
CORRECTION_LIBDIR:=$(shell correction config --libdir)
OBJDIR=src
SRCDIR=src

SOFLAGS       = -shared

LD = g++ -m64 -g -Wall

#CXXFLAGS = -O0 -g -Wall -fmessage-length=0 $(rootflags) -fpermissive -fPIC -pthread -DSTANDALONE -I$(SRCDIR) -I$(CORRECTION_INCDIR)
CXXFLAGS = -O0 -g -Wall -fmessage-length=0 $(rootflags) -fPIC -I$(SRCDIR) -I$(CORRECTION_INCDIR) -I. -I/gridgroup/cms/amram/Conda/anaconda3/pkgs/python-3.10.13-h955ad1f_0/include/python3.10

SRCS := $(wildcard $(SRCDIR)/*.cpp)
HEADERS = $(wildcard $(SRCDIR)/*.h)
OBJS := $(patsubst %.cpp,%.o,$(SRCS)) $(SRCDIR)/rootdict.o

LIBS_EXE = src/libLMVA.so xgboost/lib/libxgboost.so $(rootlibs) -lMathMore -lGenVector -lcorrectionlib -L$(CORRECTION_LIBDIR) -lTMVA /gridgroup/cms/amram/Conda/anaconda3/lib/libpython3.10.so
LIBS = $(rootlibs) -lTMVA src/libLMVA.so xgboost/lib/libxgboost.so

TARGET =	nanoaodrdataframe

all:	$(TARGET) src/libLMVA.so xgboost/lib/libxgboost.so libnanoadrdframe.so 

all: $(EXECUTABLE)
	@echo "                                                                              dddddddd                    dddddddd ";
	@echo "                                                                              d::::::d  iiii              d::::::d       iiii           tttt                !!! ";
	@echo "                                                                              d::::::d i::::i             d::::::d      i::::i       ttt:::t               !!:!! ";
	@echo "                                                                              d::::::d  iiii              d::::::d       iiii        t:::::t               !:::! ";
	@echo "                                                                              d:::::d                     d:::::d                    t:::::t               !:::! ";
	@echo " yyyyyyy           yyyyyyy   ooooooooooo   uuuuuu    uuuuuu           ddddddddd:::::d iiiiiii     ddddddddd:::::d      iiiiiii ttttttt:::::ttttttt         !:::! ";
	@echo "  y:::::y         y:::::y  oo:::::::::::oo u::::u    u::::u         dd::::::::::::::d i:::::i   dd::::::::::::::d      i:::::i t:::::::::::::::::t         !:::! ";
	@echo "   y:::::y       y:::::y  o:::::::::::::::ou::::u    u::::u        d::::::::::::::::d  i::::i  d::::::::::::::::d       i::::i t:::::::::::::::::t         !:::! ";
	@echo "    y:::::y     y:::::y   o:::::ooooo:::::ou::::u    u::::u       d:::::::ddddd:::::d  i::::i d:::::::ddddd:::::d       i::::i tttttt:::::::tttttt         !:::! ";
	@echo "     y:::::y   y:::::y    o::::o     o::::ou::::u    u::::u       d::::::d    d:::::d  i::::i d::::::d    d:::::d       i::::i       t:::::t               !:::! ";
	@echo "      y:::::y y:::::y     o::::o     o::::ou::::u    u::::u       d:::::d     d:::::d  i::::i d:::::d     d:::::d       i::::i       t:::::t               !:::! ";
	@echo "       y:::::y:::::y      o::::o     o::::ou::::u    u::::u       d:::::d     d:::::d  i::::i d:::::d     d:::::d       i::::i       t:::::t               !!:!! ";
	@echo "        y:::::::::y       o::::o     o::::ou:::::uuuu:::::u       d:::::d     d:::::d  i::::i d:::::d     d:::::d       i::::i       t:::::t    tttttt      !!! ";
	@echo "         y:::::::y        o:::::ooooo:::::ou:::::::::::::::uu     d::::::ddddd::::::ddi::::::id::::::ddddd::::::dd     i::::::i      t::::::tttt:::::t ";
	@echo "          y:::::y         o:::::::::::::::o u:::::::::::::::u      d:::::::::::::::::di::::::i d:::::::::::::::::d     i::::::i      tt::::::::::::::t      !!! ";
	@echo "         y:::::y           oo:::::::::::oo   uu::::::::uu:::u       d:::::::::ddd::::di::::::i  d:::::::::ddd::::d     i::::::i        tt:::::::::::tt     !!:!! ";
	@echo "        y:::::y              ooooooooooo       uuuuuuuu  uuuu        ddddddddd   dddddiiiiiiii   ddddddddd   ddddd     iiiiiiii          ttttttttttt        !!! ";
	@echo "       y:::::y ";
	@echo "      y:::::y ";
	@echo "     y:::::y ";
	@echo "    y:::::y ";
	@echo "   yyyyyyy ";

	@echo "                                                                                                           ";
	@echo "                                                              :...:                                        ";
	@echo "                                                           -......--=                                      ";
	@echo "                                                   =--::::........--.-                                    ";
	@echo "                                                -:...........:....:=..:=                                  ";
	@echo "                                              -............:---....=-...-=                                ";
	@echo "                                          =-:.............:---:.....=:....:--                             ";
	@echo "                                    -::...................----......:-.......-                            ";
	@echo "                                  -........:......:.......---.......:=.-====-.:=                          ";
	@echo "                            -:::..........*@-....:@%................:=:=======:::                         ";
	@echo "                        -:................-@#----*@+................---========-.:                        ";
	@echo "                      -.............#%:.:===:.    .--=:.............=-==========-..:                      ";
	@echo "                 --::...............-%@+-             =+............=::==========:....=                   ";
	@echo "              -:.....................-+         :===-  :#:..........=:.==========-....=                   ";
	@echo "            -.............::::......:+        :*+=+=+#- .%:.........--.:==========....                    ";
	@echo "       ::...:--.......-++++===+++:..*        .#=#@@@%=#: =+..........=..:=========...:                    ";
	@echo "      .....:---:...-++:.........:++-+        -++@@@@@#=+  %..........--...-======-...:                    ";
	@echo "      :....----:..*=..............:%-        .#=%@@@@+*-.:+-----:....:=.....:---:....-                    ";
	@echo "       ....---:.:#:................--         :*+***+*+-:....:...=:..--..............-                    ";
	@echo "       -....:...#=..................+           :-====-.:........:=..=-..............-                    ";
	@echo "        ...:...-%.:...............-+%-             .=:.....:......-.:=:.....:=-:.....-                    ";
	@echo "        :.:-...+@+-...-++++++=+++++=:%=            --....---.....:..-=......-===-....-                    ";
	@echo "        :.::..+#:.-++=-...........:+%%-*:          :=..-%@#--.......=:......:====-...=         =..=       ";
	@echo "        -.....%-+*=..............:-=-@..=*=.       -+-:-##%.........=...--:..:===-..-          ....:      ";
	@echo "        =-....:@-:*++*****=-:......+#:....:=+======:..+###%+........-..=====-..::...=          =....      ";
	@echo "         =-....#-#:.......:-=+++++**-:..............-######%........-.:=======......           %...+      ";
	@echo "           :...:*+...............................:+########@:.......-.:=======-.....           =-=+%      ";
	@echo "           -.....*=...............-=-:.........-*%#########@-.......=..========:....          =#+:...:=#  ";
	@echo "          =:......+#*=:....:-=+**+:.........-*%############@-......-:..-=======-...:         -@:.-===-..# ";
	@echo "           :........-+*###@@*=-.........:=##=@#############@-.....:-....-======:...:        +.#*#=-:-=*#: ";
	@echo "           -.............*#:.......:=+#@#=.  %#############@......-:.....:-====....=        +.%=........  ";
	@echo "            -..........:%*===++*###**@#%     #############%#.:-:..=:........:.....-         #.#*+++++**-# ";
	@echo "            =:..........:----::#    -%#%****#%######%%***#@-:---:.=:.............:-         *.-%:......*  ";
	@echo "             -................:#+==*##########%###%*-----#*.----:.=:......--:....=          ..+*.:-=....  ";
	@echo "             -.....................%#######%*===%*------##.:---:.:=......-===-..:          ....:=-:....+  ";
	@echo "             :....................=%#####%*----+------=%*...:::..=-......-====:.-         ..:# *=---=*    ";
	@echo "             :.....:-:...........:%####@*----------+*+:...:-.....--.....+#+--=+#@*      *..+               ";
	@echo "             -....----.......:..-%####@#------=+**+:.....:-:.....=:....++        -*=   =..#                ";
	@echo "              ....---:...::...+*@####@#------=+**+:.....:-:.....=:....++        -*=   =..#                ";
	@echo "              :...::.....-......:+#%@@#######+-..........:......=.....%            *#:.-*                 ";
	@echo "              -.....:---.---:::::....::::......................--.....%.     :*%+=%=..+                   ";
	@echo "              =...:-----.......::--:....::::::.......:--=--:::-=-=-:..*+    :#.:==..-%                    ";
	@echo "          *=-:=:.:-----:...........:---:::::::...::-=-:.      *:..:--..%=   =%-...:*@                     ";
	@echo "       :      ==.-----:...............:-=-::::=*=::.         .*.....-=--%*. .=+%##**@                     ";
	@echo "      +       -=.:--:........:-=-++===+*+. .=*=.          .:-*=.......::-#%=    .:+                       ";
	@echo "      +       --..:::--::--==-.:*-**+*%-.=*+.  .::-===++**+++%#*+-:.......-#%++*                          ";
	@echo "     #@:      ==--::....:.:-=-+=  =#*%--==++++*++==-===++---**+++*#*=:.....#                              ";
	@echo "    =.*@=.  :+%#           .:===+%*++%#===++++---#%@@@@@#--=%++*@%#**#*+-:-@                              ";
	@echo "  :..=        @-.::-====++++===+%+++++@@@@@@%*--=**++===---+#++*%@@@%#*+*#%                               ";
	@echo "  *..-      :%%*++====+=--#%@@@@@*+++##*++==---------------%++++++*#%@@#+*@                               ";
	@echo "   %+..:+#:..@+-#%@@@@%+--*+++===%+*#=----------------===+*%++++++++++*++%                                ";
	@echo "     @*:....-@=-+++==------------+#=------==+++******#***+++*##*++++++++*@                                ";
	@echo "       %....+%----------------===++****#****++===-------------=+*#*+++++%                                 ";
	@echo "        +...#*-----==++*********++===------------+*---------------=**#*#@                                 ";
	@echo "        @*+.%##*****++==-----------------------=#%=--------+=++*##%%@+                                    ";
	@echo "        #.--:#@%#+=----=*#=-------------------=%%=---------%                                               ";
	@echo "           **      %*++%*=-------=*-==++**##%%@%=-=+***=---@                                               ";
	@echo "                      %=---------+@@*           +-#+.:@#--==                                               ";
	@echo "                     ==--+**##=--*@              %%-.-@+++                                                ";
	@echo "                      #==%-.*%=--#                @..+                                                    ";
	@echo "                        -@..%%*#                  %..*                                                    ";
	@echo "                         %..@                     %..*                                                    ";
	@echo "                         #.:@                     %..*                                                    ";
	@echo "                         #-+@                     %..*                                                    ";
	@echo "                         %::@                     @::+                                                    ";
	@echo "                         @::#                      -..                                                    ";
	@echo "                          - -                      *  +                                                   ";
	@echo "                          *                         . .                                                   ";
	@echo "                           - =                      #+#@.                                                 ";
	@echo "                           @%@@                    -@@@@@@                                                ";
	@echo "                    --#   @@@@@@            #..*@@@@@@@@@@                                                ";
	@echo "                  @%+*@@@@@@@@@@%           @@@@@@@@@@@@@@                                                ";
	@echo "                  @@@@@@@@@@@@@@            +@@@@@@@@%@@@@                                                ";
	@echo "                   @@@@@@   *@@@                                                                          ";
	@echo "                      @                                                                                   ";


clean:
	rm -f $(OBJS) $(TARGET) libnanoaodrdframe.so $(SRCDIR)/rootdict.C rootdict_rdict.pcm
	rm -rf .nfs*
#$(SRCDIR)/rootdict.C: $(SRCDIR)/NanoAODAnalyzerrdframe.h $(SRCDIR)/SkimEvents.h $(SRCDIR)/Linkdef.h 
$(SRCDIR)/rootdict.C: $(SRCDIR)/NanoAODAnalyzerrdframe.h $(SRCDIR)/BaseAnalyser.h $(SRCDIR)/Linkdef.h  

	rm -f $@
	rootcling -I$(CORRECTION_INCDIR) -I$(SRCDIR) $@ $^

	rm -f rootdict_rdict.pcm
	ln -s $(SRCDIR)/rootdict_rdict.pcm .

$(SRCDIR)/rootdicttmp.C: $(SRCDIR)/testing.h $(SRCDIR)/test_Linkdef.h 
	rm -f $@
	rootcling -I$(CORRECTION_INCDIR) -I$(SRCDIR) $@ $^

libtest.so: $(SRCDIR)/testing.o $(SRCDIR)/rootdicttmp.o
	$(LD) $(SOFLAGS) $(LIBS) -o $@ $^ 

$(SRCDIR)/rootdicttmp.o: $(SRCDIR)/rootdicttmp.C
	$(CXX) -c -o $@ $(CXXFLAGS) $<

libnanoadrdframe.so: $(OBJS)
	$(LD) $(SOFLAGS) $(LIBS) -o $@ $^ 

$(SRCDIR)/RoccoR.o: $(SRCDIR)/RoccoR.cpp $(SRCDIR)/RoccoR.h
	g++ -c -o $@ $(CXXFLAGS) $<

$(SRCDIR)/rootdict.o: $(SRCDIR)/rootdict.C
	$(CXX) -c -o $@ $(CXXFLAGS) $<

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) -c -o $@ $(CXXFLAGS) $<

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET)  $(OBJS) $(LIBS_EXE)

