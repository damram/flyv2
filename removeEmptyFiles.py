#!/usr/bin/env python3

import ROOT
import os
import sys

def tree_exists(filename):

    file = ROOT.TFile(filename)
    if not file:
        return False

    tree = file.Get('outputTree')
    if not tree:
        return False


    if tree.GetEntries() == 0:
        return False

    return True

def list_data_filemanes(root):
    listFiles=[]

    for dir in os.listdir(root):
        #Search in root/Run*
        if dir.startswith('Run'):
            fullPath = os.path.join(root,dir)
            #fullpath is now root/Run*
            for file in os.listdir(fullPath):
                file_path=os.path.join(fullPath,file)
                if file.endswith('.root'):
                    listFiles.append(file_path)
    return listFiles

def list_mc_filemanes(root):
    listFiles=[]

    for dir in os.listdir(root):
        #Search in root/MCDIR*
        if (not dir.startswith('Run')) and os.path.isdir(os.path.join(root,dir)):
            fullPath = os.path.join(root,dir)
            #fullpath is now root/MCDIR*
            for file in os.listdir(fullPath):
                filepath=os.path.join(fullPath,file)
                #true file is root/MCDIR*/...root
                if file.endswith('.root'):
                    listFiles.append(filepath)
    return listFiles

root = 'results/17/'+sys.argv[1]+'/test_nbTasksall'
typ=sys.argv[2]
if typ == 'MC':
    list=list_mc_filemanes(root)
else:
    list=list_data_filemanes(root)
print(len(list))
i=0
for file in list:
    if not tree_exists(file):
        print('Removing '+file)
        os.remove(file)
        i=i+1
        #pass
print(str(i)+' files removed ( '+str(i/len(list))+' )')