#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 11:01:46 2018

Auteur : Suyong Choi (Department of Physics, Korea University suyong@korea.ac.kr)

Ce script applique le traitement nanoaod à un fichier
"""

import sys
import cppyy
import ROOT
import os
from importlib import import_module
from argparse import ArgumentParser
import time

def process_file(inputfile, outfile, config, BTagEff, max_entries):
    t = ROOT.TChain(config['intreename'])
    t.Add(inputfile)
    
    if t.GetEntries() > max_entries:
        t.SetEntries(max_entries)
    
    aproc = ROOT.BaseAnalyser(t, outfile)
    aproc.setParams(config['year'], config['runtype'], config['datatype'])
    aproc.setupCorrections(config['goodjson'], config['pileupfname'], config['pileuptag'], config['btvfname'], config['btvtype'], config['fname_btagEff'], config['hname_btagEff_bcflav'], config['hname_btagEff_lflav'], config['muon_roch_fname'], config['muon_fname'], config['muon_HLT_type'], config['muon_RECO_type'], config['muon_ID_type'], config['muon_ISO_type'], config['electron_fname'], config['electron_reco_type'], config['electron_id_type'], config['jercfname'], config['jerctag'], config['jercunctag'])
    aproc.setupObjects(BTagEff)
    aproc.setupAnalysis(BTagEff, config['stepone'], config['weights'])
    aproc.run(config['saveallbranches'], config['outtreename'])
    return outfile

if __name__ == '__main__':
    start_time = time.time()

    parser = ArgumentParser(usage="%prog inputfile outputfile jobconfmod")
    parser.add_argument("infile")
    parser.add_argument("outfile")
    parser.add_argument("jobconfmod")
    parser.add_argument("BTagEff")
    parser.add_argument("--max_entries", type=int, default=10, help="Nombre maximum d'entrées à traiter")
    args = parser.parse_args()
    infile = args.infile
    outfile = args.outfile
    jobconfmod = args.jobconfmod
    BTagEff = 0 if args.BTagEff == '0' else 1

    # Charger le module de configuration
    mod = import_module(jobconfmod)
    config = getattr(mod, 'config')
    procflags = getattr(mod, 'procflags')
    config['saveallbranches'] = procflags['saveallbranches']
    config['stepone'] = config['stepone']
    config['weights'] = config['weights']

    # Charger les bibliothèques C++ compilées dans ROOT/python
    cppyy.load_reflection_info("libcorrectionlib.so")
    cppyy.load_reflection_info("libMathMore.so")
    cppyy.load_reflection_info("libnanoadrdframe.so")

    # Traiter le fichier jusqu'à max_entries
    process_file(infile, outfile, config, BTagEff, args.max_entries)

    end_time = time.time()  # Temps de fin
    execution_time = (end_time - start_time) / 60.0
    print(f"Temps d'exécution : {execution_time:.2f} min pour {infile}")
